dnl
dnl $Id: acinclude.m4,v 1.2 2004-12-15 17:00:57 cholm Exp $
dnl
dnl  Example offline project
dnl  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or
dnl  modify it under the terms of the GNU Lesser General Public License
dnl  as published by the Free Software Foundation; either version 2.1
dnl  of the License, or (at your option) any later version.
dnl
dnl  This library is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
dnl  Lesser General Public License for more details.
dnl
dnl  You should have received a copy of the GNU Lesser General Public
dnl  License along with this library; if not, write to the Free
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
dnl  02111-1307 USA
dnl
dnl ------------------------------------------------------------------
AC_DEFUN([AC_DEBUG],
[
  AC_REQUIRE([AC_PROG_CC])
  AC_REQUIRE([AC_PROG_CXX])
  AC_MSG_CHECKING(whether to make debug objects)
  AC_ARG_ENABLE(debug,
    [AC_HELP_STRING([--enable-debug],[Enable debugging symbols in objects])])
  if test "x$enable_debug" = "xno" ; then
    CFLAGS=`echo $CFLAGS | sed 's,-g,,'`
    CXXFLAGS=`echo $CXXFLAGS | sed 's,-g,,'`
  else
    case $CXXFLAGS in
    *-g*) ;;
    *)    CXXFLAGS="$CXXFLAGS -g" ;;
    esac
    case $CFLAGS in
    *-g*) ;;
    *)    CFLAGS="$CFLAGS -g" ;;
    esac
  fi
  AC_MSG_RESULT($enable_debug 'CFLAGS=$CFLAGS')
])

dnl ------------------------------------------------------------------
AC_DEFUN([AC_OPTIMIZATION],
[
  AC_REQUIRE([AC_PROG_CC])
  AC_REQUIRE([AC_PROG_CXX])

  AC_ARG_ENABLE(optimization,
    [AC_HELP_STRING([--enable-optimization],
	            [Enable optimization of objects])])

  AC_MSG_CHECKING(for optimiztion level)

  changequote(<<, >>)dnl
  if test "x$enable_optimization" = "xno" ; then
    CFLAGS=`echo   $CFLAGS   | sed 's,-O\([0-9][0-9]*\|\),,'`
    CXXFLAGS=`echo $CXXFLAGS | sed 's,-O\([0-9][0-9]*\|\),,'`
  elif test "x$enable_optimization" = "xyes" || 
	test "x$enable_optimization" = "x" ; then
    case $CXXFLAGS in
    *-O*) ;;
    *)    CXXFLAGS="$CXXFLAGS -O2" ;;
    esac
    case $CFLAGS in
    *-O*) ;;
    *)    CFLAGS="$CXXFLAGS -O2" ;;
    esac
  else
    CFLAGS=`echo   $CFLAGS   | sed "s,-O\([0-9][0-9]*\|\),-O$enable_optimization,"`
    CXXFLAGS=`echo $CXXFLAGS | sed "s,-O\([0-9][0-9]*\|\),-O$enable_optimization,"`
  fi
  changequote([, ])dnl
  AC_MSG_RESULT($enable_optimization 'CFLAGS=$CFLAGS')
])

dnl ------------------------------------------------------------------
dnl
dnl $Id: acinclude.m4,v 1.2 2004-12-15 17:00:57 cholm Exp $
dnl $Author: cholm $
dnl $Date: 2004-12-15 17:00:57 $
dnl
dnl Autoconf macro to check for existence or ROOT on the system
dnl Synopsis:
dnl
dnl  ROOT_PATH([MINIMUM-VERSION, [ACTION-IF-FOUND, [ACTION-IF-NOT-FOUND]]])
dnl
dnl Some examples: 
dnl 
dnl    ROOT_PATH(3.03/05, , AC_MSG_ERROR(Your ROOT version is too old))
dnl    ROOT_PATH(, AC_DEFINE([HAVE_ROOT]))
dnl 
dnl The macro defines the following substitution variables
dnl
dnl    ROOTCONF           full path to root-config
dnl    ROOTEXEC           full path to root
dnl    ROOTCINT           full path to rootcint
dnl    ROOTLIBDIR         Where the ROOT libraries are 
dnl    ROOTINCDIR         Where the ROOT headers are 
dnl    ROOTCFLAGS         Extra compiler flags
dnl    ROOTLIBS           ROOT basic libraries 
dnl    ROOTGLIBS          ROOT basic + GUI libraries
dnl    ROOTAUXLIBS        Auxilary libraries and linker flags for ROOT
dnl    ROOTAUXCFLAGS      Auxilary compiler flags 
dnl    ROOTRPATH          Same as ROOTLIBDIR
dnl
dnl The macro will fail if root-config and rootcint isn't found.
dnl
dnl Christian Holm Christensen <cholm@nbi.dk>
dnl
AC_DEFUN([ROOT_PATH],
[
  AC_ARG_WITH(rootsys,
  [  --with-rootsys          top of the ROOT installation directory],
    user_rootsys=$withval,
    user_rootsys="none")
  if test ! x"$user_rootsys" = xnone; then
    rootbin="$user_rootsys/bin"
  elif test ! x"$ROOTSYS" = x ; then 
    rootbin="$ROOTSYS/bin"
  else 
   rootbin=$PATH
  fi
  AC_PATH_PROG(ROOTCONF, root-config , no, $rootbin)
  AC_PATH_PROG(ROOTEXEC, root , no, $rootbin)
  AC_PATH_PROG(ROOTCINT, rootcint , no, $rootbin)
	
  if test ! x"$ROOTCONF" = "xno" && \
     test ! x"$ROOTCINT" = "xno" ; then 

    # define some variables 
    ROOTLIBDIR=`$ROOTCONF --libdir`
    ROOTINCDIR=`$ROOTCONF --incdir`
    ROOTCFLAGS=`$ROOTCONF --noauxcflags --cflags` 
    ROOTLIBS=`$ROOTCONF --noauxlibs --noldflags --libs`
    ROOTGLIBS=`$ROOTCONF --noauxlibs --noldflags --glibs`
    ROOTAUXCFLAGS=`$ROOTCONF --auxcflags`
    ROOTAUXLIBS=`$ROOTCONF --auxlibs`
    ROOTRPATH=$ROOTLIBDIR
	
    if test $1 ; then 
      AC_MSG_CHECKING(wether ROOT version >= [$1])
      vers=`$ROOTCONF --version | tr './' ' ' | awk 'BEGIN { FS = " "; } { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
      requ=`echo $1 | tr './' ' ' | awk 'BEGIN { FS = " "; } { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
      if test $vers -lt $requ ; then 
        AC_MSG_RESULT(no)
	no_root="yes"
      else 
        AC_MSG_RESULT(yes)
      fi
    fi
  else
    # otherwise, we say no_root
    no_root="yes"
  fi

  AC_SUBST(ROOTLIBDIR)
  AC_SUBST(ROOTINCDIR)
  AC_SUBST(ROOTCFLAGS)
  AC_SUBST(ROOTLIBS)
  AC_SUBST(ROOTGLIBS) 
  AC_SUBST(ROOTAUXLIBS)
  AC_SUBST(ROOTAUXCFLAGS)
  AC_SUBST(ROOTRPATH)

  if test "x$no_root" = "x" ; then 
    ifelse([$2], , :, [$2])     
  else 
    ifelse([$3], , :, [$3])     
  fi
])

dnl -*- mode: Autoconf -*- 
dnl
dnl $Id: acinclude.m4,v 1.2 2004-12-15 17:00:57 cholm Exp $ 
dnl  
dnl  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk> 
dnl
dnl  This library is free software; you can redistribute it and/or 
dnl  modify it under the terms of the GNU Lesser General Public License 
dnl  as published by the Free Software Foundation; either version 2.1 
dnl  of the License, or (at your option) any later version. 
dnl
dnl  This library is distributed in the hope that it will be useful, 
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of 
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
dnl  Lesser General Public License for more details. 
dnl 
dnl  You should have received a copy of the GNU Lesser General Public 
dnl  License along with this library; if not, write to the Free 
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
dnl  02111-1307 USA 
dnl
dnl AC_ROOT_FRAMEWORK([MINIMUM-VERSION 
dnl                   [,ACTION-IF_FOUND 
dnl                    [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_ROOT_FRAMEWORK],
[
    AC_ARG_WITH([framework-prefix],
        [AC_HELP_STRING([--with-framework-prefix],
		[Prefix where Framework is installed])],
        framework_prefix=$withval, framework_prefix="")

    if test "x${FRAMEWORK_CONFIG+set}" != xset ; then 
        if test "x$framework_prefix" != "x" ; then 
	    FRAMEWORK_CONFIG=$framework_prefix/bin/framework-config
	fi
    fi   
    AC_ARG_WITH([framework-url],
        [AC_HELP_STRING([--with-framework-url],
		[Base URL where the Framework dodumentation is installed])],
        framework_url=$withval, framework_url="")

    if test "x${FRAMEWORK_CONFIG+set}" != xset ; then 
        if test "x$framework_prefix" != "x" ; then 
	    FRAMEWORK_CONFIG=$framework_prefix/bin/framework-config
	fi
    fi   
    AC_PATH_PROG(FRAMEWORK_CONFIG, framework-config, no)
    framework_min_version=ifelse([$1], ,0.1,$1)
    
    AC_MSG_CHECKING(for Framework version >= $framework_min_version)

    framework_found=no    
    if test "x$FRAMEWORK_CONFIG" != "xno" ; then 
       FRAMEWORK_CPPFLAGS=`$FRAMEWORK_CONFIG --cppflags`
       FRAMEWORK_INCLUDEDIR=`$FRAMEWORK_CONFIG --includedir`
       FRAMEWORK_LIBS=`$FRAMEWORK_CONFIG --libs`
       FRAMEWORK_LIBDIR=`$FRAMEWORK_CONFIG --libdir`
       FRAMEWORK_LDFLAGS=`$FRAMEWORK_CONFIG --ldflags`
       FRAMEWORK_PREFIX=`$FRAMEWORK_CONFIG --prefix`
       
       framework_version=`$FRAMEWORK_CONFIG -V` 
       framework_vers=`echo $framework_version | \
         awk 'BEGIN { FS = " "; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       framework_regu=`echo $framework_min_version | \
         awk 'BEGIN { FS = " "; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       if test $framework_vers -ge $framework_regu ; then 
            framework_found=yes
       fi
    fi
    AC_MSG_RESULT($framework_found - is $framework_version) 
    AC_MSG_CHECKING(where the Framework documentation is installed)
    if test "x$framework_url" = "x" && \
	test ! "x$FRAMEWORK_PREFIX" = "x" ; then 
       FRAMEWORK_URL=${FRAMEWORK_PREFIX}/share/doc/framework/html
    else 
	FRAMEWORK_URL=$framework_url
    fi	
    AC_MSG_RESULT($FRAMEWORK_URL)
  
    if test "x$framework_found" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST(FRAMEWORK_URL)
    AC_SUBST(FRAMEWORK_PREFIX)
    AC_SUBST(FRAMEWORK_CPPFLAGS)
    AC_SUBST(FRAMEWORK_INCLUDEDIR)
    AC_SUBST(FRAMEWORK_LDFLAGS)
    AC_SUBST(FRAMEWORK_LIBDIR)
    AC_SUBST(FRAMEWORK_LIBS)
])

dnl
dnl EOF
dnl 
dnl -*- mode: Autoconf -*- 
dnl
dnl $Id: acinclude.m4,v 1.2 2004-12-15 17:00:57 cholm Exp $ 
dnl  
dnl  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk> 
dnl
dnl  This library is free software; you can redistribute it and/or 
dnl  modify it under the terms of the GNU Lesser General Public License 
dnl  as published by the Free Software Foundation; either version 2.1 
dnl  of the License, or (at your option) any later version. 
dnl
dnl  This library is distributed in the hope that it will be useful, 
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of 
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
dnl  Lesser General Public License for more details. 
dnl 
dnl  You should have received a copy of the GNU Lesser General Public 
dnl  License along with this library; if not, write to the Free 
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
dnl  02111-1307 USA 
dnl
dnl AC_ROOT_SIMULATION([MINIMUM-VERSION 
dnl                   [,ACTION-IF_FOUND 
dnl                    [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_ROOT_SIMULATION],
[
    AC_REQUIRE([AC_ROOT_FRAMEWORK])
    AC_ARG_WITH([simulation-prefix],
        [AC_HELP_STRING([--with-simulation-prefix],
		[Prefix where Simulation is installed])],
        simulation_prefix=$withval, simulation_prefix="")

    AC_ARG_WITH([simulation-url],
        [AC_HELP_STRING([--with-simulation-url],
		[Base URL where the Simulation dodumentation is installed])],
        simulation_url=$withval, simulation_url="")
    if test "x${SIMULATION_CONFIG+set}" != xset ; then 
        if test "x$simulation_prefix" != "x" ; then 
	    SIMULATION_CONFIG=$simulation_prefix/bin/simulation-config
	fi
    fi   
    AC_PATH_PROG(SIMULATION_CONFIG, simulation-config, no)
    simulation_min_version=ifelse([$1], ,0.1,$1)
    
    AC_MSG_CHECKING(for Simulation version >= $simulation_min_version)

    simulation_found=no    
    if test "x$SIMULATION_CONFIG" != "xno" ; then 
       SIMULATION_CPPFLAGS=`$SIMULATION_CONFIG --cppflags`
       SIMULATION_INCLUDEDIR=`$SIMULATION_CONFIG --includedir`
       SIMULATION_LIBS=`$SIMULATION_CONFIG --libs`
       SIMULATION_LIBDIR=`$SIMULATION_CONFIG --libdir`
       SIMULATION_LDFLAGS=`$SIMULATION_CONFIG --ldflags`
       SIMULATION_PREFIX=`$SIMULATION_CONFIG --prefix`
       
       simulation_version=`$SIMULATION_CONFIG -V` 
       simulation_vers=`echo $simulation_version | \
         awk 'BEGIN { FS = " "; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       simulation_regu=`echo $simulation_min_version | \
         awk 'BEGIN { FS = " "; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       if test $simulation_vers -ge $simulation_regu ; then 
            simulation_found=yes
       fi
    fi
    AC_MSG_RESULT($simulation_found - is $simulation_version) 
    AC_MSG_CHECKING(where the Framework documentation is installed)
    if test "x$simulation_url" = "x" && \
	test ! "x$SIMULATION_PREFIX" = "x" ; then 
       SIMULATION_URL=${SIMULATION_PREFIX}/share/doc/simulation/html
    else 
	SIMULATION_URL=$simulation_url
    fi	
    AC_MSG_RESULT($SIMULATION_URL)
   
    if test "x$simulation_found" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST(SIMULATION_URL)
    AC_SUBST(SIMULATION_PREFIX)
    AC_SUBST(SIMULATION_CPPFLAGS)
    AC_SUBST(SIMULATION_INCLUDEDIR)
    AC_SUBST(SIMULATION_LDFLAGS)
    AC_SUBST(SIMULATION_LIBDIR)
    AC_SUBST(SIMULATION_LIBS)
])

dnl
dnl EOF
dnl 

# EOF
#
