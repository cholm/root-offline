#!/usr/bin/make -f
# -*- mode: makefile -*-
# $Id: autogen.sh,v 1.4 2005-12-02 15:58:46 cholm Exp $
#
#    Example offline project
#    Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
#
#   This library is free software; you can redistribute it and/or
#   modify it under the terms of the GNU Lesser General Public License 
#   as published by the Free Software Foundation; either version 2 of 
#   the License, or (at your option) any later version.  
#
#   This library is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   Lesser General Public License for more details. 
#
#   You should have received a copy of the GNU Lesser General Public
#   License along with this library; if not, write to the Free
#   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
#   MA 02111-1307  USA  
#
PREFIX		= $(HOME)/tmp
CONFFLAGS	= --prefix=$(PREFIX) 
AUTOMAKEVERSION = -1.9
PACKAGE		:= $(strip $(shell grep AC_INIT configure.ac |\
			sed 's/.*([^,]*,\([^,]*\),[^,]*,\[*\(.*\)\]*).*/\2/'))
VERSION		:= $(strip $(shell grep AC_INIT configure.ac |\
			sed 's/.*([^,]*,\([^,]*\),[^,]*,\(.*\)).*/\1/'))

all:	setup
	./configure $(CONFFLAGS)

noopt:	setup
	./configure --disable-optimization $(CONFFLAGS)

setup:	ChangeLog configure 

config/ltmain.sh:
	libtoolize --automake --copy

config/config.hh.in:configure.ac 
	autoheader

aclocal.m4: configure.ac acinclude.m4 config/ltmain.sh
	aclocal$(AUTOMAKEVERSION) -I .

Makefile.in:configure.ac Makefile.am aclocal.m4 
	automake$(AUTOMAKEVERSION) --add-missing  --copy

configure:Makefile.in configure.ac 
	autoconf 


make:	all
	$(MAKE) -f Makefile 

dists:
	$(MAKE) dist
	$(MAKE) tar-ball -C doc


ChangeLog:
	rm -f $@
	touch $@
	rcs2log > $@

show:
	@echo "$(PACKAGE) version $(VERSION)"
clean: 
	-if test -f Makefile ; then $(MAKE) clean ; fi 
	find . -name Makefile.in 	| xargs rm -f 
	find . -name "*~" 		| xargs rm -f 
	find . -name core 		| xargs rm -f 
	find . -name .libs 		| xargs rm -rf 
	find . -name .deps 		| xargs rm -rf 
	find . -name "*.lo" 		| xargs rm -f 
	find . -name "*.o" 		| xargs rm -f 
	find . -name "*.la" 		| xargs rm -rf
	find . -name "*.log" 		| xargs rm -rf
	find . -name "semantic.cache" 	| xargs rm -rf
	find . -name "*Dict.*" 		| xargs rm -rf

	rm -f   Makefile 		\
		offline/Makefile	\
		doc/Makefile
	rm -f 	config/missing       	\
	  	config/mkinstalldirs 	\
	  	config/ltmain.sh     	\
	  	config/config.guess 	\
		config/config.sub 	\
		config/install-sh 	\
		config/ltconfig 	\
		config/config.hh 	\
		config/config.hh.in 	\
		config/stamp-h		\
		config/stamp-h.in	\
		config/depcomp		\
		config/stamp-h1

	rm -rf 	aclocal.m4 		\
		autom4te.cache		\
		config.cache 		\
		config.status  		\
		config.log 		\
		configure 		\
		INSTALL 		\
		ChangeLog		\
		configure-stamp		\
		build-stamp		\
		libtool			\
		libtool.m4		\
		ltoptions.m4		\
		ltsugar.m4		\
		ltversion.m4		\
		*.root 			\
		.rootrc 		\
		gphysi.dat		\
		log

	rm -rf  doc/html			\
		doc/xml				\
		doc/latex			\
		doc/man				\
		doc/doxyconfig			\
		doc/header.html			\
		doc/$(PACKAGE).tags

	rm -rf  $(PACKAGE)-*.tar.gz 	

#
# EOF
#
