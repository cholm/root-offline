//
// $Id: Examples.cxx,v 1.8 2006-04-03 15:19:47 cholm Exp $
//
//   Example offline project
//   Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//   This library is free software; you can redistribute it and/or
//   modify it under the terms of the GNU Lesser General Public License 
//   as published by the Free Software Foundation; either version 2 of 
//   the License, or (at your option) any later version.  
//
//   This library is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//   Lesser General Public License for more details. 
//
//   You should have received a copy of the GNU Lesser General Public
//   License along with this library; if not, write to the Free
//   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
//   MA 02111-1307  USA  
//
//
/** @file   
    @author Christian Holm
    @date   Sun Sep 15 16:29:24 2002
    @brief  Examples. */


/** @page examples Examples 
    Here are the various examples.
 */

/** @example geometry/GeomConfig.C
    @par Configuration for geometry. */

/** @example geometry/GeomRun.C
    @par Create geometry in a file.
    Run in batch mode with: 
    @verbatim
    root -l -b -q GeomRun.C
    @endverbatim
 */

/** @example geometry/Draw.C
    @par Draw all or a single detector geometry. */

/** @example montecarlo/McConfig.C
    @par Configuration for simulation. */

/** @example montecarlo/McRun.C
    @par Run a simulation. 
    Run in batch mode with: 
    @verbatim
    root -l -b -q McRun.C
    @endverbatim
 */

/** @example digitization/DigiConfig.C
    @par Configuration for digitization. */

/** @example digitization/DigiRun.C
    @par Run digitization. 
    Run in batch mode with: 
    @verbatim
    root -l -b -q DigiRun.C
    @endverbatim
 */

/** @example reconstruction/RecoConfig.C
    @par Configuration for reconstruction. */

/** @example reconstruction/RecoRun.C
    @par Run reconstruction. 
    Run in batch mode with: 
    @verbatim
    root -l -b -q RecoRun.C
    @endverbatim
 */

//
// EOF
//
