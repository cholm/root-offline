//
// $Id: Mainpage.cxx,v 1.9 2006-04-03 15:19:47 cholm Exp $
//
//   Example offline project
//   Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//   This library is free software; you can redistribute it and/or
//   modify it under the terms of the GNU Lesser General Public License 
//   as published by the Free Software Foundation; either version 2 of 
//   the License, or (at your option) any later version.  
//
//   This library is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//   Lesser General Public License for more details. 
//
//   You should have received a copy of the GNU Lesser General Public
//   License along with this library; if not, write to the Free
//   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
//   MA 02111-1307  USA  
//
//
/** @file   
    @author Christian Holm
    @date   Sun Sep 15 16:29:24 2002
    @brief  Title page documentation. */
/** @mainpage Example Offline Project

    @section off_intro Introduction. 

    <center>
    @image html display3dcutwhite.png 
    </center>

    This is a small example of how I would organise an offline project
    for an experiment.   The project is build on top of the 
    <a href="http://cern.ch/cholm/root/#framework">Framework</a>
    package for maximum flexibility.   

    The example covers the following 
    <ul>
      <li> @link Data Data @endlink formats </li>
      <li> @link Geometry Geometry @endlink parameters </li>
      <li> @link Montecarlo Simulation @endlink of the experiment </li>
      <li> @link Digitization Digitization @endlink of the simulated
        data </li>
      <li> @link Reconstruction Reconstruction @endlink of the
        (digitized simulated) data </li>
   </ul>
   
   One can run a full chain on 10 events by doing 
   @verbatim
   prompt> root -l -b -q offline/geometry/GeomRun.C 
   prompt> root -l -b -q offline/montecarlo/McRun.C 
   prompt> root -l -b -q offline/digitization/DigiRun.C
   prompt> root -l -b -q offline/reconstruction/RecoRun.C
   @endverbatim 
   
   This will produce the files @c geom.root, @c hits.root, 
   @c digits.root, and @c reco.root, each of which contains a tree
   with the relevant data. The tree's have one branch per defined
   component.  

   Note, that the geometry is completely decoupled from the rest of
   the code, meaning that the simulation, digitization, and
   reconstruction code only uses the finished geometry (via 
   @c %TGeoManager).  One therefor has the choice of making the
   geometry @e in a given step, or use a predefined geometry.  Hence,
   one could imagine a further step, say 
   @verbatim 
   prompt> root -l -b -q offline/alignment/AlignRun.C 
   @endverbatim 
   injected @e anywhere in the chain above, to correct the ideal
   geometry (as created by the code) for real-life differences. 

   The experiment used for inspiration was 
   <a href="http://cern.ch/alice">ALICE</a>, and the geometry is an
   extreme simplifcation of ALICE detectors, with the exception of the
   FMD detector which is fully fleshed out.   The answer to the
   obvious question why that detector gets preferential treatment, is
   that I'm working on the FMD, and I've used this small example as a
   testing ground for various things. 
   
   Use any @c %TGenerator derived class you'd like.  For example 
   @c %THijing, @c %THijingFlow, or @c %THijingPara from 
   <a href="http://cern.ch/cholm/root/#thijing">here</a>.

   @image html display3d.png 

*/


//
// EOF
//
