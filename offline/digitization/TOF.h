// -*- mode: c++ -*- 
//____________________________________________________________________ 
//  
// $Id: TOF.h,v 1.7 2006-11-08 01:41:43 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    digitization/TOF.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Tue Dec 14 15:32:30 2004
    @brief   Declaration of the TOF digitizer 
*/
#ifndef Digitization_TOF
#define Digitization_TOF
#ifndef Framework_Task
# include <framework/Task.h>
#endif
class TRandom;


namespace Digitization 
{
  /** @class TOF digitization/TOF.h <digitization/TOF.h>
      @brief Digitize TOF Hits and make TOF digits 
      @ingroup digitization tof
   */
  class TOF : public Framework::Task
  {
  private:
    /** Random number generator */
    TRandom* fRandom;
    /** Energy deposited by a MIP */
    Double_t fMIPEnergy;
    /** ADC MIP dynamic range */
    Int_t    fMIPRange;
    /** ADC dynamic range */
    Int_t    fADCRange;
    /** time to TDC conversion factor */
    Double_t fTDCConv;
    /** time to TDC offset */
    Double_t fTDCDelay;
    /** time to TDC gate */
    Double_t fTDCGate;
    /** Pedestal mean */
    Double_t fPedestal;
    /** Pedestal spread */
    Double_t fPedestalWidth;
    Int_t fNSector;
    Int_t fNRing;
  public:
    /** Create an TOF digitizer */
    TOF();
    virtual ~TOF() {}
    
    /** Set MIP energy 
	@param x Energy deposisted by a MIP */
    void     SetMIPEnergy(Double_t x=10*1.936)  { fMIPEnergy = x; }    //*MENU*
    /** Set dynamic range 
	@param x ADC dynamic MIP range */
    void     SetMIPRange(Int_t x=5)      { fMIPRange = x; }    //*MENU*
    /** Set dynamic range 
	@param x ADC range */
    void     SetADCRange(Int_t x=1024) { fADCRange = x; }    //*MENU*
    /** Set TDC factor 
	@param x Time to TDC conversion */
    void     SetTDCConv(Double_t x=1e8)    { fTDCConv = x; }   //*MENU*
    /** Set Delay 
	@param x TDC delay */
    void     SetTDCDelay(Double_t x=1e-9) { fTDCDelay = x; }   //*MENU*
    /** Set Gate 
	@param x TDC gate */
    void     SetTDCGate(Double_t x=1e-7)      { fTDCGate = x; }   //*MENU*
    /** Set Pedestal parameters 
	@param m Pedestal mean 
	@param w Pedestal width  */
    void     SetPedestal(Double_t m=10, Double_t w=.5) { 
      fPedestal = m; fPedestalWidth = w; 
    }
    /** @return  Energy deposisted by a MIP */
    Double_t GetMIPEnergy() const { return fMIPEnergy; } 
    /** @return  ADC dynamic MIP range */
    Int_t    GetMIPRange()  const { return fMIPRange; }
    /** @return  ADC range */
    Int_t    GetADCRange()  const { return fADCRange; }
    /** @return  Time to TDC conversion */
    Double_t GetTDCConv()   const { return fTDCConv; }
    /** @return  TDC delay */
    Double_t GetTDCDelay()   const { return fTDCDelay; }
    /** @return  TDC gate */
    Double_t GetTDCGate()   const { return fTDCGate; }

    /** Register output branch, etc. 
	@param option Not used  */
    virtual void Register(Option_t* option=""); 
    /** Create digits from hits 
	@param option Not used. */
    virtual void Exec(Option_t* option=""); 
    ClassDef(TOF,0) // Write Point to TTree and TFolder
  };
}

#endif
//____________________________________________________________________ 
//  
// EOF
//
