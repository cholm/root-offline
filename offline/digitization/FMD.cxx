//____________________________________________________________________ 
//  
// $Id: FMD.cxx,v 1.10 2007-09-28 14:50:40 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    digitization/FMD.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec 16 01:36:07 2004
    @brief   Implementation of Digitization::FMD
*/
#include <TClonesArray.h>
#include <TRandom.h>
#include <TBranch.h>
#include <TTree.h>
#include <TFolder.h>
#include <TGeoVolume.h>
#include <TGeoManager.h>
#include "digitization/FMD.h"
#include "data/FMDDigit.h"
#include "data/FMDHit.h"
#include "data/FMDMap.h"


  
//____________________________________________________________________
ClassImp(Digitization::FMD);

//____________________________________________________________________
Digitization::FMD::FMD()
  : Framework::Task("FMD", "FMD digtizer")
{
  // Default constructor
  fRandom = new TRandom;
  fCache  = new TClonesArray("Data::FMDDigit");
  SetMIPEnergy();
  SetMIPRange();
  SetADCRange();
  SetPedestal();
  fNSectors[0] = fNSectors[1] = 0;
  fNStrips[0]  = fNStrips[1]  = 0;
}

//____________________________________________________________________
void 
Digitization::FMD::Register(Option_t* option) 
{
  // Register a clones array as a branch and in a folder 
  Debug(5, "Register", "register output branch");
  TTree* tree = 0;
  if (!(tree = GetBaseTree("Digits"))) return;
  fBranch = tree->Branch(GetName(), &fCache);

  for (Int_t i = 0; i < 2; i++) {
    Char_t c = (i == 0 ? 'I' : 'O');
    TGeoVolume* vol = gGeoManager->GetVolume(Form("F%cSE", c));
    if (!vol) return;
    Double_t phi1, phi2;
    vol->GetShape()->GetAxisRange(2, phi1, phi2);
    Double_t theta  = (phi2 - phi1);
    fNSectors[i]    = Int_t(360 / theta);
    fNStrips[i]     = vol->GetNdaughters();
  }
  Verbose(1, "Register", "# of sectors: %d/%d - # of strips %d/%d", 
	  fNSectors[0], fNSectors[1], fNStrips[0], fNStrips[1]);
}

//____________________________________________________________________
void 
Digitization::FMD::Exec(Option_t* option) 
{
  TFolder* folder = GetBaseFolder();
  if (!folder)
    return;
  folder = static_cast<TFolder*>(folder->FindObject("FMDHits"));
  if (!folder) {
    Debug(1, "Exec", "couldn't find the hits folder");
    return;
  }
  TClonesArray* hits =
    static_cast<TClonesArray*>(folder->FindObject("Data::FMDHits"));
  if (!hits) {
    Stop("Exec", "Couldn't find hits array");
    return;
  }
  
  Int_t n = hits->GetEntries();
  Verbose(1, "Exec", "got %d space points", n);

  Data::FMDDoubleMap sum(3, 2, 40, 512);
  fCache->Clear();
  TClonesArray& cache = *static_cast<TClonesArray*>(fCache);

  for (Int_t i = 0; i < n; i++) {
    Data::FMDHit* hit = static_cast<Data::FMDHit*>(hits->At(i));
    Double_t elo = hit->EnergyLoss();
    sum[*hit] += elo;
    Debug(10, "Exec", "Adding %f to eloss %f in %s", 
	  elo, sum[*hit], hit->Name());
  }
  
  Int_t i = 0;
  for (Int_t d=1; d <= 3; d++) {
    for (Int_t ir=0; ir < 2; ir++) {
      if (d == 1 && ir == 1) break;
      Char_t r;
      switch (ir) {
      case 0: r = 'I'; break;
      case 1: r = 'O'; break;
      }
      Int_t nsec = fNSectors[ir]; // ring->GetNSectors();
      Int_t nstr = fNStrips[ir];  // ring->GetNStrips();
      for (Int_t sec = 0; sec < nsec; sec++) {
	for (Int_t str = 0; str < nstr; str++) {
	  Data::FMDIndex idx(d,r,sec,str);
	  Double_t edep = 0;
	  if (sum.Has(idx)) edep   = sum[idx];
	  Double_t nMip   = edep / fMIPEnergy;
	  Double_t ped    = fRandom->Gaus(fPedestal, fPedestalWidth);
	  Int_t    counts = Int_t(nMip / fMIPRange * fADCRange + ped);
	  if (edep > 0) {
	    Debug(10, "Exec", "Adding digit at %s = %d "
		  "= %f / (%f * %d) * %d + %f",
		  idx.Name(), counts, edep, fMIPEnergy, fMIPRange, 
		  fADCRange, ped);
	  }
	  new(cache[i]) Data::FMDDigit(idx, counts);
	  i++;
	}
      }
    }
  }
}


//____________________________________________________________________ 
//  
// EOF
//
