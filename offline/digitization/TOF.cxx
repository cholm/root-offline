//____________________________________________________________________ 
//  
// $Id: TOF.cxx,v 1.10 2007-09-28 14:50:40 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    digitization/TOF.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec 16 01:36:07 2004
    @brief   Implementation of Digitization::TOF
*/
#include <TClonesArray.h>
#include <TRandom.h>
#include <TBranch.h>
#include <TTree.h>
#include <TGeoVolume.h>
#include <TGeoManager.h>
#include <TFolder.h>
#include "digitization/TOF.h"
#include "data/TOFDigit.h"
#include "data/TOFHit.h"

//____________________________________________________________________
namespace 
{
  /** @struct TOFCache 
      @brief Internal class used in Digitization::TOF
   */
  struct TOFCache 
  {
    /** The data */
    Double_t* fData;
    /** Max detector */
    UInt_t    fNSector;
    /** Max rings */
    UInt_t    fNRing;
    
    /** Create a cache object 
	@param nsec  Max sectors
	@param nring Max rings
	@param nstr  Max strips */
    TOFCache(UInt_t nsec, UInt_t nring) 
      : fData(0), 
	fNSector(nsec),
	fNRing(nring)
    {
      fData = new Double_t[nsec*nring];
      Reset(0);
    }
    /** Reset data to @a val
	@param val No value */
    void Reset(Double_t val=0) 
    {
      for (Int_t i = 0; i < fNSector * fNRing; i++) fData[i] = val;
    }
    /** Get/Set data at @f$ [sec,ring,str]@f$ 
	@return  @f$ D_{sec,ring,str}@f$ */
    Double_t& 
    operator()(UShort_t sec, UShort_t ring) 
    {
      return fData[CalcIndex(sec, ring)];
    }
    /** Get/Set data at @f$ [sec,ring,str]@f$ 
	@return  @f$ D_{sec,ring,str}@f$ */
    const Double_t& 
    operator()(UShort_t sec, UShort_t ring) const
    {
      return fData[CalcIndex(sec, ring)];
    }
    /** Calculate real index 
	@param sec  Sector #
	@param ring Ring   #
	@param str  Strip #
	@return Real index into data array  */
    UInt_t CalcIndex(UShort_t sec, UShort_t ring) const
    {
      UInt_t idx = (sec + fNSector * ring);
      if (idx >= fNSector * fNRing) {
	Error("TOFCache::CalcIndex", "Index (%c,%d) out of bounds", 
	      sec, ring);
	return 0;
      }
      return idx;
    }
  };
}
//____________________________________________________________________
ClassImp(Digitization::TOF);

//____________________________________________________________________
Digitization::TOF::TOF()
  : Framework::Task("TOF", "TOF digtizer")
{
  // Default constructor
  fRandom = new TRandom;
  fCache  = new TClonesArray("Data::TOFDigit");
  SetMIPEnergy();
  SetMIPRange();
  SetADCRange();
  SetTDCConv();
  SetTDCDelay();
  SetTDCGate();
  SetPedestal();
}

//____________________________________________________________________
void 
Digitization::TOF::Register(Option_t* option) 
{
  // Register a clones array as a branch and in a folder 
  Debug(5, "Register", "register output branch");
  TTree* tree = 0;
  if (!(tree = GetBaseTree("Digits"))) return;
  fBranch = tree->Branch(GetName(), &fCache);

  TGeoVolume* tof = gGeoManager->GetVolume("TOF");
  if (!tof) return;
  fNRing     = tof->GetNdaughters();

  TGeoVolume* tofr = gGeoManager->GetVolume("TOFR");
  if (!tofr) return;
  fNSector    = tofr->GetNdaughters();
}

//____________________________________________________________________
void 
Digitization::TOF::Exec(Option_t* option) 
{
  TFolder* folder = GetBaseFolder();
  if (!folder)
    return;
  folder = static_cast<TFolder*>(folder->FindObject("TOFHits"));
  if (!folder) {
    Warning("Exec", "couldn't find the hits folder");
    return;
  }
  TClonesArray* hits =
    static_cast<TClonesArray*>(folder->FindObject("Data::TOFHits"));
  if (!hits) {
    Stop("Exec", "Couldn't find hits array");
    return;
  }

  Int_t n = hits->GetEntries();
  Verbose(1, "Exec", "got %d space points", n);

  fCache->Clear();
  TClonesArray& cache = *static_cast<TClonesArray*>(fCache);


  static TOFCache energy(fNSector,fNRing);
  static TOFCache time(fNSector,fNRing);
  energy.Reset();
  time.Reset(1 / fTDCDelay);

  // Find first hit 
  for (Int_t i = 0; i < n; i++) {
    Data::TOFHit* hit = static_cast<Data::TOFHit*>(hits->At(i));
    Float_t oldT = time(hit->fSector, hit->fRing);
    Debug(30, "Exec", "old time: %g new time: %g", oldT, hit->T());
    time(hit->fSector-1, hit->fRing-1) = TMath::Min(oldT, hit->T());
  }
  // Only use hits within time gate 
  for (Int_t i = 0; i < n; i++) {
    Data::TOFHit* hit = static_cast<Data::TOFHit*>(hits->At(i));
    if (hit->T() > time(hit->fSector, hit->fRing) + fTDCGate) continue;
    energy(hit->fSector, hit->fRing) += hit->EnergyLoss();
  }
  
  Int_t ndigit = 0;
  for (Int_t sec = 0; sec < fNSector; sec++) {
    for (Int_t ring = 0; ring < fNRing; ring++) {
      Double_t edep   = energy(sec,ring);
      Double_t nMip   = edep / fMIPEnergy;
      Double_t ped    = fRandom->Gaus(fPedestal, fPedestalWidth);
      Int_t    counts = Int_t(nMip / fMIPRange * fADCRange + ped);
      Float_t  t      = time(sec,ring);
      if (t >= .1 / fTDCDelay) t = 0;
      Int_t    tdc    = Int_t((t - fTDCDelay) * fTDCConv);
      Debug(10, "Exec", "t->TDC: %g -> %d = (%g - %g) * %g", 
	    t, tdc, t, fTDCDelay, fTDCConv);
      Debug(10, "Exec", "e->ADC: %g -> %d = %g / (%g * %d) * %d + %g", 
	    edep, counts, edep, fMIPEnergy, fMIPRange, fADCRange, ped);
      if (tdc < 0)    tdc = 0;
      new(cache[ndigit]) Data::TOFDigit(sec, ring, counts, tdc);
      ndigit++;
    }
  }
}


//____________________________________________________________________ 
//  
// EOF
//
