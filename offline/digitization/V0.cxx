//____________________________________________________________________ 
//  
// $Id: V0.cxx,v 1.8 2007-09-28 14:50:40 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    digitization/V0.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec 16 01:36:07 2004
    @brief   Implementation of Digitization::V0
*/
#include <TClonesArray.h>
#include <TRandom.h>
#include <TBranch.h>
#include <TTree.h>
#include <TFolder.h>
#include <TGeoVolume.h>
#include <TGeoManager.h>
#include "digitization/V0.h"
#include "data/V0Digit.h"
#include "data/V0Hit.h"
#include <iostream>

//____________________________________________________________________
namespace 
{
  /** @struct V0Cache 
      @brief Internal class used in Digitization::V0
   */
  struct V0Cache 
  {
    /** The data */
    Double_t* fData;
    /** Max detector */
    UInt_t    fNDetector;
    /** Max rings */
    UInt_t    fNRing;
    /** Max sectors */
    UInt_t    fNSector;
    
    /** Create a cache object 
	@param ndet  Max detectors
	@param nring Max rings
	@param nsec  Max sectors */
    V0Cache(UInt_t ndet, UInt_t nring, UInt_t nsec) 
      : fData(0), 
	fNDetector(ndet),
	fNRing(nring), 
	fNSector(nsec)
    {
      fData = new Double_t[ndet*nring*nsec];
      Reset(0);
    }
    /** Reset data to @a val
	@param val No value */
    void Reset(Double_t val=0) 
    {
      for (Int_t i = 0; i < fNDetector * fNRing * fNSector; i++) 
	fData[i] = val;
    }
    /** Get/Set data at @f$ [d,r,sec]@f$ 
	@return  @f$ D_{d,r,sec}@f$ */
    Double_t& 
    operator()(Char_t d, UShort_t r, UShort_t sec) 
    {
      return fData[CalcIndex(d, r, sec)];
    }
    /** Get data at @f$ [d,r,sec]@f$ 
	@return  @f$ D_{d,r,sec}@f$ */
    const Double_t& 
    operator()(Char_t d, UShort_t r, UShort_t sec)  const
    {
      return fData[CalcIndex(d, r, sec)];
    }
    /** Calculate real index 
	@param d    Detector ID
	@param r    Ring #
	@param sec  Sector #
	@return Real index into data array  */
    UInt_t CalcIndex(Char_t d, UShort_t r, UShort_t sec) const
    {
      UShort_t di  = (d == 'A' || d == 'a' ? 0 : 1);
      UInt_t   idx = (di + fNDetector * (r - 1 + fNRing * sec));
      if (idx >= fNDetector * fNRing * fNSector) {
	Error("V0Cache::CalcIndex", "Index (%c,%d,%d) out of bounds", 
	      d, r, sec);
	idx = 0;
      }
      return idx;
    }
  };
}
//____________________________________________________________________
ClassImp(Digitization::V0);

//____________________________________________________________________
Digitization::V0::V0()
  : Framework::Task("V0", "V0 digtizer")
{
  // Default constructor
  fRandom = new TRandom;
  fCache  = new TClonesArray("Data::V0Digit");
  SetMIPEnergy();
  SetMIPRange();
  SetADCRange();
  SetTDCConv();
  SetTDCDelay();
  SetTDCGate();
  SetPedestal();
}

//____________________________________________________________________
void 
Digitization::V0::Register(Option_t* option) 
{
  // Register a clones array as a branch and in a folder 
  Debug(5, "Register", "register output branch");
  TTree* tree = 0;
  if (!(tree = GetBaseTree("Digits"))) return;
  fBranch = tree->Branch(GetName(), &fCache);

  Int_t maxA=0, maxC=0;
  TGeoVolume* v0a = gGeoManager->GetVolume("V0A");
  if (!v0a) return;
  fNRingA.Set(v0a->GetNdaughters());
  for (Int_t i = 0; i < fNRingA.fN; i++) {
    TGeoVolume* v0ar = gGeoManager->GetVolume(Form("V0A%d",i+1));
    if (!v0ar) return;
    fNRingA[i] = v0ar->GetNdaughters();
    if (fNRingA[i] > maxA) maxA = fNRingA[i];
  }
  TGeoVolume* v0c = gGeoManager->GetVolume("V0C");
  if (!v0c) return;
  fNRingC.Set(v0c->GetNdaughters());
  for (Int_t i = 0; i < fNRingC.fN; i++) {
    TGeoVolume* v0cr = gGeoManager->GetVolume(Form("V0C%d",i+1));
    if (!v0cr) return;
    fNRingC[i] = v0cr->GetNdaughters();
    if (fNRingC[i] > maxC) maxC = fNRingC[i];
  }
  fMaxRing =   (fNRingA.fN > fNRingC.fN ? fNRingA.fN : fNRingC.fN);
  fMaxSector = (maxA > maxC ? maxA : maxC);

  
}

//____________________________________________________________________
void 
Digitization::V0::Exec(Option_t* option) 
{
  TFolder* folder = GetBaseFolder();
  if (!folder)
    return;
  folder = static_cast<TFolder*>(folder->FindObject("V0Hits"));
  if (!folder) {
    Warning("Exec", "couldn't find the hits folder");
    return;
  }
  TClonesArray* hits = 
    static_cast<TClonesArray*>(folder->FindObject("Data::V0Hits"));
  if (!hits) {
    Stop("Exec", "Couldn't find hits array");
    return;
  }

  Int_t n = hits->GetEntriesFast();
  Verbose(1, "Exec", "got %d space points", n);

  fCache->Clear();
  TClonesArray& cache = *static_cast<TClonesArray*>(fCache);

  static V0Cache energy(2,fMaxRing,fMaxSector);
  static V0Cache time(2,fMaxRing,fMaxSector);
  energy.Reset();
  time.Reset(1 / fTDCDelay);

  // Find first hit 
  Debug(10, "Exec", "Finding fastest hit in each element");
  for (Int_t i = 0; i < hits->GetEntriesFast(); i++) {
    Data::V0Hit* hit = static_cast<Data::V0Hit*>(hits->At(i));
    Float_t oldT = time(hit->fDetector, hit->fRing, hit->fSector);
    time(hit->fDetector, hit->fRing, hit->fSector) = 
      TMath::Min(oldT, hit->T());
  }
  // Only use hits within time gate 
  Debug(10, "Exec", "Adding ELoss for hits withing time gate");
  for (Int_t i = 0; i < hits->GetEntriesFast(); i++) {
    Data::V0Hit* hit = static_cast<Data::V0Hit*>(hits->At(i));
    if (hit->T()>time(hit->fDetector,hit->fRing,hit->fSector)+ fTDCGate) 
      continue;
    energy(hit->fDetector, hit->fRing, hit->fSector) += hit->EnergyLoss();
  }
  
  Int_t ndigit = 0;
  Debug(10, "Exec", "Creating digits");
  for (Int_t i = 0; i < 2; i++) {
    Char_t d;
    switch (i) {
    case 0: d = 'A'; break;
    case 1: d = 'C'; break;
    }
    TArrayI& nRing = (i == '0' ? fNRingA : fNRingC);
    for (Int_t ring = 1; ring <= nRing.fN; ring++) {
      Int_t nsectors = nRing[ring-1];
      for (Int_t sector = 0; sector < nsectors; sector++) {
	Double_t edep   = energy(d,ring,sector);
	Double_t nMip   = edep / fMIPEnergy;
	Double_t ped    = fRandom->Gaus(fPedestal, fPedestalWidth);
	Int_t    counts = Int_t(nMip / fMIPRange * fADCRange + ped);
	Double_t t      = time(d,ring,sector);
	if (t >= 1 / fTDCDelay) t = 0;
	Int_t    tdc    = Int_t((t - fTDCDelay) * fTDCConv);
	Debug(10, "Exec", "t->TDC: %g -> %d = (%g - %g) * %g", 
	      t, tdc, t, fTDCDelay, fTDCConv);
	Debug(10, "Exec", "e->ADC: %g -> %d = %g / (%g * %d) * %d + %g", 
	      edep, counts, edep, fMIPEnergy, fMIPRange, fADCRange, ped);
	if (tdc < 0)    tdc = 0;
	new(cache[ndigit]) Data::V0Digit(d, ring, sector, counts, tdc);
	ndigit++;
      }
    }
  }
}


//____________________________________________________________________ 
//  
// EOF
//
