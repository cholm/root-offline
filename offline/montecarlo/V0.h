// -*- mode: C++ -*- 
//____________________________________________________________________ 
//  
// $Id: V0.h,v 1.3 2005-12-14 22:05:19 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    montecarlo/V0.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Montecarlo::V0
*/
#ifndef Montecarlo_V0
#define Montecarlo_V0
#ifndef Simulation_Task
# include <simulation/Task.h>
#endif
#ifndef ROOT_TArrayI
# include <TArrayI.h>
#endif
class TVector3;

namespace Montecarlo 
{
  /** @class V0 montecarlo/V0.h <montecarlo/V0.h>
      @brief V0 simulation 
      @ingroup montecarlo v0
   */
  class V0 : public Simulation::Task 
  {
  public:
    /** CTOR */
    V0();
    /** Initialize */
    void Initialize(Option_t* option="");
    /** Register */
    void Register(Option_t* option="");
    /** Deal with hit */
    void Step();
    /** Deal with magnetic field */
    void Field(const TVector3& x, TVector3& b);
    ClassDef(V0,0) //
  };
}

#endif
//____________________________________________________________________
//
// EOF
//
