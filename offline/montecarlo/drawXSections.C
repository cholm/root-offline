//____________________________________________________________________ 
//  
// $Id: drawXSections.C,v 1.1 2005-12-14 22:05:19 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    drawXSections.C
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Wed Dec 14 21:33:01 2005
    @brief   Draw X-sections
    
*/
#include <TTree.h>
#include <TGraph.h>
#include <TFile.h>
#include <TString.h>
#include <TMath.h>
#include <TLegend.h>
#include <TH1.h>
#include <TH2.h>
#include <TCanvas.h>
#include <iostream>
#include <TGeoMaterial.h>
#include <TGeoManager.h>
#include <TDatabasePDG.h>
#include <TVirtualMC.h>

const char* names[] = {  "HADF",
			 "INEF",
			 "ELAF",
			 "HADG",
			 "INEG",
			 "ELAG",
			 "FISG",
			 "CAPG",
			 "LOSS",
			 "PHOT",
			 "ANNI",
			 "COMP",
			 "BREM",
			 "PAIR",
			 "DRAY",
			 "PFIS",
			 "RAYL",
			 "MUNU",
			 "RANG",
			 "STEP",
			 0 };

const char* titles[] = { "Fluka Had. total",
			 "Fluka Had. inelastic",
			 "Fluka Had. elastic",
			 "Gheisha Had. total",
			 "Gheisha Had. inelastic",
			 "Gheisha Had. elastic",
			 "Nuclear fission",
			 "Neutron capture",
			 "Stopping power",
			 "Photo-electric",
			 "e^+ annihilation",
			 "Compton effect",
			 "Bremsstrahlung",
			 "Pair production",
			 "#delta ray",
			 "Photo-fission",
			 "Rayleigh scattering",
			 "#mu nuclear",
			 "Range",
			 "Max. step",
			 0 };

const char* units[]  = { "cm^{-1}",
			 "cm^{-1}",
			 "cm^{-1}",
			 "cm^{-1}",
			 "cm^{-1}",
			 "cm^{-1}",
			 "cm^{-1}",
			 "cm^{-1}",
			 "MeV cm^{-1}",
			 "cm^{-1}",
			 "cm^{-1}",
			 "cm^{-1}",
			 "cm^{-1}",
			 "cm^{-1}",
			 "cm^{-1}",
			 "cm^{-1}",
			 "cm^{-1}",
			 "cm^{-1}",
			 "cm",
			 "cm",
			 0 };

Float_t fact[] = { 1.,
		   1.,
		   1.,
		   1.,
		   1.,
		   1.,
		   1.,
		   1.,
		   1.,
		   1.,
		   1.,
		   1.,
		   1.,
		   1.,
		   1,
		   1.,
		   1.,
		   1.,
		   1e-6,
		   1e-6,
		   0 };

Float_t pMin    = 1.e-5;
Float_t pMax    = 1.e4;
Int_t nMech = 20;

struct Entry 
{
  Float_t p;
  Float_t mech[20];
};


void
Mechs(const char* mname="FMD Si", const char* pname="pi-")
{
  TDirectory* dir = gDirectory->mkdir(Form("%s_%s", mname, pname));
  dir->cd();

  TString mechS("p/F");
  Int_t nMech = 0;
  char* name = names;
  while (mech) {
    mechS.Append(":");
    mechS(name);
    nMech++;
    name++;
  }
  TGeant3TGeo* vmc  = (TGeant3TGeo*)TVirtualMC::GetMC();
  // TVirtualMC* vmc = TVirtualMC::GetMC();

  // Get the particle Id 
  TParticlePDG* ppdg = TDatabasePDG::Instance()->GetParticle(pname);
  if (!ppdg) {
    std::cerr << "No particle found for '" << pname << "'" << std::endl;
    return;
  }
  Int_t pid = vmc->IdFromPDG(ppdg->PdgCode());

  // Get the material Id
  TGeoMaterial* mat = gGeoManager->GetMaterial(mname);
  Int_t imat = mat->GetIndex();
  
  // Make arrays 
  const Int_t nP = 100;
  Float_t ps[100];
  Float_t va[20][100];
  Float_t lpMin   = TMath::Log(pMin);
  Float_t lpMax   = TMath::Log(pMax);
  Float_t cuts[6] = { 1.e6, 1.e6, 1.e6, 1.e6, 1.e6, 1.e6 };
  Entry   entries[100];
  for (Int_t i = 0; i < nP; i++)
  {
    ps[i] = TMath::Exp(i * (lpMax - lpMin) / nP + lpMin);
    for (Int_t j = 0; j < nMech; j++) va[j][i] = 0;
  }

  // Now, get the values 
  name = names;
  while (name) {
    Int_t ret;
    vmc->Gftmat(imat, pid, name, nP, ps, va[i], cuts, ret);
    if (ret != 1) {
      std::cout << "Gftmat failed for " << name << std::endl;
      fact[i] = 0;
      continue;
    }
    for (Int_t j = 0; j < nP; j++) {
      entries[j].p       = ps[j];
      entries[j].mech[i] = va[i][j];
    }
    name++;
  }

  TTree*       tree = new TTree("T", "T");
  TGraph**     g    = new TGraph * [20];
  Entry        entry;
  tree->Branch("eloss", &entry, mechS.Data());
  for (Int_t j = 0; j < nMech; j++) g[j] = new TGraph(nP);
  for (Int_t i = 0; i < nP; i++) {
    entry.p = entries[i].p;
    for (Int_t j = 0; j < nMech; j++) {
      entry.mech[j] = entries[i].mech[j];
      (g[j])->SetPoint(i, entry.p, fact[j] * entry.mech[j]);
    }
    tree->Fill();
  }
  gStyle->SetOptStat(0);
  gStyle->SetTitleBorderSize(1);
  gStyle->SetTitleFillColor(0);
  gStyle->SetTitleX(.1);
  gStyle->SetTitleY(.95);
  gStyle->SetTitleW(.4);
  TCanvas* c = new  TCanvas("C", "C", 600, 800);
  c->SetLogx();
  c->SetLogy();
  c->SetTopMargin(0.05);
  c->SetRightMargin(0.05);
  c->SetFillColor(0);
  c->SetBorderSize(0);
  c->SetBorderMode(0);
  
  TLegend* l = new TLegend(.6, .6, .95, .95);
  l->SetBorderSize(1);
  l->SetFillColor(0);
  Float_t minY = 5e-6;
  Float_t maxY = 5e5;
  TH1* null = new TH2F("null", Form("%s interactions in %s", pname, mname),
		       100,.9*ps[0],1.1*ps[nP-1],100,minY,maxY); 
  null->SetXTitle("p [GeV]");
  null->Draw();
  for (Int_t i = 0; i < nMech; i++) {
    if (fact[i] == 0) continue;
    (g[i])->Draw("L");
    (g[i])->SetLineWidth(2);
    (g[i])->SetLineColor(i % 3 + 2);
    (g[i])->SetLineStyle(i / 3 + 1);
    (g[i])->SetName(names[i]);
    (g[i])->SetTitle(Form("%s - %s %s", titles[i], pname, mname));
    g[i]->Write();
    l->AddEntry(g[i], Form("%s #times 10^{%d} [%s]", titles[i], 
			   Int_t(TMath::Log10(fact[i])), units[i]), "l");
    std::cout << names[i] << " in " << g[i]->GetHistogram()->GetMinimum() 
	      << " to " << g[i]->GetHistogram()->GetMaximum() << std::endl;
  }
  l->Draw();
  c->Modified();
  c->Update();
  c->Print(Form("%s.gif", pname));
  dir->cd("..");
  // tree->Draw("LOSS");
}

const char* parts[] = { "e-",
			"pi-", 
			"K-",
			"proton", 
			"Alpha", 
			"Deuteron", 
			"Triton",
			0 };

void
drawXSections(const char* name="FMD Si")
{
  
  gROOT->Macro("McConfig.C");
  Simulation::Main::Instance()->ExecInitialize();
  
  TFile* file = TFile::Open("eloss.root", "RECREATE");
  char* part = parts;
  while (part) {
    Mechs(name, part);
    part++;
  }
  TCanvas* c = new TCanvas("loss", "Energy loss");
  c->SetTopMargin(0.05);
  c->SetRightMargin(0.05);
  c->SetFillColor(0);
  c->SetBorderSize(0);
  c->SetBorderMode(0);
  c->SetLogy();
  c->SetLogx();
  c->SetGridx();
  c->SetGridy();
  TH2* null = new TH2F("null", "Energy loss in 300 #mu Si",
		       100,.9*pMin, 1.1*pMax,100,1*.03,50000*.03); 
  null->SetXTitle("p / m ");
  null->SetYTitle("#Delta E [MeV]");
  null->Draw();
  TLegend* l = new TLegend(.6, .6, .95, .95);
  l->SetBorderSize(1);
  l->SetFillColor(0);
  TGraph**       g     = new TGraph * [npart];
  TParticlePDG** pdgs  = new TParticlePDG * [npart];
  for (Int_t i = 0; i < npart; i++) {
    pdgs[i] = TDatabasePDG::Instance()->GetParticle(parts[i]);
    g[i] = (TGraph*)(file->Get(Form("%s_%s/LOSS", name, parts[i])));
  }

  for (Int_t i = 0; i < npart; i++) {
    for (Int_t j = 0; j < g[i]->GetN(); j++) {
      Double_t x, y;
      g[i]->GetPoint(j, x, y);
      g[i]->SetPoint(j, x, y * .03);
    }
    (g[i])->Draw("L");
    (g[i])->SetLineWidth(2);
    (g[i])->SetLineColor(i % 3 + 2);
    (g[i])->SetLineStyle(i / 3 + 1);
    (g[i])->SetName(parts[i]);
    (g[i])->SetTitle(parts[i]);
    // g[i]->Write();
    l->AddEntry(g[i], Form("%s  (PDG %d)", parts[i], pdgs[i]->PdgCode()), "l");
  }
  l->Draw();
  c->Modified();
  c->Update();
  c->Print("eloss.eps");
  return;
  
  c->Clear();
  c->SetGridx();
  c->SetGridy();
  // null->SetBins(100,.9*pMin, 1.1*pMax,100,.1,1000);
  null->Draw();
  for (Int_t i = 0; i < npart; i++) {
    Double_t mass = pdgs[i]->Mass();
    for (Int_t j = 0; j < g[i]->GetN(); j++) {
      Double_t x, y;
      g[i]->GetPoint(j, x, y);
      g[i]->SetPoint(j, x / mass, y);
    }
    (g[i])->Draw("L");
    (g[i])->SetLineWidth(2);
    (g[i])->SetLineColor(i % 3 + 2);
    (g[i])->SetLineStyle(i / 3 + 1);
    (g[i])->SetName(parts[i]);
    (g[i])->SetTitle(parts[i]);
    // g[i]->Write();
    // l->AddEntry(g[i], Form("%s  (PDG %d)", parts[i], pdgs[i]->PdgCode()), "l");
  }
  l->Draw();
  c->Modified();
  c->Update();
  c->Print("scaled_eloss.eps");
  
  // file->ls();
  // file->Close();
}

//
// EOF
//

