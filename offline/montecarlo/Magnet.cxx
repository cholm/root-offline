//____________________________________________________________________ 
//  
// $Id: Magnet.cxx,v 1.6 2007-09-28 14:50:40 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    montecarlo/Magnet.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementation file for Montecarlo::Magnet
*/
#include "montecarlo/Magnet.h"
#include "geometry/Magnet.h"
#include <TGeoVolume.h>
#include <TGeoMatrix.h>
#include <TGeoPgon.h>
#include <TGeoMaterial.h>
#include <TGeoMedium.h>
#include <TGeoManager.h>
#include <TTree.h>
#include <TLorentzVector.h>
#include <TVector3.h>
#include <TVirtualMC.h>
#include <TF1.h>

//____________________________________________________________________
ClassImp(Montecarlo::Magnet);

//____________________________________________________________________
Montecarlo::Magnet::Magnet() 
  : Simulation::Task("Magnet", "Solonoidial magnet")
{
  fCache = 0;
}

//____________________________________________________________________
void
Montecarlo::Magnet::Initialize(Option_t* option) 
{
  TGeoMedium* medium = gGeoManager->GetMedium("Magnet Iron");
  RegisterMedium(medium);
}

//____________________________________________________________________
void
Montecarlo::Magnet::Step() 
{}

//____________________________________________________________________
void
Montecarlo::Magnet::Field(const TVector3& x, TVector3& b) 
{
  Geometry::Magnet::Instance()->Field(x, b);  
}

  
//____________________________________________________________________
//
// EOF
//
