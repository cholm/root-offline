// -*- mode: c++ -*- 
//____________________________________________________________________ 
//  
// $Id: Generator.h,v 1.6 2005-12-14 22:05:19 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    montecarlo/Generator.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Montecarlo::Generator
*/
#ifndef Montecarlo_Generator
#define Montecarlo_Generator
#ifndef Simulation_Generator
# include <simulation/Generator.h>
#endif 

class TRandom;
class TLorentzVector;

namespace Montecarlo 
{
  /** @class Generator montecarlo/Generator.h <montecarlo/Generator.h>
      @brief Task to deal with generators.  Puts generated particles
      into the output tree 
      @ingroup montecarlo
   */
  class Generator : public Simulation::Generator
  {
  private:
    /** Parameters of vertex smearing */
    Double_t fMeanZ;
    /** Parameters of vertex smearing */
    Double_t fSigmaZ;
    /** Random number generator */
    TRandom* fRandom;
  public:
    /** Create a generator task. */
    Generator();

    /** Register branch */
    void Register(Option_t* option="");
    
    /** Set vertex parameters */
    void SetVertex(Double_t m=0, Double_t s=5) { fMeanZ=m; fSigmaZ=s; }//*MENU*
    /** @param v on output - created vertex  */
    void MakeVertex(TLorentzVector& v);
    
    ClassDef(Generator,1) // Generator in Montecarlo framework
  };
}

#endif
