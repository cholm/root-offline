//____________________________________________________________________ 
//  
// $Id: Generator.cxx,v 1.5 2007-09-28 14:50:40 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    montecarlo/Generator.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementation file for Montecarlo::Generator
*/
#include "montecarlo/Generator.h"
#include <TTree.h>
#include <TRandom.h>
#include <TGenerator.h>
#include <TLorentzVector.h>
//____________________________________________________________________
ClassImp(Montecarlo::Generator);


//____________________________________________________________________
Montecarlo::Generator::Generator() 
  : Simulation::Generator("Canon", "Particle canon"), 
    fRandom(0)
{
  SetGenerator(0);
  fRandom = new TRandom;
}

//____________________________________________________________________
void
Montecarlo::Generator::Register(Option_t* option) 
{
  TTree* tree = 0;
  if (!fGenerator) return;
  if ((tree = GetBaseTree("Hits")))
    fBranch = tree->Branch(fGenerator->GetName(), &fCache);
}

//____________________________________________________________________
void
Montecarlo::Generator::MakeVertex(TLorentzVector& v) 
{
  v.SetXYZT(0, 0, 0, fRandom->Gaus(fMeanZ, fSigmaZ));
}

  
//____________________________________________________________________
//
// EOF
//
