//____________________________________________________________________ 
//  
// $Id: Cave.cxx,v 1.11 2007-09-28 14:50:40 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    montecarlo/Cave.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementation file for Montecarlo::Cave
*/
#include "montecarlo/Cave.h"
#include <TParticle.h>
#include <TGeoVolume.h>
#include <TGeoMedium.h>
#include <TGeoManager.h>
#include <TClonesArray.h>
#include <TVirtualMC.h>
#ifndef Simulation_Main
# include <simulation/Main.h>
#endif

//____________________________________________________________________
ClassImp(Montecarlo::Cave);

//____________________________________________________________________
Montecarlo::Cave::Cave() 
  : Simulation::Task("Cave", "The cave")
{
  CreateDefaultHitArray();
}

//____________________________________________________________________
void
Montecarlo::Cave::Initialize(Option_t* option) 
{
  Debug(5, "Initialize", "Setting up materials");
  Simulation::Task::Initialize(option);
  RegisterMedium(gGeoManager->GetMedium("Cave air"));
}

//____________________________________________________________________
void
Montecarlo::Cave::Register(Option_t* option) 
{
  RegisterVolume(gGeoManager->GetVolume("Cave"));
}

//____________________________________________________________________
void 
Montecarlo::Cave::DefineParticles() 
{
  TVirtualMC* mc = TVirtualMC::GetMC();
  TDatabasePDG* pdgDb = TDatabasePDG::Instance();
  Int_t  pdgs[] = { -130, 311, -311, 0 };
  Int_t  npdg   = 0;
  for (Int_t i = 0; i < npdg; i++) {
    TParticlePDG* pdgP  = pdgDb->GetParticle(pdgs[i]);
    if (pdgP) {
      Verbose(1, "DefineParticles", "Defining %d: %s", 
	      pdgs[i], pdgP->GetName());
      TMCParticleType t = kPTHadron;
      if (TMath::Abs(pdgP->Charge()) < 0) t = kPTNeutron;
      mc->DefineParticle(pdgP->PdgCode(), pdgP->GetName(), t,pdgP->Mass(),
			 pdgP->Charge() / 3, pdgP->Lifetime());
    }
    else 
      Verbose(1, "DefineParticles", "PDG %d not in database", pdgs[i]);
  }
};

//____________________________________________________________________
void
Montecarlo::Cave::Step() 
{
  if (fDebug >= 10) {
    TVirtualMC* mc = TVirtualMC::GetMC();
    Int_t copy;
    Int_t vol = mc->CurrentVolID(copy);
    if (!IsSensitive(vol)) return;
    // Info("Step", "Making a hit in cave");
    if (!mc->IsTrackAlive()) return;
    CreateDefaultHit();
    mc->GetStack()->GetCurrentTrack()->SetBit(Simulation::kKeepBit);
  }
}

//____________________________________________________________________
void
Montecarlo::Cave::Field(const TVector3& x, TVector3& b) 
{
  b[0] = b[1] = b[2] = 0;
  Task* magnet = 0;
  if (fParent) {
    TList* tasks = fParent->GetListOfTasks();
    magnet = static_cast<Simulation::Task*>(tasks->FindObject("Magnet"));
    if (magnet) magnet->Field(x, b);
  }
  Debug(20, "Field", "Bz = %f @ (%f,%f,%f) from 0x%X", 
	b[2], x[0], x[1], x[2], magnet);
}

//____________________________________________________________________
//
// EOF
//
