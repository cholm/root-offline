//____________________________________________________________________ 
//  
// $Id: TPC.cxx,v 1.10 2007-09-28 14:50:40 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    montecarlo/TPC.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementation file for Montecarlo::TPC
*/
#include "montecarlo/TPC.h"
#ifndef Simulation_Main
# include <simulation/Main.h>
#endif
#include <TParticle.h>
#include <TGeoVolume.h>
#include <TGeoMedium.h>
#include <TGeoManager.h>
#include <TTree.h>
#include <TLorentzVector.h>
#include <TVector3.h>
#include <TVirtualMC.h>

//____________________________________________________________________
ClassImp(Montecarlo::TPC);

//____________________________________________________________________
Montecarlo::TPC::TPC() 
  : Simulation::Task("TPC", "Time Projection Chamber")
{
  CreateDefaultHitArray();
}

//____________________________________________________________________
void
Montecarlo::TPC::Initialize(Option_t* option) 
{
  Debug(5, "Initialize", "Setting up materials");
  Simulation::Task::Initialize(option);

  TGeoMedium* gas = gGeoManager->GetMedium("TPC Gas");
  RegisterMedium(gas);
  TGeoMedium* al = gGeoManager->GetMedium("TPC Al");
  RegisterMedium(al);
}

//____________________________________________________________________
void
Montecarlo::TPC::Register(Option_t* option) 
{
  Debug(5, "Register", "Setting up volume");

  TTree* tree = 0;
  if ((tree = GetBaseTree("Hits")))
    fBranch = tree->Branch(GetName(), &fCache);

  TGeoVolume* fGasVol = gGeoManager->GetVolume("TPC");
  RegisterVolume(fGasVol);
  fInner = gGeoManager->GetVolume("TPCi");
  RegisterVolume(fInner);
  fOuter = gGeoManager->GetVolume("TPCo");
  RegisterVolume(fOuter);
}

//____________________________________________________________________
void
Montecarlo::TPC::Step() 
{
  Debug(10, "Step", "Processing hit");
  TVirtualMC* mc = TVirtualMC::GetMC();
  Int_t copy;
  Int_t vol = mc->CurrentVolID(copy);
  if (!IsSensitive(vol)) return;
  if (!mc->IsTrackAlive()) return;
  // Stop laser tracks when they hit the inner vessel.
  // if (TMath::Abs(mc->TrackCharge()) <= 0 && vol == fInner->GetNumber()) {
  //   mc->StopTrack();
  //   return;
  // }
  CreateDefaultHit();
  mc->GetStack()->GetCurrentTrack()->SetBit(Simulation::kKeepBit);
}

//____________________________________________________________________
void
Montecarlo::TPC::Field(const TVector3& x, TVector3& b) 
{
  b[0] = b[1] = b[2] = 0;
  Task* magnet = 0;
  if (fParent) {
    TList* tasks = fParent->GetListOfTasks();
    magnet = static_cast<Simulation::Task*>(tasks->FindObject("Magnet"));
    if (magnet) magnet->Field(x, b);
  }
  Debug(10, "Field", "Bz = %f @ (%f,%f,%f) from 0x%X", 
	b[2], x[0], x[1], x[2], magnet);
}

//________________________________________________________c____________
//
// EOF
//
