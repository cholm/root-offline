//____________________________________________________________________ 
//  
// $Id: FMD.cxx,v 1.14 2007-09-28 14:50:40 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    montecarlo/FMD.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementation file for Montecarlo::FMD
*/
#include "montecarlo/FMD.h"
#include "data/FMDHit.h"
#include <simulation/Main.h>
#include <TGeoManager.h>
#include <TGeoVolume.h>
#include <TGeoMedium.h>
#include <TTree.h>
#include <TParticle.h>
#include <TLorentzVector.h>
#include <TVector2.h>
#include <TVector3.h>
#include <TVirtualMC.h>
#include <TClonesArray.h>
#include <TMath.h>
#include <iostream>

//____________________________________________________________________
ClassImp(Montecarlo::FMD);

//____________________________________________________________________
Montecarlo::FMD::FMD() 
  : Simulation::Task("FMD", "Forward multiplicity"),
    fNEvents(0)
{
  fCache   = new TClonesArray("Data::FMDHit");
  fBadHits = new TObjArray;
}

//____________________________________________________________________
void
Montecarlo::FMD::Initialize(Option_t* option) 
{
  Debug(5, "Initialize", "Setting up materials");
  Simulation::Task::Initialize(option);

  RegisterMedium(gGeoManager->GetMedium("FMD Air"));
  RegisterMedium(gGeoManager->GetMedium("FMD Plastic"));
  RegisterMedium(gGeoManager->GetMedium("FMD PCB"));
  RegisterMedium(gGeoManager->GetMedium("FMD Al"));
  RegisterMedium(gGeoManager->GetMedium("FMD Si"));
  RegisterMedium(gGeoManager->GetMedium("FMD Chip"));
  RegisterMedium(gGeoManager->GetMedium("FMD C"));
  RegisterMedium(gGeoManager->GetMedium("FMD Kapton"));
}

//____________________________________________________________________
void
Montecarlo::FMD::Register(Option_t* option) 
{
  Debug(5, "Register", "Setting up volume");
  TTree* tree = 0;
  if ((tree = GetBaseTree("Hits"))) fBranch = tree->Branch(GetName(), &fCache);

  RegisterVolume(gGeoManager->GetVolume("FIST"));
  RegisterVolume(gGeoManager->GetVolume("FOST"));
}

//____________________________________________________________________
Bool_t
Montecarlo::FMD::VMC2FMD(Int_t copy, UShort_t& detector, Char_t& ring, 
			 UShort_t& sector, UShort_t& strip) 
{
  TVirtualMC* mc = TVirtualMC::GetMC();

  strip = copy - 1;
  Int_t sectordiv;
  mc->CurrentVolOffID(1, sectordiv);
  Int_t module;
  mc->CurrentVolOffID(3, module);
  sector = 2 * module + (sectordiv-1);
  Int_t iring;
  mc->CurrentVolOffID(4, iring);
  ring = Char_t(iring);
  Int_t det;
  mc->CurrentVolOffID(5, det);
  detector = det;
  Debug(10,"VMC2FMD", "Hit in FMD%d%c[%2d,%3d] %s", 
	detector, ring, sector, strip, 
	mc->CurrentVolPath());
  return kTRUE;
}

//____________________________________________________________________
void
Montecarlo::FMD::CheckHit(Data::FMDHit* hit)
{
  TVirtualMC* mc     = TVirtualMC::GetMC();
  Double_t mass      = mc->TrackMass();
  Double_t pm        = hit->P().P() / mass;
  Bool_t   largeEdep = (pm > 1 && hit->EnergyLoss() > 1);
  if (largeEdep) {
    // Get processes as a string 
    TArrayI procs;
    mc->StepProcesses(procs);
    TString processes;
    for (Int_t ip = 0; ip < procs.fN; ip++) {
      if (ip != 0) processes.Append(",");
      processes.Append(TMCProcessName[procs.fArray[ip]]);
    }
    // Get particle name 
    TParticlePDG* ppdg = TDatabasePDG::Instance()->GetParticle(hit->Pdg());
    TString pname(ppdg ? ppdg->GetName() : Form("%d", hit->Pdg()));
    Warning("AddHit", "Large edep in hit in FMD%d%c[%2d,%3d] (%s) "
	    "from track # %d (%s p/m=%f GeV): %f MeV (%s)", 
	    hit->Detector(), hit->Ring(), hit->Sector(), hit->Strip(), 
	    mc->CurrentVolPath(), hit->NTrack(), pname.Data(), pm, 
	    hit->EnergyLoss(), processes.Data());
    // gROOT->ProcessLineSync(".x printStack.C");
    hit->SetMarkerColor(2);
    fBadHits->Add(hit);
  }
}

//____________________________________________________________________
void
Montecarlo::FMD::AddHit(Int_t track, 
			UShort_t detector, Char_t ring, 
			UShort_t sector, UShort_t strip, 
			Float_t edep, const TLorentzVector& v, 
			const TLorentzVector& p, Int_t pdg, 
			Float_t length) 
{
  TClonesArray* hits = static_cast<TClonesArray*>(fCache);
  Int_t nhits = hits->GetEntriesFast();
  Data::FMDHit* hit = 0;
  
  for (Int_t i = 0; i < nhits; i++) {
    // There may be holes in the cache 
    if (!hits->At(i)) continue;
    hit = static_cast<Data::FMDHit*>(hits->At(i));
    if (track    == hit->NTrack()    && 
	detector == hit->Detector()  && 
	ring     == hit->Ring()      &&
	sector   == hit->Sector()    && 
	strip    == hit->Strip()) {
      Debug(15,"AddHit", "Warning: There already exists a hit for track "
	    "# %d in FMD%d%c[%2d,%3d], updating that one", 
	    track, detector, ring, sector, strip);
      hit->SetEnergyLoss(hit->EnergyLoss() + edep);
    }
    else 
      hit = 0;
  } // for (Int_t i = 0; i < nhits; i++)
  if(!hit) {
    Debug(15, "AddHit", "Adding a hit from track %d in FMD%d%c[%2d,%3d]", 
	  track, detector, ring, sector, strip);
    hit = new ((*hits)[nhits]) Data::FMDHit(detector, ring, sector, strip,
					    track, v, p, edep, pdg, length);
  }
  if (fDebug > 50) CheckHit(hit);
} 
    
//____________________________________________________________________
void
Montecarlo::FMD::Step() 
{
  TVirtualMC* mc = TVirtualMC::GetMC();

  if (!mc->IsTrackAlive()) return;
  if (TMath::Abs(mc->TrackCharge()) <= 0) return;
  // Check if we're in an active volume 
  Int_t vol, copy;
  vol = mc->CurrentVolID(copy);
  if (!IsSensitive(vol)) return;

  // Coordinates 
  UShort_t detector; 
  Char_t   ring;
  UShort_t sector;
  UShort_t strip;

  // Check if we're inside a strip. 
  if (!VMC2FMD(copy, detector, ring, sector, strip)) return;
   // Get track position 
 
  static Float_t energyLoss;
  static TLorentzVector v;
  static TLorentzVector p;
  if (mc->IsTrackEntering()) { 
    mc->TrackPosition(v);
    mc->TrackMomentum(p);
    energyLoss = 0;
  }
  if (mc->IsTrackInside())   energyLoss += 1000 * mc->Edep();
  if (mc->IsTrackExiting()|| mc->IsTrackDisappeared()|| mc->IsTrackStop()) {
    // Get some stuff from the VMC, like particle momentum, track
    // number, and so on 
    Int_t    track = mc->GetStack()->GetCurrentTrackNumber();
    Double_t edep  = mc->Edep();
    Int_t    pdg   = mc->TrackPid();
    TLorentzVector cur;
    mc->TrackPosition(cur);
    TVector3 vv(cur.Vect());
    vv -= v.Vect();
    // Now, we create a hit, if one does not already exist for the
    // current track number. 
    AddHit(track, detector, ring, sector, strip, 1000 * edep, 
	   v, p, pdg, vv.Mag());
    mc->GetStack()->GetCurrentTrack()->SetBit(Simulation::kKeepBit);
  }
}

//____________________________________________________________________
void 
Montecarlo::FMD::FinishEvent()
{
  Simulation::Task::FinishEvent();
  Int_t n = fBadHits->GetEntries();
  Verbose(1, "FinishEvent", "A total of %d bad hits", n);
  for (Int_t i = 0; i < n; i++) {
    Data::FMDHit* hit = static_cast<Data::FMDHit*>(fBadHits->At(i));
    hit->Print();
  }
  fSummedBadHits += n;
  fNEvents++;
  fBadHits->Clear();
}

//____________________________________________________________________
void 
Montecarlo::FMD::Finish(Option_t* option)
{
  Verbose(1, "Finish", "An average of %f bad hits", 
       Float_t(fSummedBadHits) / fNEvents);
}

//____________________________________________________________________
//
// EOF
//
