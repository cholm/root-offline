//____________________________________________________________________ 
//  
// $Id: TOF.cxx,v 1.11 2007-09-28 14:50:40 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    montecarlo/TOF.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementation file for Montecarlo::TOF
*/
#include "montecarlo/TOF.h"
#include "data/TOFHit.h"
#include <simulation/Main.h>
#include <TGeoVolume.h>
#include <TGeoMedium.h>
#include <TGeoManager.h>
#include <TTree.h>
#include <TParticle.h>
#include <TLorentzVector.h>
#include <TVector3.h>
#include <TVirtualMC.h>
#include <TClonesArray.h>

//____________________________________________________________________
ClassImp(Montecarlo::TOF);

//____________________________________________________________________
Montecarlo::TOF::TOF() 
  : Simulation::Task("TOF", "Time of Flight")
{
  fCache = new TClonesArray("Data::TOFHit");
}

//____________________________________________________________________
void
Montecarlo::TOF::Initialize(Option_t* option) 
{
  Debug(5, "Initialize", "Setting up materials");
  Simulation::Task::Initialize(option);
  TGeoMedium* medium = gGeoManager->GetMedium("TOF scint");
  RegisterMedium(medium);
}

//____________________________________________________________________
void
Montecarlo::TOF::Register(Option_t* option) 
{
  Debug(5, "Register", "Setting up volume");

  TTree* tree = 0;
  if ((tree = GetBaseTree("Hits"))) fBranch = tree->Branch(GetName(), &fCache);
  TGeoVolume* sens = gGeoManager->GetVolume("TOFS");
  Debug(5, "Register", "Registering sensitive volume %s", sens->GetName());
  RegisterVolume(sens);
}

//____________________________________________________________________
void
Montecarlo::TOF::Step() 
{
  TVirtualMC* mc = TVirtualMC::GetMC();
  // Debug(15,"Step", "Hit in TOF");
  if (!mc->IsTrackAlive()) { 
    // Debug(10, "Step", "Track is dead");
    return;
  }
  if (TMath::Abs(mc->TrackCharge()) <= 0) { 
    // Debug(10, "Step", "Track is not charged %d", mc->TrackCharge());
    return;
  }
  
  
  Int_t copy;
  Int_t vol = mc->CurrentVolID(copy);
  if (!IsSensitive(vol)) { 
    Debug(10, "Step", "Track is not in a sensitive volume %s", 
	  mc->CurrentVolName());
    return;
  }
  Debug(10, "Step", "Track is in a sensitive volume %s", 
	mc->CurrentVolName());
  
  static Float_t energyLoss;
  if (mc->IsTrackEntering()) energyLoss = 0;
  if (mc->IsTrackInside())   energyLoss += 1000 * mc->Edep();
  if (mc->IsTrackExiting()|| mc->IsTrackDisappeared()|| mc->IsTrackStop()) {

    Int_t sector = copy - 1;
    Int_t ring;
    mc->CurrentVolOffID(1, ring);
    ring--;
    Int_t    pdg   = mc->TrackPid();

    Debug(10, "Step", "Processing hit in TOF[%d,%d]", ring, sector);

    TClonesArray* hits = static_cast<TClonesArray*>(fCache);
    Int_t n = hits->GetEntriesFast();
    TLorentzVector v, p;
    mc->TrackPosition(v);
    mc->TrackMomentum(p);
    energyLoss += 1000 * mc->Edep();
    new ((*hits)[n]) Data::TOFHit(sector, ring,
				  mc->GetStack()->GetCurrentTrackNumber(),
				  v, p, energyLoss, pdg);
    mc->GetStack()->GetCurrentTrack()->SetBit(Simulation::kKeepBit);
  }
}

//____________________________________________________________________
void
Montecarlo::TOF::Field(const TVector3& x, TVector3& b) 
{
  b[0] = b[1] = b[2] = 0;
  Task* magnet = 0;
  if (fParent) {
    TList* tasks = fParent->GetListOfTasks();
    magnet = static_cast<Simulation::Task*>(tasks->FindObject("Magnet"));
    if (magnet) magnet->Field(x, b);
  }
  Debug(10, "Field", "Bz = %f @ (%f,%f,%f) from 0x%X", 
	b[2], x[0], x[1], x[2], magnet);
}

//____________________________________________________________________
//
// EOF
//
