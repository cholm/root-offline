//____________________________________________________________________ 
//  
// $Id: Laser.cxx,v 1.3 2007-09-28 14:50:40 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    montecarlo/Laser.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementation file for Montecarlo::Laser
*/
#include "montecarlo/Laser.h"
#include <TGeoManager.h>
#include <TGeoShape.h>
#include <TGeoVolume.h>
#include <TClonesArray.h>
#include <TRandom.h>
#include <TParticle.h>
#include <TParticlePDG.h>
#include <TDatabasePDG.h>
#include <TMath.h>
#ifndef __IOSTREAM__
# include <iostream>
#endif

//____________________________________________________________________
ClassImp(Montecarlo::Laser);

//____________________________________________________________________
Montecarlo::Laser::Laser() 
  : TGenerator("laser", "A simple particle laser")
{
  fParticles = new TClonesArray("TParticle");
  fRandom    = new TRandom;
  SetPdgCode();
  SetNz();
  SetNphi();
  SetNbeam();
  SetAngle();
  SetP();
}

//____________________________________________________________________
Int_t
Montecarlo::Laser::ImportParticles(TClonesArray* a, Option_t* option) 
{
  TGeoVolume* tpc = gGeoManager->GetVolume("TPC");
  if (!tpc) return 0;
  TGeoShape* tpcShape = tpc->GetShape();
  if (!tpcShape) return 0;
  Double_t zmin, zmax, rmin, rmax;
  tpcShape->GetAxisRange(1, rmin, rmax);
  tpcShape->GetAxisRange(3, zmin, zmax);
  Double_t l = zmax - zmin;
  Double_t r = rmax;
  Int_t partnum = 0;

  a->Clear();
  TDatabasePDG* pdgDb = TDatabasePDG::Instance();
  TParticlePDG* pdgP  = pdgDb->GetParticle(fPdgCode);
  Float_t       m     = pdgP->Mass();
  //  Int_t j=0;
  for (Int_t iphi = 0; iphi < fNphi; iphi++) {
    Double_t phi =  360. / fNphi * iphi;
    Double_t x   =  TMath::Cos(phi * TMath::Pi() / 180.) * r;
    Double_t y   =  TMath::Sin(phi * TMath::Pi() / 180.) * r;
    phi          += fAngle / 2;
    for (Int_t ibeam = 0; ibeam < fNbeam; ibeam++) {
      Double_t cos_ang = TMath::Cos(phi * TMath::Pi() / 180.);
      Double_t sin_ang = TMath::Sin(phi * TMath::Pi() / 180.);
      Double_t px      = - fP * cos_ang;
      Double_t py      = - fP * sin_ang;
      Double_t pz      = 0;
      Double_t E       = TMath::Sqrt(px * px + py * py + pz * pz);      
      phi -= fAngle / (fNbeam-1);
      for (Int_t iz = 0; iz < fNz; iz++) {
	Double_t z   = -l/2.   + l / fNz * iz;
	TParticle* pp = new ((*a)[partnum]) 
	  TParticle(fPdgCode, 0, -1, -1, -1, -1, px, py, pz, E, x, y, z, 0);
	partnum++;      
      }
    }
  }
  
  return 0;
}


//____________________________________________________________________
TObjArray* 
Montecarlo::Laser::ImportParticles(Option_t* option) 
{
  ImportParticles(static_cast<TClonesArray*>(fParticles), option);
  return fParticles;
}


//____________________________________________________________________
//
// EOF
//
