// -*- mode: c++ -*-
//____________________________________________________________________ 
//  
// $Id: Laser.h,v 1.3 2005-12-14 22:05:19 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    montecarlo/Laser.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Montecarlo::Laser
*/
#ifndef Montecarlo_Laser
#define Montecarlo_Laser
#ifndef ROOT_TGenerator
# include <TGenerator.h>
#endif
class TClonesArray;
class TRandom;
class TObjArray;


namespace Montecarlo
{
  /** @class Laser montecarlo/Laser.h <montecarlo/Laser.h>
      @brief A simple event generator 
      @ingroup montecarlo
   */
  class Laser : public TGenerator 
  {
  private:
    TRandom* fRandom;
    /** Particle type to make */
    Int_t    fPdgCode;
    /** Number of stations along Z */ 
    Int_t    fNz;
    /** Number of stations in phi */
    Int_t    fNphi;
    /** Number of beams per station */ 
    Int_t    fNbeam;
    /** Opening angle of beams at stations */
    Double_t fAngle;
    /** Momentum */
    Double_t fP;
  public:
    /** CTOR */
    Laser();
    /** @param pdg Particle type  */
    void SetPdgCode(Int_t pdg=22) { fPdgCode = pdg; }//*MENU*
    /** @param x Number of stations along Z */ 
    void SetNz(Int_t x=8) { fNz = x; }
    /** @param x Number of stations in phi */
    void SetNphi(Int_t x=6) { fNphi = x; }
    /** @param x Number of beams per station */ 
    void SetNbeam(Int_t x=7) { fNbeam = x; }
    /** @param x Opening angle of beams at stations */
    void SetAngle(Double_t x=45) { fAngle = x; }
    /** @param x Momentum */
    void SetP(Double_t x=.1) { fP = x; }
    /** Generate one event, and return it in @a a 
	@param a Return array 
	@param option Not used 
	@return  # of particles made */
    Int_t ImportParticles(TClonesArray* a, Option_t* option="");
    /** Generate one event, and return it
	@param option Not used 
	@return  particles made */
    TObjArray* ImportParticles(Option_t* option="");
    
    ClassDef(Laser, 0) 
  };
}

#endif
