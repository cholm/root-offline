//____________________________________________________________________ 
//  
// $Id: V0.cxx,v 1.7 2007-09-28 14:50:40 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    montecarlo/V0.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementation file for Montecarlo::V0
*/
#include "montecarlo/V0.h"
#include "data/V0Hit.h"
#include <simulation/Main.h>
#include <TGeoVolume.h>
#include <TGeoMedium.h>
#include <TGeoManager.h>
#include <TTree.h>
#include <TParticle.h>
#include <TLorentzVector.h>
#include <TVector3.h>
#include <TVirtualMC.h>
#include <TClonesArray.h>

//____________________________________________________________________
ClassImp(Montecarlo::V0);

//____________________________________________________________________
Montecarlo::V0::V0() 
  : Simulation::Task("V0", "Primary Vertex")
{
  fCache = new TClonesArray("Data::V0Hit");
}

//____________________________________________________________________
void
Montecarlo::V0::Initialize(Option_t* option) 
{
  Debug(5, "Initialize", "Setting up materials");
  Simulation::Task::Initialize(option);
  RegisterMedium(gGeoManager->GetMedium("V0 scint"));
  RegisterMedium(gGeoManager->GetMedium("V0 air"));
}

//____________________________________________________________________
void
Montecarlo::V0::Register(Option_t* option) 
{
  Debug(5, "Register", "Setting up volume");
  TTree* tree = 0;
  if ((tree = GetBaseTree("Hits"))) fBranch = tree->Branch(GetName(),&fCache);
  for (Int_t i = 0; i < 4; i++) {
    RegisterVolume(gGeoManager->GetVolume(Form("VA%dM", i+1)));
    RegisterVolume(gGeoManager->GetVolume(Form("VC%dM", i+1)));
  }
}

//____________________________________________________________________
void
Montecarlo::V0::Step() 
{
  TVirtualMC* mc = TVirtualMC::GetMC();
  if (!mc->IsTrackAlive()) {
    Debug(50, "Step", "track is dead");
    return;
  }
  if (TMath::Abs(mc->TrackCharge()) <= 0) {
    Debug(50, "Step", "particle isn't charged");
    return;
  }
  
  static Float_t energyLoss;

  Int_t sector;
  Int_t vol =  mc->CurrentVolID(sector);
  Bool_t ok = kFALSE;
  if (!IsSensitive(vol)) return;
  if (mc->IsTrackEntering()) energyLoss = 0;
  if (mc->IsTrackInside())   energyLoss += 1000 * mc->Edep();
  if (mc->IsTrackExiting() || mc->IsTrackDisappeared() || mc->IsTrackStop()) {
    Int_t ring;
    mc->CurrentVolOffID(1, ring);
    Int_t idet;
    mc->CurrentVolOffID(2, idet);
    Char_t detector = Char_t(idet);    
    sector--;

    Debug(10, "Step", "Processing hit in V0%c[%d,%2d]", 
	  detector, ring, sector);
  
    TClonesArray* hits = static_cast<TClonesArray*>(fCache);
    Int_t nhits = hits->GetEntriesFast();
    Int_t    pdg   = mc->TrackPid();
    TLorentzVector v, p;
    mc->TrackPosition(v);
    mc->TrackMomentum(p);
    energyLoss += 1000 * mc->Edep();
    new ((*hits)[nhits]) Data::V0Hit(detector, ring, sector, 
				     mc->GetStack()->GetCurrentTrackNumber(),
				     v, p, energyLoss, pdg);
    mc->GetStack()->GetCurrentTrack()->SetBit(Simulation::kKeepBit);
  }
}

//____________________________________________________________________
void
Montecarlo::V0::Field(const TVector3& x, TVector3& b) 
{
  b[0] = b[1] = b[2] = 0;
  Task* magnet = 0;
  if (fParent) {
    TList* tasks = fParent->GetListOfTasks();
    magnet = static_cast<Simulation::Task*>(tasks->FindObject("Magnet"));
    if (magnet) magnet->Field(x, b);
  }
  Debug(10, "Field", "Bz = %f @ (%f,%f,%f) from 0x%X", 
	b[2], x[0], x[1], x[2], magnet);
}

//____________________________________________________________________
//
// EOF
//
