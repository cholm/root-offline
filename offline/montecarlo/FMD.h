// -*- mode: C++ -*- 
//____________________________________________________________________ 
//  
// $Id: FMD.h,v 1.10 2005-12-14 22:05:19 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    montecarlo/FMD.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Montecarlo::FMD
*/
#ifndef Montecarlo_FMD
#define Montecarlo_FMD
#ifndef Simulation_Task
# include <simulation/Task.h>
#endif
#ifndef ROOT_TArrayI
# include <TArrayI.h>
#endif
namespace Data 
{
  class FMDHit;
}
class TVector3;
class TGeoVolume;
class TGeoMedium;
class TLorentzVector;

namespace Montecarlo 
{
  /** @class FMD montecarlo/FMD.h <montecarlo/FMD.h>
      @brief FMD simulation 
      @ingroup montecarlo fmd
      @image html FMD.png 
   */
  class FMD : public Simulation::Task 
  {
  protected:
    TObjArray* fBadHits;
    Int_t      fSummedBadHits;
    Int_t      fNEvents;
    
    /** Get the coordinates, or return kFALSE if the current point is
	not inside a strip. 
	@param copy The copy number of divided volume 
	@param detector On return, the detectorn number
	@param ring On return, the ring ID
	@param sector On return, the sector number 
	@param strip On return, the strip number  
	@return true it track is inside a strip, false otherwise  */
    Bool_t VMC2FMD(Int_t copy, UShort_t& detector, 
		   Char_t& ring, UShort_t& sector, UShort_t& strip);
    /** Add a hit to the cache.  Note, that if a hit exists in the
	same detection element for the same track, no new hit will be
	created, but the energy loss will be added to that hit.  In
	that case, none of the other fields (like position or
	momentum) will be updated. 
	@param track    The track # of the hit
	@param detector Detector #
	@param ring     Ring id
	@param sector   Sector # 
	@param strip    Strip #
	@param edep     Energy deposited in this step
	@param v        Position of the track in detector
	@param p        Momentum of the track 
	@param pdg      PDG code 
	@param length   Track length through medium */
    void AddHit(Int_t track, UShort_t detector, Char_t ring, UShort_t sector, 
		UShort_t strip, Float_t edep, const TLorentzVector& v, 
		const TLorentzVector& p, Int_t pdg, Float_t length);
    void CheckHit(Data::FMDHit* hit);
  public:
    /** CTOR */
    FMD();
    /** Initialize */
    void Initialize(Option_t* option="");
    /** Register */
    void Register(Option_t* option="");
    /** Finish */
    void FinishEvent();
    /** Finish */
    void Finish(Option_t* option="");
    /** Deal with a hit in the FMD */
    void Step();
    
     
    ClassDef(FMD,0) //
  };
}

#endif
//____________________________________________________________________
//
// EOF
//
