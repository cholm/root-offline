// -*- mode: C++ -*- 
//____________________________________________________________________ 
//  
// $Id: Cave.h,v 1.9 2006-04-03 15:19:48 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    montecarlo/Cave.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Montecarlo::Cave
*/
#ifndef Montecarlo_Cave
#define Montecarlo_Cave
#ifndef Simulation_Task
# include <simulation/Task.h>
#endif

/** @defgroup montecarlo Simulation classes */
/** @namespace Montecarlo Namespace for all simulation classes
 */
namespace Montecarlo 
{
  /** @class Cave montecarlo/Cave.h <montecarlo/Cave.h>
      @brief Top level volume 
      @ingroup montecarlo cave
      @image html All.png 
   */
  class Cave : public Simulation::Task 
  {
  private:
    Double_t fR;
    Double_t fZ;
  public:
    /** CTOR */
    Cave();
    void DefineParticles();
    /** Initialise */
    void Initialize(Option_t* option="");
    /** Register */
    void Register(Option_t* option="");
    /** Get the magnetic field at @f$ \mathbf{x} = (x,y,z) @f$ in
	units of kilo Gaus (10000 Gaus = 1 T). 
	@param x Space point to get the magnetic field for 
	@param b On return, the magnetic field in units of kilogaus */
    void Field(const TVector3& x, TVector3& b);
    void SetR(Double_t r) { fR = r; }
    void SetZ(Double_t z) { fZ = z; }
    void Step();
    ClassDef(Cave,0) //
  };
}

#endif
//____________________________________________________________________
//
// EOF
//
