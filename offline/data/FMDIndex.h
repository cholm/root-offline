// -*- mode: C++ -*- 
//____________________________________________________________________ 
//  
// $Id: FMDIndex.h,v 1.1 2007-09-28 14:00:54 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
#ifndef data_FMDIndex
#define data_FMDIndex
/** @file    FMDIndex.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Apr  3 12:57:06 2006
    @brief   Index into FMD data structures */
#ifndef ROOT_Rtypes
# include <Rtypes.h>
#endif
#ifndef ROOT_TObject
# include <TObject.h>
#endif
#ifndef ROOT_TString
# include <TString.h>
#endif
#ifndef __IOSFWD__
# include <iosfwd>
#endif

/** @defgroup fmd Forward Multiplicity Array classes */
namespace Data 
{
  /** @class FMDIndex data/FMDIndex.h <data/FMDIndex.h>
      @brief Index of an FMD strip
      @ingroup data fmd 
   */
  class FMDIndex 
  {
  public:
    /** Default constructor */
    FMDIndex();  
    /** Constructor
	@param detector Detector number  
	@param ring     Ring id 
	@param sector   Sector number
	@param strip    Strip number */
    FMDIndex(UShort_t  detector, 
	     Char_t    ring, 
	     UShort_t  sector, 
	     UShort_t  strip);
    /** Copy constructor 
	@param o */
    FMDIndex(const FMDIndex& o);
    /** Destructor */
    virtual ~FMDIndex() {}
    /** Assignment operator 
	@param o Object to assign from */
    FMDIndex& operator=(const FMDIndex& o);
    /** Comparison (equality) operator 
	@param rhs Object to compare to 
	@return @c true if these two objects refer to the same strip  */
    bool operator==(const FMDIndex& rhs) const;
    /** Comparison (less than) operator 
	@param rhs Object to compare to 
	@return @c true if this object is less than @a o */
    bool operator<(const FMDIndex& rhs) const;
    /** @return Detector # */
    UShort_t     Detector()          const { return fDetector; }
    /** @return Ring ID */
    Char_t       Ring()              const { return fRing;     }
    /** @return sector # */
    UShort_t     Sector()            const { return fSector;   }
    /** @return strip # */
    UShort_t     Strip()             const { return fStrip;    }
    /** @param x Detector # */
    void SetDetector(UShort_t x)     { fHash = -1; fDetector = x; }
    /** @param x Ring ID */
    void SetRing(Char_t x)           { fHash = -1; fRing = x; }
    /** @param x sector # */
    void SetSector(UShort_t x)       { fHash = -1; fSector = x; }
    /** @param x strip # */
    void SetStrip(UShort_t x)        { fHash = -1; fStrip = x; }
    /** @return Name */
    const char* Name() const;
  protected:
    /** Calculate the hash value (if not already done), and return it.
	@return Has value of this object  */
    Int_t Hash() const;
    /** Detector number */
    UShort_t fDetector;
    /** Ring Id */
    Char_t   fRing;
    /** Sector number */
    UShort_t fSector;
    /** Strip number */
    UShort_t fStrip;
    /** Cached name */
    mutable TString  fName;  //! 
    /** Cached hash value */
    mutable Int_t    fHash;  //! 
    ClassDef(FMDIndex, 1); // Base class for FMD digits
  };

  //__________________________________________________________________
  /** @class FMDObjIndex data/FMDIndex.h <data/FMDIndex.h>
      @brief TObject version of index of an FMD strip
      @ingroup data fmd 
   */
  class FMDObjIndex : public TObject, public FMDIndex
  {
  public:
    /** Copy CTOR
	@param o Object to copy from */
    FMDObjIndex(const FMDObjIndex& o) : TObject(o), FMDIndex(o) {}
    /** Construct from a pure index
	@param o Object to copy from */
    FMDObjIndex(const FMDIndex& o) : FMDIndex(o) {}
    /** Constrctor
	@param detector Detector
	@param ring     Ring
	@param sector   Sector
	@param strip    Strip */
    FMDObjIndex(UShort_t detector,
		Char_t   ring='\0',
		UShort_t sector=0,
		UShort_t strip=0)
      : FMDIndex(detector, ring, sector, strip)
    {}
    /** DTOR */
    virtual ~FMDObjIndex() {}
    
    /** @return the name */
    virtual const Char_t* GetName() const { return Name(); }
    /** @return always true */
    virtual Bool_t IsSortable() const { return kTRUE; }
    /** Compare to other hit 
	@param o Object to compare to 
	@return -1 if this object is less than @a o, 0 if they are
	equal, and +1 if this object is larger than @a o */
    virtual Int_t Compare(const TObject* o) const;
    ClassDef(FMDObjIndex,1);
  };
}

/** Print an index to a stream
    @param o Output stream
    @param i Index to print
    @return  @a o after pushing @a i to it */
extern std::ostream& 
operator<<(std::ostream& o, Data::FMDIndex& i);

#endif
//____________________________________________________________________
//
// EOF
//


