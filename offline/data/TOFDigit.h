// -*- mode: C++ -*- 
//____________________________________________________________________ 
//  
// $Id: TOFDigit.h,v 1.5 2005-12-14 22:04:27 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    data/TOFDigit.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Data::TOFDigit
*/
#ifndef Data_TOFDigit
#define Data_TOFDigit
#ifndef ROOT_TObject
# include <TObject.h>
#endif
#ifndef ROOT_TAttMarker
# include <TAttMarker.h>
#endif

namespace Data 
{
  /** @class TOFDigit data/TOFDigit.h <data/TOFDigit.h> 
      @brief A digit in the TOF 
      @ingroup data digitization tof
   */
  class TOFDigit : public TObject
  {
  public:
    /** Sector # */
    UShort_t fSector;
    /** Ring # */
    UShort_t fRing;
    /** # of ADC counts */
    Int_t fCounts;
    /** # of TDC counts */
    Int_t fTime;
    
    /** Create a TOF digit 
	@param sector Sector #
	@param ring   Ring #
	@param adc    # of ADC counts
	@param time   # of TDC counts  */
    TOFDigit(UShort_t sector=0, UShort_t ring=0, Int_t adc=-1, Int_t time=-1);
    
    /** Print information on the digit 
	@param option Not used */
    void Print(Option_t* option="") const;
    ClassDef(TOFDigit,1);
  };
}
#endif
