// -*- mode: C++ -*- 
//____________________________________________________________________ 
//  
// $Id: V0Reco.h,v 1.3 2005-12-14 22:04:28 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    data/V0Reco.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Data::V0Reco
*/
#ifndef Data_V0Reco
#define Data_V0Reco
#ifndef ROOT_TObject
# include <TObject.h>
#endif
#ifndef ROOT_TAttMarker
# include <TAttMarker.h>
#endif

namespace Data 
{
  /** @class V0Reco data/V0Reco.h <data/V0Reco.h> 
      @brief A recocontructed point in the V0 
      @ingroup data reconstruction v0
   */
  class V0Reco : public TObject
  {
  public:
    /** Detector ID */
    Char_t     fDetector;
    /** Ring number */
    UShort_t   fRing;
    /** Sector number */
    UShort_t   fSector;
    /** Energy loss in detector */
    Float_t    fEnergyLoss;
    /** multiplicity in detector */
    Float_t    fMultiplicity;
    /** Time */
    Float_t fTime;
    
    /** Make a V0 reco
	@param detector     Detector ID
	@param ring         Ring #
	@param sector       Sector #
	@param energyloss   The energy loss in the detector
	@param multiplicity The multiplicity in the detector 
	@param time         The time */
    V0Reco(Char_t   detector='\0',   UShort_t ring=0, 
	   UShort_t sector=0,         Float_t energyloss=0, 
	   Float_t  multiplicity=0,   Float_t time=0);
    
    /** Print information on the reco 
	@param option Not used */
    void Print(Option_t* option="") const;
    ClassDef(V0Reco,1);
  };
}
#endif
