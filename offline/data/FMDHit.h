// -*- mode: C++ -*- 
//____________________________________________________________________ 
//  
// $Id: FMDHit.h,v 1.9 2006-04-03 15:19:47 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    data/FMDHit.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Data::FMDHit
*/
#ifndef Data_FMDHit
#define Data_FMDHit
#ifndef Simulation_Hit
# include <simulation/Hit.h>
#endif
#ifndef Data_FMDIndex
# include <data/FMDIndex.h>
#endif

namespace Data 
{
  /** @class FMDHit data/FMDHit.h <data/FMDHit.h> 
      @brief A simulated hit in the FMD 
      @ingroup data montecarlo fmd 
   */
  class FMDHit : public Simulation::Hit, public FMDIndex
  {
  public:
    /** Length */ 
    Float_t  fLength;
    /** Default CTOR.   Not used  */
    FMDHit() {}
    /** Make a FMD hit 
	@param detector Detector number  
	@param ring     Ring id 
	@param sector   Sector number
	@param strip    Strip numberr
	@param nTrack   Track number
	@param v        Space position
	@param p        Momentum 
	@param energyLoss Energy deposited in this hit.  
	@param pdg      Particle Data Group particle number 
	@param l        Track length throuh material */
    FMDHit(UShort_t              detector, 
	   Char_t                ring, 
	   UShort_t              sector, 
	   UShort_t              strip,
	   Int_t                 nTrack,
	   const TLorentzVector& v, 
	   const TLorentzVector& p, 
	   Float_t               energyLoss, 
	   Int_t                 pdg,
	   Float_t               l) 
      : Simulation::Hit(nTrack, pdg, v, p, energyLoss), 
	FMDIndex(detector, ring, sector, strip), 
	fLength(0)
    {}
    void SetEnergyLoss(Float_t edep) { fEnergyLoss = edep; }
    /** @return  Name of the hit */
    virtual const Char_t* GetTitle() const { Name(); }
    /** @return always true */
    Bool_t IsSortable() const { return kTRUE; }
    /** Compare to other hit 
	@param o Object to compare to 
	@return -1 if this object is less than @a o, 0 if they are
	equal, and +1 if this object is larger than @a o */
    Int_t Compare(const TObject* o) const 
    {
      const FMDHit* i = dynamic_cast<const FMDHit*>(o);
      if (!i) {
	Fatal("Compare", "Trying to compare a %s to a FMDHit", o->ClassName());
	return 0;
      }
      if (FMDIndex::operator<(*i)) return -1;
      if (FMDIndex::operator==(*i)) return 0;
      return 1;
    }
    ClassDef(FMDHit,1);
  };
}
#endif
