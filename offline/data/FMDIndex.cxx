//____________________________________________________________________ 
//  
// $Id: FMDIndex.cxx,v 1.2 2007-09-28 14:50:40 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    data/FMDIndex.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementation file for Data::FMDIndex
*/
#include "data/FMDIndex.h"
#ifndef __IOSTREAM__
# include <iostream>
#endif
#ifndef __IOMANIP__
# include <iomanip>
#endif

//____________________________________________________________________
ClassImp(Data::FMDIndex);

//____________________________________________________________________
Data::FMDIndex::FMDIndex() 
  : fDetector(0), 
    fRing('\0'),
    fSector(0),
    fStrip(0),
    fHash(-1)
{}


//____________________________________________________________________
Data::FMDIndex::FMDIndex(UShort_t              detector, 
			 Char_t                ring, 
			 UShort_t              sector, 
			 UShort_t              strip)
  : fDetector(detector), 
    fRing(ring),
    fSector(sector),
    fStrip(strip), 
    fHash(-1)
{}

//____________________________________________________________________
Data::FMDIndex::FMDIndex(const FMDIndex& o)
  : fDetector(o.fDetector),
    fRing(o.fRing),
    fSector(o.fSector),
    fStrip(o.fStrip),
    fHash(o.fHash)
{
  // Copy constructor
}

//____________________________________________________________________
Data::FMDIndex&
Data::FMDIndex::operator=(const FMDIndex& o)
{
  // Assignment operator
  fDetector = o.fDetector;
  fRing     = o.fRing;
  fSector   = o.fSector;
  fStrip    = o.fStrip;
  fHash     = o.fHash;
  return *this;
}
//____________________________________________________________________
Int_t
Data::FMDIndex::Hash() const
{
  if (fHash < 0) {
    size_t ringi = (fRing == 'I' ||  fRing == 'i' ? 0 : 1);
    fHash = (fStrip + 512 * (fSector + 40 * (ringi + 2 * (fDetector-1))));
  }
  return fHash;
}

//____________________________________________________________________
const Char_t* 
Data::FMDIndex::Name() const 
{ 
  if (fName.IsNull()) 
    fName = Form("FMD%d%c[%2d,%3d]", fDetector, fRing, fSector, fStrip);
  return fName.Data(); 
}

//====================================================================
ClassImp(Data::FMDObjIndex);

//____________________________________________________________________
Int_t 
Data::FMDObjIndex::Compare(const TObject* o) const 
{
  const FMDObjIndex* i = dynamic_cast<const FMDObjIndex*>(o);
  if (!i) {
    Fatal("Compare", "Trying to compare a %s to a FMDObjIndex",
	  o->ClassName());
    return 0;
  }
  if (FMDIndex::operator<(*i)) return -1;
  if (FMDIndex::operator==(*i)) return 0;
  return 1;
}


//====================================================================
std::ostream&
operator<<(std::ostream& o, const Data::FMDIndex& i)
{
  return o << i.Name();
}

//____________________________________________________________________
//
// EOF
//
