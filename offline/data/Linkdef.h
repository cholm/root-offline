// -*- mode: c++ -*-
//____________________________________________________________________ 
//  
// $Id: Linkdef.h,v 1.6 2006-04-03 15:19:47 cholm Exp $ 
//
//  ROOT generic framework
//  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
// Author: Christian Holm Christensen <cholm@nbi.dk>
// Update: 2002-06-20 15:06:41+0200
// Copyright: 2002 (C) Christian Holm Christensen (LGPL)
//
/** @file    data/Linkdef.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:24:27 2004
    @brief   Link specification for CINT
*/

#ifndef __CINT__
#error Not for compilation
#endif

#pragma link off all functions;
#pragma link off all globals;
#pragma link off all classes;

#pragma link C++ namespace Data;
#pragma link C++ class     Data::TOFHit+;
#pragma link C++ class     Data::TOFDigit+;
#pragma link C++ class     Data::FMDIndex+;
#pragma link C++ class     Data::FMDObjIndex+;
#pragma link C++ class     Data::FMDHit+;
#pragma link C++ class     Data::FMDDigit+;
#pragma link C++ class     Data::FMDReco+;
#pragma link C++ class     Data::V0Hit+;
#pragma link C++ class     Data::V0Digit+;
#pragma link C++ class     Data::V0Reco+;
#pragma link C++ class     Data::Vertex+;
#pragma link C++ class     Data::FMDMap<Double_t>+;
// #pragma link C++ typedef   Data::FMDDoubleMap;


//____________________________________________________________________ 
//  
// EOF
//
