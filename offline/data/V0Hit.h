// -*- mode: C++ -*- 
//____________________________________________________________________ 
//  
// $Id: V0Hit.h,v 1.5 2006-04-03 15:19:47 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    data/V0Hit.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Data::V0Hit
*/
#ifndef Data_V0Hit
#define Data_V0Hit
#ifndef Simulation_Hit
# include <simulation/Hit.h>
#endif
/** @defgroup v0 V0 classes */
namespace Data 
{
  /** @class V0Hit data/V0Hit.h <data/V0Hit.h> 
      @brief A simulated hit in the V0 
      @ingroup data montecarlo v0
   */
  class V0Hit : public Simulation::Hit
  {
  public:
    /** Detector Id */
    Char_t   fDetector;
    /** Ring number */
    UShort_t   fRing;
    /** Sector number */
    UShort_t fSector;

    /** Default CTOR.   Not used  */
    V0Hit() : fDetector('\0'), fRing(0), fSector(0) {}
    /** Make a V0 hit 
	@param detector Detector ID
	@param ring     Ring number 
	@param sector   Sector number
	@param nTrack   Track number
	@param v        Space position
	@param p        Momentum 
	@param energyLoss Energy deposited in this hit.  
	@param pdg      Particle Data Group particle number */
    V0Hit(Char_t                detector, 
	  UShort_t              ring,
	  UShort_t              sector, 
	  Int_t                 nTrack,
	  const TLorentzVector& v, 
	  const TLorentzVector& p, 
	  Float_t               energyLoss, 
	  Int_t                 pdg)
      : Simulation::Hit(nTrack, pdg, v, p, energyLoss), 
	fDetector(detector), fRing(ring), fSector(sector)
    {}
    
    /** @return  Name of the hit */
    virtual const Char_t* GetTitle() const;

    ClassDef(V0Hit,1);
  };
}
#endif
