// -*- mode: C++ -*- 
//____________________________________________________________________ 
//  
// $Id: Vertex.h,v 1.2 2005-12-14 22:04:28 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    data/Vertex.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Data::Vertex
*/
#ifndef Data_Vertex
#define Data_Vertex
#ifndef ROOT_TObject
# include <TObject.h>
#endif
#ifndef ROOT_TAttMarker
# include <TAttMarker.h>
#endif
#ifndef ROOT_TString
# include <TString.h>
#endif

namespace Data 
{
  /** @class Vertex data/Vertex.h <data/Vertex.h> 
      @brief A recocontructed point in the V0 
      @ingroup data reconstruction
   */
  class Vertex : public TObject
  {
  protected:
    mutable TString fTitle; //!
  public:
    /** X coordinate */
    Float_t fX;
    /** Y coordinate */
    Float_t fY;
    /** Z coordinate */
    Float_t fZ;
    /** T coordinate */
    Float_t fT;
    
    /** Make a V0 reco
	@param x     X position
	@param y     Y position
	@param z     Z position
	@param t     Time */
    Vertex(Double_t x=0, Double_t y=0, 
	   Double_t z=0, Double_t t=0);
    
    virtual const Char_t* GetName() const { return "vertex"; }
    virtual const Char_t* GetTitle() const;

    /** Print information on the reco 
	@param option Not used */
    void Print(Option_t* option="") const;
    ClassDef(Vertex,1);
  };
}
#endif
