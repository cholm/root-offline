// -*- mode: C++ -*- 
//____________________________________________________________________ 
//  
// $Id: FMDMap.h,v 1.3 2006-04-03 15:19:47 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    data/FMDMap.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Data::FMDMap
*/
#ifndef Data_FMDMap
#define Data_FMDMap
#ifndef ROOT_TObject
# include <TObject.h>
#endif
#ifndef data_FMDIndex
# include <data/FMDIndex.h>
#endif
#ifndef __MAP__
# include <map>
#endif
#ifdef G__DICTIONARY
// Blow is an ugly hack to force proper instantation of
// std::map<FMDIndex,T> in the dictionary. 
namespace ROOT 
{
  namespace Shadow 
  {
    namespace Data 
    {
      using ::Data::FMDIndex;
    }
  }
}
#endif 

namespace Data 
{
  /** @class FMDMap data/FMDMap.h <data/FMDMap.h> 
      @brief A simulated hit in the FMD 
      @ingroup fmd
   */
  template <typename T>
  struct FMDMap : public TObject
  {
    typedef FMDIndex Index_t;
    typedef T Data_t;
    typedef std::map<Index_t,T> Map_t;
    
    /** Size of array */
    Int_t     fN;
    /** Max detector */
    UInt_t    fNDetector;
    /** Max rings */
    UInt_t    fNRing;
    /** Max sectors */
    UInt_t    fNSector;
    /** Max strips */
    UInt_t    fNStrip;
    /** The data */
    Map_t     fData; //[fN]

    /** Create a map object */
    FMDMap()
      : fNDetector(0),
	fNRing(0), 
	fNSector(0),
	fNStrip(0)
    {}
    /** Copy an FMD map 
	@param o To copy from */ 
    FMDMap(const FMDMap& o) 
      : fData(o.fData), 
	fNDetector(o.fNDetector),
	fNRing(o.fNRing), 
	fNSector(o.fNSector),
	fNStrip(o.fNStrip)
    {}
    /** Assign a map object 
	@param o to assign from 
	@return reference to this */
    FMDMap& 
    operator=(const FMDMap& o) 
    {
      fData = o.fData;
      fNDetector = o.fNDetector;
      fNRing     = o.fNRing;
      fNSector   = o.fNSector;
      fNStrip    = o.fNStrip;
      return *this;
    }
	
    /** Create a cache object 
	@param ndet  Max detectors
	@param nring Max rings
	@param nsec  Max sectors
	@param nstr  Max strips  */
    FMDMap(UInt_t ndet, UInt_t nring=2, UInt_t nsec=40, UInt_t nstr=512) 
      : fNDetector(ndet),
	fNRing(nring), 
	fNSector(nsec),
	fNStrip(nstr)
    {}
    /** Destructor */
    virtual ~FMDMap() 
    {}
    

    /** Reset data to @a val
	@param val No value */
    void Reset(Data_t val=Data_t()) 
    {
      fData.clear();
    }
    Data_t& operator[](const FMDIndex& i) 
    {
      return fData[i];
    }
    const Data_t& operator[](const FMDIndex& i) const
    {
      typename Map_t::const_iterator p = fData.find(i);
      if (p == fData.end()) 
	Fatal("operator[]", "No data for %s", i.Name());
      return p->second;
    }
    Bool_t Has(const FMDIndex& i) const 
    {
      typename Map_t::const_iterator p = fData.find(i);
      if (p == fData.end()) return kFALSE;
      return kTRUE;
    }
    ClassDef(FMDMap,1);
  };
  //__________________________________________________________________
  typedef FMDMap<Double_t> FMDDoubleMap;

}
#endif
//
// EOF
//
