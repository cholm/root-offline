// -*- mode: C++ -*- 
//____________________________________________________________________ 
//  
// $Id: V0Digit.h,v 1.2 2005-12-14 22:04:27 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    data/V0Digit.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Data::V0Digit
*/
#ifndef Data_V0Digit
#define Data_V0Digit
#ifndef ROOT_TObject
# include <TObject.h>
#endif
#ifndef ROOT_TAttMarker
# include <TAttMarker.h>
#endif

namespace Data 
{
  /** @class V0Digit data/V0Digit.h <data/V0Digit.h> 
      @brief A digit in the V0 
      @ingroup data digitization v0
   */
  class V0Digit : public TObject
  {
  public:
    /** Detector number */
    Char_t     fDetector;
    /** Ring Id */
    UShort_t   fRing;
    /** Sector number */
    UShort_t   fSector;
    /** number of ADC counts */
    UShort_t   fCounts;
    /** number of TDC counts */
    UShort_t   fTime;
    
    /** Make a V0 digit
	@param detector Detector Id
	@param ring     Ring #
	@param sector   Sector #
	@param counts   ADC count 
	@param time     TDC count */
    V0Digit(Char_t   detector='\0', 
	    UShort_t ring=0, 
	    UShort_t sector=0,
	    UShort_t counts=0, 
	    UShort_t time=0);
    
    /** Print information on the digit 
	@param option Not used */
    void Print(Option_t* option="") const;
    ClassDef(V0Digit,1);
  };
}
#endif
