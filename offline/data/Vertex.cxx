//____________________________________________________________________ 
//  
// $Id: Vertex.cxx,v 1.2 2007-09-28 14:50:40 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    data/Vertex.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementation file for Data::Vertex
*/
#include "data/Vertex.h"
#include <TROOT.h>
#ifndef __IOSTREAM__
# include <iostream>
#endif
#ifndef __IOMANIP__
# include <iomanip>
#endif

//____________________________________________________________________
ClassImp(Data::Vertex)

//____________________________________________________________________
Data::Vertex::Vertex(Double_t x, Double_t y, Double_t z, Double_t t)
  : fX(x),
    fY(y),
    fZ(z),
    fT(t)
{}

//____________________________________________________________________
const Char_t*
Data::Vertex::GetTitle() const 
{
  if (fTitle.IsNull())
    fTitle = Form("[%f,%f,%f,%f]", fX, fY, fZ, fT);
  return fTitle.Data();
}

//____________________________________________________________________
void
Data::Vertex::Print(Option_t* option) const
{
  TROOT::IndentLevel();
  std::cout << "Data::Vertex: " << GetName() << ": " << GetTitle()
	    << std::endl;
}

    
//____________________________________________________________________
//
// EOF
//
