// -*- mode: C++ -*- 
//____________________________________________________________________ 
//  
// $Id: FMDReco.h,v 1.5 2006-11-08 01:41:42 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    data/FMDReco.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Data::FMDReco
*/
#ifndef Data_FMDReco
#define Data_FMDReco
#ifndef ROOT_TObject
# include <TObject.h>
#endif
#ifndef Data_FMDIndex
# include <data/FMDIndex.h>
#endif

namespace Data 
{
  /** @class FMDReco data/FMDReco.h <data/FMDReco.h> 
      @brief A recocontructed point in the FMD 
      @ingroup data reconstruction fmd
   */
  class FMDReco : public FMDObjIndex
  {
  public:
    /** Energy loss in detector */
    Float_t    fEnergyLoss;
    /** multiplicity in detector */
    Float_t    fMultiplicity;
    
    /** Make a FMD reco
	@param detector     Detector #
	@param ring         Ring ID
	@param sector       Sector #
	@param strip        Strip #
	@param energyloss   The energy loss in the detector
	@param multiplicity The multiplicity in the detector */
    FMDReco(UShort_t detector=0,  Char_t   ring='\0', 
	    UShort_t sector=0,    UShort_t strip=0,
	    Float_t energyloss=0, Float_t  multiplicity=0)
      : FMDObjIndex(detector, ring, sector, strip), 
	fEnergyLoss(energyloss), 
	fMultiplicity(multiplicity)
    {}
    /** Constructor 
	@param idx Index
	@param eloss Energy loss
	@param mult  Multiplicity */
    FMDReco(const FMDIndex& idx, Float_t eloss=0, Float_t mult=0) 
      : FMDObjIndex(idx), fEnergyLoss(eloss), fMultiplicity(mult)
    {}
    
    /** Print information on the reco 
	@param option Not used */
    void Print(Option_t* option="") const;
    ClassDef(FMDReco,1);
  };
}
#endif
