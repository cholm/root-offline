// -*- mode: C++ -*- 
//____________________________________________________________________ 
//  
// $Id: TOFHit.h,v 1.6 2006-04-03 15:19:47 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    data/TOFHit.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Data::TOFHit
*/
#ifndef Data_TOFHit
#define Data_TOFHit
#ifndef Simulation_Hit
# include <simulation/Hit.h>
#endif
/** @defgroup tof Time Of Flight classes */
namespace Data 
{
  /** @class TOFHit data/TOFHit.h <data/TOFHit.h> 
      @brief A simulated hit in the TOF 
      @ingroup data montecarlo tof
   */
  class TOFHit : public Simulation::Hit
  {
  public:
    /** Sector number */
    UShort_t   fSector;
    /** Ring number */
    UShort_t   fRing;

    /** Default CTOR.   Not used  */
    TOFHit() : fSector(0), fRing(0) {}
    /** Make a TOF hit 
	@param sector   Detector number
	@param ring     Ring number 
	@param nTrack   Track number
	@param v        Space position
	@param p        Momentum 
	@param energyLoss Energy deposited in this hit.  
	@param pdg      Particle Data Group particle number */
    TOFHit(UShort_t              sector,
	   UShort_t              ring,
	   Int_t                 nTrack,
	   const TLorentzVector& v, 
	   const TLorentzVector& p, 
	   Float_t               energyLoss, 
	   Int_t                 pdg)
      : Simulation::Hit(nTrack, pdg, v, p, energyLoss), 
	fSector(sector), fRing(ring)
    {}
    
    /** @return  Name of the hit */
    virtual const Char_t* GetTitle() const;

    ClassDef(TOFHit,1);
  };
}
#endif
