// -*- mode: C++ -*- 
//____________________________________________________________________ 
//  
// $Id: FMD.h,v 1.11 2006-11-08 01:41:43 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    geometry/FMD.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Geometry::FMD
*/
#ifndef Geometry_FMD
#define Geometry_FMD
#ifndef Framework_Task
# include <framework/Task.h>
#endif
#ifndef ROOT_TObjArray
# include <TObjArray.h>
#endif
class TVector2;
class TGeoVolume;
class TGeoMedium;



namespace Geometry 
{
  //__________________________________________________________________
  /** @class FMDRing geometry/FMD.h <geometry/FMD.h>
      @brief Geometry description and parameters of a ring in the FMD
      detector. 
      @ingroup geometry fmd
      As there are only 2 kinds of rings @e Inner (@c 'I') and @e
      Outer (@c 'O') the two objects of this class is owned by the
      Geometry::FMD singleton object.  The 3 Geometry::FMDDetector
      objects shares these two instances as needed. 
      @image html FMDI.png
      @image html FMDO.png
  */
  class FMDRing : public Framework::Task
  {
  public:
    FMDRing(Char_t fId);
    /** Initialize the geometry 
	@param option  ignored */
    virtual void Register(Option_t* option);
    /** Do nothing 
	@param option  ignored */
    void Exec(Option_t* option="") {}
    
    /** @param x Value of The Id of this ring type */
    void SetId(Char_t x) { fId = x; }
    /** @param x Value of With of bonding pad on sensor */
    void SetBondingWidth(Double_t x=.5) { fBondingWidth = x; } //*MENU*
    /** @param x Value of Size of wafer the sensor was made from */
    void SetWaferRadius(Double_t x=13.4/2) { fWaferRadius = x; } //*MENU*
    /** @param x Value of Thickness of sensor */
    void SetSiThickness(Double_t x=.03) { fSiThickness = x; } //*MENU*
    /** @param x Value of Lower radius of ring */
    void SetLowR(Double_t x) { fLowR = x; } //*MENU*
    /** @param x Value of Upper radius of ring */
    void SetHighR(Double_t x) { fHighR = x; } //*MENU*
    /** @param x Value of Opening angle of the silicon wafers */
    void SetTheta(Double_t x) { fTheta = x; } //*MENU*
    /** @param x Value of Number of strips */
    void SetNStrips(Int_t x) { fNStrips = x; } //*MENU*
    /** @param x Value of How far the ring extends beyond the z value given. */
    void SetRingDepth(Double_t x) { fRingDepth = x; } //*MENU*
    /** @param x Value of Radius of support legs */
    void SetLegRadius(Double_t x=.5) { fLegRadius = x; } //*MENU*
    /** @param x Value of Radius of support legs */
    void SetLegLength(Double_t x=1) { fLegLength = x; } //*MENU*
    /** @param x Value of Radius of support legs */
    void SetLegOffset(Double_t x=2) { fLegOffset = x; } //*MENU*
    /** @param x Value of Staggering offset */
    void SetModuleSpacing(Double_t x=1) { fModuleSpacing = x; } //*MENU*
    /** @param x Value of Thickness of print board */
    void SetPrintboardThickness(Double_t x=.1) { fPrintboardThickness = x; } //*MENU*
    void SetSpacerHeight(Double_t h=.03)  { fSpacerHeight = h; } //*MENU*

    /** Whether to use Assemblies 
        @param use Enable this mode */
    void UseAssembly(Bool_t use=kTRUE) { fUseAssembly = use; }//*MENU*
    /** @return The Id of this ring type */
    Char_t GetId() const { return fId; }
    /** @return With of bonding pad on sensor */
    Double_t GetBondingWidth() const { return fBondingWidth; }
    /** @return Size of wafer the sensor was made from */
    Double_t GetWaferRadius() const { return fWaferRadius; }
    /** @return Thickness of sensor */
    Double_t GetSiThickness() const { return fSiThickness; }
    /** @return Lower radius of ring */
    Double_t GetLowR() const { return fLowR; }
    /** @return Upper radius of ring */
    Double_t GetHighR() const { return fHighR; }
    /** @return Opening angle of the sector (half that of silicon wafers) */
    Double_t GetTheta() const { return fTheta; }
    /** @return Number of strips */
    Int_t GetNStrips() const { return fNStrips; }
    /** @return Number of sectors */
    Int_t GetNSectors() const { return Int_t(360. / fTheta); }
    /** @return Number of modules (2 sectors per module) */
    Int_t GetNModules() const { return GetNSectors() / 2; }
    /** @return How far the ring extends beyond the z value given. */
    Double_t GetRingDepth() const { return fRingDepth; }
    /** @return Radius of support legs */
    Double_t GetLegRadius() const { return fLegRadius; }
    /** @return Radius of support legs */
    Double_t GetLegLength() const { return fLegLength; }
    /** @return Radius of support legs */
    Double_t GetLegOffset() const { return fLegOffset; }
    /** @return Staggering offset */
    Double_t GetModuleSpacing() const { return fModuleSpacing; }
    /** @return Thickness of print board */
    Double_t GetPrintboardThickness() const { return fPrintboardThickness; }
    /** @return List of verticies */
    const TObjArray& GetVerticies() const { return fVerticies; }
    /** @return Number of verticies */
    Int_t GetNVerticies() const { return fVerticies.GetEntries(); }
    /** @param i Vertex number 
	@return the ith vertex */
    TVector2* GetVertex(Int_t i) const;
    /** @return spacing between sensor and hybrid */
    Double_t GetSpacerHeight() const { return fSpacerHeight; }
    /** Get the top-volume of this ring */
    TGeoVolume* GetTopVolume() const { return fTopVolume; }
    /** Get the bottom-volume of this ring */
    TGeoVolume* GetBottomVolume() const { return fBottomVolume; }
    /** Translate detector coordinates (sector,strip) into
	@f$(x,y,z)@f$ cartisian coordinates 
	@param sector Sector # 
	@param strip  Strip #
	@param x      Cartisian X coordinate
	@param y      Cartisian Y coordinate 
	@param z      Cartisian Z coordinate */
    void Detector2XYZ(UShort_t sector, UShort_t strip, 
		      Double_t& x, Double_t& y, Double_t& z) const;

    /** Name of Active volumes */
    static const char* fgkActiveName;
    /** Name of Sector volumes */
    static const char* fgkSectorName;
    /** Name of Strip volumes */
    static const char* fgkStripName;
    /** Name of Sensor volumes */
    static const char* fgkSensorName;
    /** Name of PCB volumes */
    static const char* fgkPCBName;
    /** Name of copper volumes */
    static const char* fgkCuName;
    /** Name of chip volumes */
    static const char* fgkChipName;
    /** Name of LongLeg volumes */
    static const char* fgkLongLegName;
    /** Name of ShortLeg volumes */
    static const char* fgkShortLegName;
    /** Name of Front volumes */
    static const char* fgkFrontVName;
    /** Name of Back volumes */
    static const char* fgkBackVName;
    /** Name of Top ring volumes */
    static const char* fgkRingTopName;
    /** Name of Bottom ring volumes */
    static const char* fgkRingBotName;
  private: 
    /** The Id of this ring type */
    Char_t   fId;
    /** With of bonding pad on sensor */
    Double_t fBondingWidth;
    /** Size of wafer the sensor was made from */
    Double_t fWaferRadius;
    /** Thickness of sensor */
    Double_t fSiThickness;
    /** Lower radius of ring */
    Double_t fLowR;
    /** Upper radius of ring */
    Double_t fHighR;
    /** Opening angle of the silicon wafers */
    Double_t fTheta;
    /** Number of strips */
    Int_t fNStrips;
    /** How far the ring extends beyond the z value given. */
    Double_t fRingDepth;
    /** Radius of support legs */
    Double_t fLegRadius;
    /** Radius of support legs */
    Double_t fLegLength;
    /** Radius of support legs */
    Double_t fLegOffset;
    /** Staggering offset */
    Double_t fModuleSpacing;
    /** Thickness of print board */
    Double_t fPrintboardThickness;
    /** List of verticies */
    TObjArray fVerticies;
    /** Spacing between sensor and hybrid */
    Double_t fSpacerHeight;
    /** Top-level volume of ring */ 
    TGeoVolume* fTopVolume;
    /** Top-level volume of ring */ 
    TGeoVolume* fBottomVolume;
    /** Whether to use assemblies */
    Bool_t fUseAssembly;
    ClassDef(FMDRing, 0);
  };

  //__________________________________________________________________
  /** @class FMDDetector geometry/FMD.h <geometry/FMD.h>
      @brief Base class for the geometry description and parameters of
      the FMD sub  detectors FMD1, FMD2, and FMD3.  
      @ingroup geometry fmd
      This class hold common parameters of the specific FMD
      detectors. 
   */
  class FMDDetector : public Framework::Task
  {
  public:
    FMDDetector(Int_t id, FMDRing* inner, FMDRing* outer);
    /** Register the geometry 
	@param option  ignored */
    virtual void Initialize(Option_t* option);
    /** Register the geometry 
	@param option  ignored */
    virtual void Register(Option_t* option);
    /** Do nothing 
	@param option  ignored */
    void Exec(Option_t* option="") {}
    
    /** @param x Detector number */
    void SetId(Int_t x) { fId = x; }
    /** @param x Position of outer ring along z */
    void SetInnerZ(Double_t x) { fInnerZ = x; }
    /** @param x Position of outer ring along z */
    void SetOuterZ(Double_t x) { fOuterZ = x; }
    /** @param x Thickness of honeycomb plate */
    void SetHoneycombThickness(Double_t x=1) { fHoneycombThickness = x; }
    /** @param x Thickness of aluminium of honeycomb */
    void SetAlThickness(Double_t x=.1) { fAlThickness = x; }
    /** @param x Inner radius of inner honeycomb */
    void SetInnerHoneyLowR(Double_t x) { fInnerHoneyLowR = x; }
    /** @param x Outer radius of inner honeycomb */
    void SetInnerHoneyHighR(Double_t x) { fInnerHoneyHighR = x; }
    /** @param x Inner radius of outer honeycomb */
    void SetOuterHoneyLowR(Double_t x) { fOuterHoneyLowR = x; }
    /** @param x Outer radius of outer honeycomb */
    void SetOuterHoneyHighR(Double_t x) { fOuterHoneyHighR = x; }
    
    /** @return Detector number */
    Int_t GetId() const { return fId; }
    /** @return Position of outer ring along z */
    Double_t GetInnerZ() const { return fInnerZ; }
    /** @return Position of outer ring along z */
    Double_t GetOuterZ() const { return fOuterZ; }
    /** @return Thickness of honeycomb plate */
    Double_t GetHoneycombThickness() const { return fHoneycombThickness; }
    /** @return Thickness of aluminium of honeycomb */
    Double_t GetAlThickness() const { return fAlThickness; }
    /** @return Inner radius of inner honeycomb */
    Double_t GetInnerHoneyLowR() const { return fInnerHoneyLowR; }
    /** @return Outer radius of inner honeycomb */
    Double_t GetInnerHoneyHighR() const { return fInnerHoneyHighR; }
    /** @return Inner radius of outer honeycomb */
    Double_t GetOuterHoneyLowR() const { return fOuterHoneyLowR; }
    /** @return Outer radius of outer honeycomb */
    Double_t GetOuterHoneyHighR() const { return fOuterHoneyHighR; }
    /** Whether to use Assemblies 
        @param use Enable this mode */
    void UseAssembly(Bool_t use=kTRUE) { fUseAssembly = use; }//*MENU*
    
    /** @return Inner ring information */
    FMDRing* GetInner() const { return fInner; }
    /** @return Outer ring information */
    FMDRing* GetOuter() const { return fOuter; }
    /** @param id Id of ring to get 
	@return Pointer to ring, 0 on failure */
    FMDRing* GetRing(Char_t id) const;
    /** @param id Id of ring to get 
	@return Z position of ring or 0 on failure */
    Double_t GetRingZ(Char_t id) const;
    /** Get the top-volume of this ring */
    TGeoVolume* GetTopVolume() const { return fTopVolume; }
    /** Get the top-volume of this ring */
    TGeoVolume* GetBottomVolume() const { return fBottomVolume; }
    
    /** Translate detector coordinates (ring,sector,strip) into
	@f$(x,y,z)@f$ cartisian coordinates 
	@param ring   Ring ID
	@param sector Sector # 
	@param strip  Strip #
	@param x      Cartisian X coordinate
	@param y      Cartisian Y coordinate 
	@param z      Cartisian Z coordinate */
    void Detector2XYZ(Char_t ring, UShort_t sector, UShort_t strip, 
		      Double_t& x, Double_t& y, Double_t& z) const;

    /** Name of Honeycomb volumes */
    static const char* fgkHCName;
    /** Name of Inner honeycomb volumes */
    static const char* fgkIHCName;
    /** Name of Half FMD volumes */
    static const char* fgkFMDName;
  protected:
    /** Detector number */
    Int_t fId;
    /** Position of outer ring along z */
    Double_t fInnerZ;
    /** Position of outer ring along z */
    Double_t fOuterZ;
    /** Thickness of honeycomb plate */
    Double_t fHoneycombThickness;
    /** Thickness of aluminium of honeycomb */
    Double_t fAlThickness;
    /** Inner radius of inner honeycomb */
    Double_t fInnerHoneyLowR;
    /** Outer radius of inner honeycomb */
    Double_t fInnerHoneyHighR;
    /** Inner radius of outer honeycomb */
    Double_t fOuterHoneyLowR;
    /** Outer radius of outer honeycomb */
    Double_t fOuterHoneyHighR;
    /** Pointer to inner ring information */ 
    FMDRing* fInner;
    /** Pointer to outer ring information */ 
    FMDRing* fOuter;
    /** Top volume */
    TGeoVolume* fTopVolume;
    /** Top volume */
    TGeoVolume* fBottomVolume;
    /** Whether to use assemblies */
    Bool_t fUseAssembly;
    /** Z coordinate of mid point of mother */
    Double_t fZ;
    ClassDef(FMDDetector, 1) // 
  };
  
  //__________________________________________________________________
  /** @class FMD1 geometry/FMD.h <geometry/FMD.h> 
      @brief Geometry description and parameters of the FMD1
      detector. 
      @ingroup geometry fmd
      The FMD1 only has one ring. 
      @image html FMD1.png
   */
  class FMD1 : public FMDDetector 
  {
  protected: 
  public:
    FMD1(FMDRing* inner);
    /** Register the geometry */
    virtual void Register(Option_t* option="");
    ClassDef(FMD1, 1)
  };

  //__________________________________________________________________
  /** @class FMD2 geometry/FMD.h <geometry/FMD.h> 
      @brief Geometry description and parameters of the FMD2
      detector. 
      @ingroup geometry fmd
      @image html FMD2.png
   */
  class FMD2 : public FMDDetector 
  {
  protected: 
  public: 
    FMD2(FMDRing* inner, FMDRing* outer);
    /** Register the geometry */
    virtual void Register(Option_t* option="");
    ClassDef(FMD2, 1)
  };

  //__________________________________________________________________
  /** @class FMD3 geometry/FMD.h <geometry/FMD.h> 
      @brief Geometry description and parameters of the FMD3
      detector. 
      @ingroup geometry fmd
      FMD3 has a fairly complicated support structure 
      @image html FMD3.png
   */
  class FMD3 : public FMDDetector 
  {
  public: 
    FMD3(FMDRing* inner, FMDRing* outer);

    /** Register the geometry */
    virtual void Register(Option_t* option="");

    /** @param z Z position of front of nose */
    void SetNoseZ(Double_t z=-46) { fNoseZ = z; } //*MENU*
    /** @param r Nose inner radius */
    void SetNoseLowR(Double_t r=5.5) { fNoseLowR = r; } //*MENU*
    /** @param r Nose outer radius */
    void SetNoseHighR(Double_t r=6.7) { fNoseHighR = r; } //*MENU*
    /** @param l Length of nose in Z */
    void SetNoseLength(Double_t l=2.8) { fNoseLength = l; } //*MENU*
    /** @param r Inner radius of base of cone */
    void SetBackLowR(Double_t r=61./2) { fBackLowR = r; } //*MENU*
    /** @param r Outer radius of base of cone */
    void SetBackHighR(Double_t r=66.8/2) { fBackHighR = r; } //*MENU*
    /** @param l Length of base of cone in Z */
    void SetBackLength(Double_t l=1.4) { fBackLength = l; } //*MENU*
    /** @param t Thickness of support beams */
    void SetBeamThickness(Double_t t=.5) { fBeamThickness = t; } //*MENU*
    /** @param w Width of support beams */
    void SetBeamWidth(Double_t w=6) { fBeamWidth = w; } //*MENU*
    /** @param l Length of the cone in Z */
    void SetConeLength(Double_t l=30.9) { fConeLength = l; } //*MENU*
    /** @param r Outer radius of flanges */
    void SetFlangeR(Double_t r=49.25) { fFlangeR = r; } //*MENU*
    /** @param n Number of support beams */
    void SetNBeam(Int_t n=8) { fNBeam = n; } //*MENU*
    /** @param n Number of support flanges */
    void SetNFlange(Int_t n=4) { fNFlange = n; } //*MENU*

    /** @return Z position of front of nose */
    Double_t GetNoseZ() const { return fNoseZ; }
    /** @return Nose inner radius */
    Double_t GetNoseLowR() const { return fNoseLowR; }
    /** @return Nose outer radius */
    Double_t GetNoseHighR() const { return fNoseHighR; }
    /** @return Length of nose in Z */
    Double_t GetNoseLength() const { return fNoseLength; }
    /** @return Inner radius of base of cone */
    Double_t GetBackLowR() const { return fBackLowR; }
    /** @return Outer radius of base of cone */
    Double_t GetBackHighR() const { return fBackHighR; }
    /** @return Length of base of cone in Z */
    Double_t GetBackLength() const { return fBackLength; }
    /** @return Thickness of support beams */
    Double_t GetBeamThickness() const { return fBeamThickness; }
    /** @return Width of support beams */
    Double_t GetBeamWidth() const { return fBeamWidth; }
    /** @return Length of the cone in Z */
    Double_t GetConeLength() const { return fConeLength; }
    /** @return Outer radius of flanges */
    Double_t GetFlangeR() const { return fFlangeR; }
    /** @return Midpoint of mother volume */
    Double_t GetZ() const { return fZ; }
    /** @return Slope of cone */
    Double_t GetAlpha() const { return fAlpha; }
    /** @return Number of support beams */
    Int_t GetNBeam() const { return fNBeam; }
    /** @return Number of support flanges */
    Int_t GetNFlange() const { return fNFlange; }
    
    /** Get the cone radii at @a z. 
	@param z Point to evaulate at 
	@param opt If @c "O" get the outer radii, if @c "I" get the
	inner radii. 
	@return the radius of the cone */
    Double_t ConeR(Double_t z, Option_t* opt="O") const;

    /** Name of Nose volumes */
    static const char* fgkNoseName;
    /** Name of Back volumes */
    static const char* fgkBackName;
    /** Name of Beam volumes */
    static const char* fgkBeamName;
    /** Name of Flange volumes */
    static const char* fgkFlangeName;

  protected: 
    /** Z position of front of nose */
    Double_t fNoseZ;
    /** Nose inner radius */
    Double_t fNoseLowR;
    /** Nose outer radius */
    Double_t fNoseHighR;
    /** Length of nose in Z */
    Double_t fNoseLength;
    /** Inner radius of base of cone */
    Double_t fBackLowR;
    /** Outer radius of base of cone */
    Double_t fBackHighR;
    /** Length of base of cone in Z */
    Double_t fBackLength;
    /** Thickness of support beams */
    Double_t fBeamThickness;
    /** Width of support beams */
    Double_t fBeamWidth;
    /** Length of the cone in Z */
    Double_t fConeLength;
    /** Outer radius of flanges */
    Double_t fFlangeR;
    /** Slope of cone */
    Double_t fAlpha;
    /** Number of support beams */
    Int_t fNBeam;
    /** Number of support flanges */
    Int_t fNFlange;
    ClassDef(FMD3, 1);
  };

  //__________________________________________________________________
  /** @class FMD geometry/FMD.h <geometry/FMD.h>
      @brief FMD geometry  
      @ingroup geometry fmd
      @image html FMD.png 
   */
  class FMD : public Framework::Task 
  {
  public:
    /** CTOR */
    FMD();
    /** Register @param option Option */
    void Initialize(Option_t* option="");
    /** Does nothing @param option Option */
    void Register(Option_t* option="") {}
    /** Does nothing @param option Option */
    void Exec(Option_t* option="") {}
    /**  Whether to use vacuum instead of air for void volumes 
        @param use Enable this mode */
    void UseVacuum(Bool_t use=kTRUE) { fUseVacuum = use; } //*MENU*
    /** @return pointer to inner task */
    FMDRing*     GetInner() const { return fInner; }
    /** @return pointer to outer task */
    FMDRing*     GetOuter() const { return fOuter; }
    /** @return pointer to FMD1 task */
    FMDDetector* GetFMD1()  const { return fFMD1; }
    /** @return pointer to FMD2 task */
    FMDDetector* GetFMD2()  const { return fFMD2; }
    /** @return pointer to FMD3 task */
    FMDDetector* GetFMD3()  const { return fFMD3; }
    /** Get detector with # @a i
	@param i Detector #
	@return  Pointer to detector task */
    FMDDetector* GetDetector(Int_t i) const;
    /** Get ring with ID @a i 
	@param i Ring ID
	@return Pointer to ring task  */
    FMDRing*     GetRing(Char_t i) const;

    /** Set ring task for ring with id @a i 
	@param i Ring id
	@param rng Ring task object */
    void SetRing(Char_t i, FMDRing* rng);
    /** Set detector task for detector with # @a i 
	@param i Detector #
	@param det Detector task object */
    void SetDetector(Int_t i, FMDDetector* det);
    /** The default setup */
    void DefaultSetup();
    /** @param b Browser */
    void Browse(TBrowser* b);
    /** @return always true */
    Bool_t IsFolder() const { return kTRUE; }
    /** Map (detector,ring,sector,strip) detector coordinates to 
	@f$(x,y,z)@f$ cartisian coordinates. 	
	@param detector Detector #
	@param ring     Ring ID
	@param sector   Sector #
	@param strip    Strip #
	@param x        Cartisian X coordinate
	@param y        Cartisian Y coordinate 
	@param z        Cartisian Z coordinate */
    void Detector2XYZ(UShort_t detector, 
		      Char_t ring, UShort_t sector, UShort_t strip, 
		      Double_t& x, Double_t& y, Double_t& z) const;
     
  protected:
    /** Poiner to inner task */
    FMDRing* fInner;
    /** Poiner to outer task */
    FMDRing* fOuter;
    /** Poiner to FMD1 task */
    FMDDetector* fFMD1;
    /** Poiner to FMD2 task */
    FMDDetector* fFMD2;
    /** Poiner to FMD3 task */
    FMDDetector* fFMD3;
    /**  Whether to use vacuum instead of air for void volumes */ 
    Bool_t fUseVacuum;
    ClassDef(FMD,0); //
  };
}

#endif
//____________________________________________________________________
//
// EOF
//
