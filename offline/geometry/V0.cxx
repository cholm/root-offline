//____________________________________________________________________ 
//  
// $Id: V0.cxx,v 1.6 2007-09-28 14:50:40 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    geometry/V0.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementation file for Geometry::V0
*/
# include "geometry/V0.h"
# include "geometry/V0.h"
# include "geometry/Magnet.h"
# include <TGeoVolume.h>
# include <TGeoTube.h>
# include <TGeoMaterial.h>
# include <TGeoMedium.h>
# include <TGeoManager.h>
# include <TBrowser.h>
# include <TMath.h>

//====================================================================
ClassImp(Geometry::V0Detector);

//____________________________________________________________________
Geometry::V0Detector::V0Detector(Char_t id, Double_t z) 
  : Framework::Task(Form("V0%c", id), "V0 detector"),
    fId(id),
    fZ(z), 
    fNRing(4)
{
  Double_t r[] = { 4.5, 7.1, 11.7, 19.3, 32.0 };
  Int_t    n[] = { 8, 8, 16, 16 };
  fRadii.Set(fNRing + 1, r);
  fNSector.Set(fNRing, n);
  fDepth = 2;
}

//____________________________________________________________________
void
Geometry::V0Detector::SetRadius(Int_t i, Bool_t inner, Double_t r) 
{
  if (i < 1 || i > fRadii.fN) return;
  fRadii[i + (inner ? - 1 : 0)] = r;
}

//____________________________________________________________________
Double_t
Geometry::V0Detector::GetRadius(Int_t i, Bool_t inner) const
{
  if (i < 1 || i > fRadii.fN) return -1;
  return fRadii[i + (inner ? - 1 : 0)];
}

//____________________________________________________________________
void
Geometry::V0Detector::SetNSector(Int_t i, Int_t n) 
{
  if (i < 1 || i > fNSector.fN) return;
  fNSector[i - 1] = n;
}

//____________________________________________________________________
Int_t
Geometry::V0Detector::GetNSector(Int_t i) const
{
  if (i < 1 || i > fNSector.fN) return -1;
  return fNSector[i  - 1];
}

//____________________________________________________________________
void
Geometry::V0Detector::Register(Option_t* option) 
{
  TGeoMedium* medium = gGeoManager->GetMedium("V0 scint");
  TGeoMedium* air    = gGeoManager->GetMedium("V0 air");
  TGeoTube* motherShape = new TGeoTube(GetRadius(1, kTRUE), 
				       GetRadius(4, kFALSE), 
				       fDepth / 2);
  TGeoVolume* motherVolume = new TGeoVolume(GetName(), motherShape, air);
  motherVolume->VisibleDaughters(kTRUE);
  TGeoVolume* top          = gGeoManager->GetTopVolume();
  TGeoMatrix* matrix       = 0;
  Double_t z = fZ;
  if (fZ < 0) {
    z                      -= fDepth / 2;
    TGeoRotation* rot      = new TGeoRotation(Form("%s rotation",GetName()));
    rot->RotateY(180);
    matrix                 = new TGeoCombiTrans(Form("%s transform",GetName()),
						0, 0, z, rot);
  }
  else {
    z                      += fDepth / 2;
    matrix                 =new TGeoTranslation(Form("%s transform",GetName()),
						0, 0, z);
  }
  top->AddNode(motherVolume, Int_t(fId), matrix);
				   
  for (Int_t i = 1; i <= fNRing; i++) {
    TGeoTube* ringShape      = new TGeoTube(GetRadius(i,kTRUE), 
					    GetRadius(i,kFALSE), fDepth / 2);
    TGeoVolume* ringVolume   = new TGeoVolume(Form("%s%d", GetName(), i),
					      ringShape, medium);
    ringVolume->SetLineColor(kCyan);
    ringVolume->VisibleDaughters(kTRUE);
    TGeoVolume* moduleVolume = ringVolume->Divide(Form("V%c%dM",fId,i), 2, 
						  GetNSector(i), 0, 0, 0, "N");
    moduleVolume->SetLineColor(kCyan);
    moduleVolume->SetVisibility(kTRUE);
    motherVolume->AddNode(ringVolume, i, 0);
  }
}

//____________________________________________________________________
void 
Geometry::V0Detector::Detector2XYZ(UShort_t ring, UShort_t sector, 
			       Double_t& x, Double_t& y, Double_t& z) const
{
  if (ring > fNRing || ring < 1) {
    Error("Detector2XYZ", "Invalid ring number %d", ring);
    return;
  }
  Int_t nSector = GetNSector(ring);
  if (sector > nSector) {
    Error("Detector2XYZ", "invalid sector number %d", sector);
    return;
  }
  Double_t phi = Float_t(sector + .5) / nSector * 2 * TMath::Pi();
  Double_t r   = (GetRadius(ring, kTRUE) + GetRadius(ring, kFALSE)) / 2;
  x = r * TMath::Cos(phi);
  y = r * TMath::Sin(phi);
  z = fZ + TMath::Sign(fDepth / 2, fZ);
}

//====================================================================
ClassImp(Geometry::V0);

//____________________________________________________________________
Geometry::V0::V0() 
  : Framework::Task("V0", "Primary Vertex")
{
  fV0C = 0;
  fV0A = 0;
}

//____________________________________________________________________
void
Geometry::V0::Initialize(Option_t* option) 
{
  Debug(5, "Initialize", "Setting up materials");
  Framework::Task::Initialize(option);

  TGeoMixture* v0Scint = new TGeoMixture("V0 scint", 2, 1.032);
  v0Scint->DefineElement(0,  1.008, 1., 0.5);
  v0Scint->DefineElement(1, 12.000, 6., 0.5);
  v0Scint->SetTransparency('0');

  Geometry::Magnet* mag = Geometry::Magnet::Instance();
  Double_t mType = mag->GetType();
  Double_t mMax  = mag->GetMax();
  Double_t par[] = { 1., mType, mMax, 1., .001, 1., .001, .001, .001, 0, 0 };
  TGeoMedium* medium = new TGeoMedium("V0 scint", 1, v0Scint,  par);

  // Air
  TGeoMixture* v0Air = new TGeoMixture("V0 air", 4, .00120479);
  v0Air->DefineElement(0, 12.0107,  6., 0.000124);
  v0Air->DefineElement(1, 14.0067,  7., 0.755267);
  v0Air->DefineElement(2, 15.9994,  8., 0.231781);
  v0Air->DefineElement(3, 39.948,  18., 0.012827);
  v0Air->SetTransparency('0');
  par[3] = 1;
  par[4] = .001;
  par[6] = .001;
  par[7] = .001;
  TGeoMedium* air = new TGeoMedium("V0 air", 1, v0Air,  par);
}

//____________________________________________________________________
void
Geometry::V0::DefaultSetup() 
{
  SetDetector('A', new V0Detector('A', 360));
  SetDetector('C', new V0Detector('C', -90));
}

//____________________________________________________________________
Geometry::V0Detector*
Geometry::V0::GetDetector(Char_t id) const
{
  switch (id) {
  case 'c': case 'C': return fV0C;
  case 'a': case 'A': return fV0A;
  }
  return 0;
}

//____________________________________________________________________
void
Geometry::V0::SetDetector(Char_t id, Geometry::V0Detector* d)
{
  V0Detector* which = 0;
  switch (id) {
  case 'c': case 'C': which = fV0C; break;
  case 'a': case 'A': which = fV0A; break;
  }
  if (which) {
    Warning("SetDetector", "Detector %d already set, will remove that", id);
    if (fTasks) fTasks->Remove(which);
  }
  switch (id) {
  case 'c': case 'C': fV0C = d; break;
  case 'a': case 'A': fV0A = d; break;
  }
  Add(d);
}

//____________________________________________________________________
void 
Geometry::V0::Detector2XYZ(Char_t detector, UShort_t ring, UShort_t sector, 
			   Double_t& x, Double_t& y, Double_t& z) const
{
  V0Detector* d = GetDetector(detector);
  if (!d) return;
  d->Detector2XYZ(ring, sector, x, y, z);
}

//____________________________________________________________________
void 
Geometry::V0::Browse(TBrowser* b) 
{
  if (fV0A) b->Add(fV0A);
  if (fV0C) b->Add(fV0C);
  Framework::Task::Browse(b);
}


//____________________________________________________________________
//
// EOF
//
