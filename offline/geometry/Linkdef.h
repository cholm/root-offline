// -*- mode: c++ -*-
//____________________________________________________________________ 
//  
// $Id: Linkdef.h,v 1.4 2005-12-14 13:31:38 cholm Exp $ 
//
//  ROOT generic framework
//  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
// Author: Christian Holm Christensen <cholm@nbi.dk>
// Update: 2002-06-20 15:06:41+0200
// Copyright: 2002 (C) Christian Holm Christensen (LGPL)
//
/** @file    geometry/Linkdef.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:24:27 2004
    @brief   Link specification for CINT
*/

#ifndef __CINT__
#error Not for compilation
#endif

#pragma link off all functions;
#pragma link off all globals;
#pragma link off all classes;

#pragma link C++ namespace Geometry;
#pragma link C++ class     Geometry::Beam+;
#pragma link C++ class     Geometry::Cave+;
#pragma link C++ class     Geometry::FMD+;
#pragma link C++ class     Geometry::FMDRing+;
#pragma link C++ class     Geometry::FMDDetector+;
#pragma link C++ class     Geometry::FMD1+;
#pragma link C++ class     Geometry::FMD2+;
#pragma link C++ class     Geometry::FMD3+;
#pragma link C++ class     Geometry::Magnet+;
#pragma link C++ class     Geometry::TOF+;
#pragma link C++ class     Geometry::TPC+;
#pragma link C++ class     Geometry::V0+;
#pragma link C++ class     Geometry::V0Detector+;
#pragma link C++ class     Geometry::GeometryWriter+;
#pragma link C++ class     Geometry::GeometryReader+;


//____________________________________________________________________ 
//  
// EOF
//
