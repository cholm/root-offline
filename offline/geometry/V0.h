// -*- mode: C++ -*- 
//____________________________________________________________________ 
//  
// $Id: V0.h,v 1.5 2006-11-08 01:41:44 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    geometry/V0.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Geometry::V0
*/
#ifndef Geometry_V0
#define Geometry_V0
#ifndef Framework_Task
# include <framework/Task.h>
#endif
#ifndef ROOT_TArrayD
# include <TArrayD.h>
#endif
#ifndef ROOT_TArrayI
# include <TArrayI.h>
#endif
class TGeoMedium;

namespace Geometry
{
  //__________________________________________________________________
  /** @class V0Detector geometry/V0.h <geometry/V0.h>
      @brief Geometry of V0 rings 
      @ingroup geometry v0
      @image html V0A.png 
      @image html V0C.png 
   */
  class V0Detector : public Framework::Task 
  {
  protected:
    /** Ring ID  */
    Char_t fId;
    /** Z position */
    Double_t fZ;
    /** Number of rings in this side */
    Int_t fNRing;
    /** Radii of the 4 rings */
    TArrayD fRadii;
    /** Number of sectors in the four rings */
    TArrayI fNSector;
    /** Thickness of the scintilator material */
    Double_t fDepth;
  public:
    /** Default CTOR - do not use   */
    V0Detector();
    /** Construct a ring 
	@param id ID 
	@param z  position along beam axis  */
    V0Detector(Char_t id, Double_t z);
    /** DTOR */
    virtual ~V0Detector() {}
    /** Register */
    void Register(Option_t* option="");
    /** @param option unused */
    virtual void Exec(Option_t* option="") {}

    /** Set Id 
	@param x Ring ID  */
    void SetId(Char_t x) { fId = x; } //*MENU*
    /** Set X position 
	@param x Z position */
    void SetZ(Double_t x) { fZ = x; } //*MENU*
    /** Set radii 
	@param x Radii of the 4 rings */
    void SetRadii(const TArrayD& x) { fRadii = x; } //*MENU*
    /** Set the radius
	@param i Ring number 
	@param inner Set the inner radius if true, otherwise the outer 
	@param r the radius value */
    void SetRadius(Int_t i, Bool_t inner, Double_t r); //*MENU*
    /** Set Number of sectors 
	@param x # of sectors of the 4 rings */
    void SetNSectors(const TArrayI& x) { fNSector = x; } //*MENU*
    /** Set Number of setcors 
	@param i Ring number 
	@param n Number of sectors in rin @a i */
    void SetNSector(Int_t i, Int_t n); //*MENU*
    /** Set Depth 
	@param x Thickness of the scintilator material */
    void SetDepth(Double_t x) { fDepth = x; } //*MENU*

    /** @return Ring ID  */
    Char_t GetId() const { return fId; }
    /** @return Z position */
    Double_t GetZ() const { return fZ; }
    /** @return number of rings */
    Int_t GetNRing() const { return fNRing; }
    /** @return Radii of the 4 rings */
    const TArrayD& GetRadii() const { return fRadii; }
    /** Get the radius
	@param i Ring number 
	@param inner Get the inner radius if true, otherwise the outer 
	@return Radius of the @a i rings */
    Double_t GetRadius(Int_t i, Bool_t inner=kTRUE) const;
    /** @return # of sectors in the 4 rings */
    const TArrayI& GetNSectors() const { return fNSector; }
    /** @param i Ring number 
	@return Number of sectors in ring number @a i */
    Int_t GetNSector(Int_t i) const;
    /** @return Thickness of the scintilator material */
    Double_t GetDepth() const { return fDepth; }

    void Detector2XYZ(UShort_t ring, UShort_t sector, 
		      Double_t& x, Double_t& y, Double_t& z) const;
    
    ClassDef(V0Detector,1);
  };


  //==================================================================
  /** @class V0 geometry/V0.h <geometry/V0.h>
      @brief V0 geometry 
      @ingroup geometry v0
      @image html V0A.png 
      @image html V0C.png 
   */
  class V0 : public Framework::Task 
  {
  protected:
    /** C counter */
    V0Detector* fV0C;
    /** A counter */
    V0Detector* fV0A;
  public:
    /** CTOR */
    V0();
    /** Initialize */
    void Initialize(Option_t* option="");
    /** Register */
    void Register(Option_t* option="") {}
    /** @param option unused */
    virtual void Exec(Option_t* option="") {}
    /** The  default setup */
    void DefaultSetup(); // *MENU*
    /** @param id Ring to get 
	@return Ring label @a id */
    V0Detector* GetDetector(Char_t id) const;
    /** Set detector 
	@param id Ring to set 
	@param det object to use for detector @a id */
    void SetDetector(Char_t id, V0Detector* det);
    /** Browse this 
	@param b Browser */
    void Browse(TBrowser* b);
    /** @return true */
    Bool_t IsFolder() const { return kTRUE; }
    void Detector2XYZ(Char_t detector, UShort_t ring, UShort_t sector, 
		      Double_t& x, Double_t& y, Double_t& z) const;
    ClassDef(V0,0) //
  };
}

#endif
//____________________________________________________________________
//
// EOF
//
