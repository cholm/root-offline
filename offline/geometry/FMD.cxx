//____________________________________________________________________ 
//  
// $Id: FMD.cxx,v 1.11 2007-09-28 14:50:40 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    geometry/FMD.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementation file for Geometry::FMD
*/
#include "geometry/FMD.h"
#include "geometry/Magnet.h"
#include <TGeoVolume.h>
#include <TGeoTube.h>
#include <TGeoTrd1.h>
#include <TGeoPcon.h>
#include <TGeoMaterial.h>
#include <TGeoMedium.h>
#include <TGeoXtru.h>
#include <TGeoPolygon.h>
#include <TGeoTube.h>
#include <TGeoManager.h>
#include <TVector2.h>
#include <TBrowser.h>
#include <TMath.h>
#include <iostream>

//====================================================================
ClassImp(Geometry::FMDRing);

//____________________________________________________________________
const char* Geometry::FMDRing::fgkSensorName      = "F%cSE";
const char* Geometry::FMDRing::fgkActiveName      = "F%cAC";
const char* Geometry::FMDRing::fgkSectorName      = "F%cSC";
const char* Geometry::FMDRing::fgkStripName       = "F%cST";
const char* Geometry::FMDRing::fgkPCBName         = "F%cPB";
const char* Geometry::FMDRing::fgkCuName          = "F%cCU";
const char* Geometry::FMDRing::fgkChipName        = "F%cCH";
const char* Geometry::FMDRing::fgkLongLegName     = "F%cLL";
const char* Geometry::FMDRing::fgkShortLegName    = "F%cSL";
const char* Geometry::FMDRing::fgkFrontVName      = "F%cFH";
const char* Geometry::FMDRing::fgkBackVName       = "F%cBH";
const char* Geometry::FMDRing::fgkRingTopName     = "F%cTV";
const char* Geometry::FMDRing::fgkRingBotName     = "F%cBV";


//____________________________________________________________________
Geometry::FMDRing::FMDRing(Char_t id) 
  : Framework::Task(Form("FMD%c", id), "Forward multiplicity ring"), 
    fId(id), 
    fVerticies(0),
    fUseAssembly(kFALSE)
{
  SetBondingWidth();
  SetWaferRadius();
  SetSiThickness();
  SetLegRadius();
  SetLegLength();
  SetLegOffset();
  SetModuleSpacing();
  SetPrintboardThickness();
  
  if (fId == 'I' || fId == 'i') {
    SetLowR(4.3);
    SetHighR(17.2);
    SetTheta(36/2);
    SetNStrips(512);
  }
  else if (fId == 'O' || fId == 'o') {
    SetLowR(15.6);
    SetHighR(28.0);
    SetTheta(18/2);
    SetNStrips(256);
  }
  SetSpacerHeight(.03);
}

//____________________________________________________________________
void
Geometry::FMDRing::Register(Option_t* option)
{
  const Char_t* name           = (fId == 'I' ? "Inner" : "Outer");
  if (gGeoManager && gGeoManager->GetVolume(Form("FMD%sTop",name))) {
    Info("Register", "Volume %s already defined", GetName());
    return;
  }
  Double_t tanTheta  = TMath::Tan(fTheta * TMath::Pi() / 180.);
  Double_t tanTheta2 = TMath::Power(tanTheta,2);
  Double_t r2        = TMath::Power(fWaferRadius,2);
  Double_t yA        = tanTheta * fLowR;
  Double_t lr2       = TMath::Power(fLowR, 2);
  Double_t hr2       = TMath::Power(fHighR,2);
  Double_t xD        = fLowR + TMath::Sqrt(r2 - tanTheta2 * lr2);
  Double_t xD2       = TMath::Power(xD,2);
  Double_t yB        = TMath::Sqrt(r2 - hr2 + 2 * fHighR * xD - xD2);
  Double_t xC        = ((xD + TMath::Sqrt(-tanTheta2 * xD2 + r2
					  + r2 * tanTheta2)) 
			/ (1 + tanTheta2));
  Double_t yC        = tanTheta * xC;
  
  fVerticies.Expand(6);
  TVector2* a = new TVector2(fLowR,   yA);
  TVector2* b = new TVector2(fHighR,  yB);
  TVector2* c = new TVector2(xC,      yC);
  
  fVerticies.AddAt(new TVector2(fLowR,  -yA), 0);
  fVerticies.AddAt(new TVector2(xC,     -yC), 1);
  fVerticies.AddAt(new TVector2(fHighR, -yB), 2);
  fVerticies.AddAt(b, 3);
  fVerticies.AddAt(c, 4);
  fVerticies.AddAt(a, 5);  

  fRingDepth = (fSiThickness + fPrintboardThickness 
		+ fLegLength + fModuleSpacing 
		+ fSpacerHeight);


  Debug(5,"Init", "-------------------------------------");
  Debug(5,"Init", "Ring %c", fId);
  Debug(5,"Init", " BondingWidth\t: %8.4f",      GetBondingWidth());
  Debug(5,"Init", " WaferRadius\t\t: %8.4f",       GetWaferRadius());
  Debug(5,"Init", " SiThickness\t\t: %8.4f",       GetSiThickness());
  Debug(5,"Init", " LowR\t\t: %8.4f",              GetLowR());
  Debug(5,"Init", " HighR\t\t: %8.4f",             GetHighR());
  Debug(5,"Init", " Theta\t\t: %8.4f",             GetTheta());
  Debug(5,"Init", " NStrips\t\t: %8d",              GetNStrips());
  Debug(5,"Init", " RingDepth\t\t: %8.4f",         GetRingDepth());
  Debug(5,"Init", " LegRadius\t\t: %8.4f",         GetLegRadius());
  Debug(5,"Init", " LegLength\t\t: %8.4f",         GetLegLength());
  Debug(5,"Init", " LegOffset\t\t: %8.4f",         GetLegOffset());
  Debug(5,"Init", " ModuleSpacing\t: %8.4f",       GetModuleSpacing());
  Debug(5,"Init", " PrintboardThickness\t: %8.4f", GetPrintboardThickness());
  Debug(5,"Init", " SpacerHeight\t: %8.4f",      GetSpacerHeight());
  for (Int_t i = 0; i < 6; i++) {
    Debug(5,"Init", " Vertex # %d\t\t: %8.4f, %8.4f", 
	 i, GetVertex(i)->X(), GetVertex(i)->Y());
  }

  Double_t      off            = (TMath::Tan(TMath::Pi()*fTheta/180)
				  *fBondingWidth);
  Double_t      rmax           = b->Mod();
  Double_t      stripoff       = a->Mod();
  Double_t      dstrip         = (rmax - stripoff) / fNStrips;
  Double_t      backContThick  = (fSiThickness + fPrintboardThickness 
				  + fLegLength + fSpacerHeight);
  Double_t      fRingDepth     = backContThick + fModuleSpacing;
  const Int_t   nv             = fVerticies.GetEntries();
  Double_t      xs[nv];
  Double_t      ys[nv];
  for (Int_t i = 0; i < nv; i++) {
    // Reverse the order 
    TVector2* vv = GetVertex(nv - 1 - i);
    xs[i]        = vv->X();
    ys[i]        = vv->Y();
  }
  TGeoMedium* voidM          = gGeoManager->GetMedium("FMD Air");
  TGeoMedium* si             = gGeoManager->GetMedium("FMD Si");
  TGeoMedium* pcb            = gGeoManager->GetMedium("FMD PCB");
  TGeoMedium* plastic        = gGeoManager->GetMedium("FMD Plastic");

  // Back container volume 

  // Shape of actual sensor 
  TGeoXtru* sensorShape      = new TGeoXtru(2);
  sensorShape->DefinePolygon(nv, xs, ys);
  sensorShape->DefineSection(0, - fSiThickness/2);
  sensorShape->DefineSection(1, fSiThickness/2);
  TGeoVolume* sensorVolume   = 
    new TGeoVolume(Form(fgkSensorName, fId),sensorShape,si);
  sensorVolume->VisibleDaughters(kFALSE);
  sensorVolume->SetVisibility(kTRUE);
  sensorVolume->SetLineColor(2);
  sensorVolume->SetFillColor(2);
    
  // Virtual volume shape to divide 
  TGeoTubeSeg* activeShape   = 
    new TGeoTubeSeg(fLowR, rmax, fSiThickness/2, - fTheta, fTheta);
  TGeoVolume*  activeVolume  = 
    new TGeoVolume(Form(fgkActiveName, fId), activeShape,si);
  sensorVolume->AddNodeOverlap(activeVolume, 0);
  TGeoVolume* sectorVolume   = 
    activeVolume->Divide(Form(fgkSectorName, fId),2,2,-fTheta,0,0,"N");
  TGeoVolume* stripVolume    = 
    sectorVolume->Divide(Form(fgkStripName, fId),
			 1,fNStrips,stripoff,dstrip,0,"SX");
  
  // PCB 
  TGeoXtru* pcbShape         = new TGeoXtru(2);
  for (Int_t i = 0;      i < nv / 2; i++) ys[i] -= off;
  for (Int_t i = nv / 2; i < nv;     i++) ys[i] += off;
  pcbShape->DefinePolygon(nv, xs, ys);
  pcbShape->DefineSection(0, -fPrintboardThickness / 2);
  pcbShape->DefineSection(1,  fPrintboardThickness / 2);
  TGeoVolume* pcbVolume      = 
    new TGeoVolume(Form(fgkPCBName, fId), pcbShape, pcb);
  pcbVolume->SetLineColor(4);
  pcbVolume->SetFillColor(4);
  
  // Short leg shape 
  TGeoTube* shortLegShape    = new TGeoTube(0, fLegRadius, fLegLength / 2);
  TGeoVolume* shortLegVolume = 
    new TGeoVolume(Form(fgkShortLegName, fId), shortLegShape, plastic);
  // Long leg shape
  TGeoTube* longLegShape     = 
    new TGeoTube(0, fLegRadius, (fLegLength + fModuleSpacing) / 2);
  TGeoVolume* longLegVolume  = 
    new TGeoVolume(Form(fgkLongLegName,fId), longLegShape, plastic);
  longLegVolume->SetLineColor(3);
  longLegVolume->SetFillColor(3);
  shortLegVolume->SetLineColor(3);
  shortLegVolume->SetFillColor(3);

  // Assembly of PCB and legs for front 
  TGeoVolume* frontVolume = new TGeoVolumeAssembly(Form(fgkFrontVName,fId));
  TGeoVolume* backVolume  = new TGeoVolumeAssembly(Form(fgkBackVName,fId));
  Double_t zf = fPrintboardThickness/2;
  Double_t zb = fPrintboardThickness/2;
  TString rn;
  frontVolume->AddNode(pcbVolume, 0, new TGeoTranslation(0,0,zf));
  backVolume->AddNode(pcbVolume, 0, new TGeoTranslation(0,0,zb));

  // Place Leg 0
  TVector2 v(a->X() + fLegOffset + fLegRadius,0);
  zb                       = fPrintboardThickness + fLegLength / 2;
  zf                       = zb + fModuleSpacing / 2;
  rn                       = Form("FMD Ring %s long leg 0 transform", name);
  TGeoTranslation* ftrans0 = new TGeoTranslation(rn.Data(),v.X(),v.Y(),zf);
  rn                       = Form("FMD Ring %s short leg 0 transform", name);
  TGeoTranslation* btrans0 = new TGeoTranslation(rn.Data(),v.X(),v.Y(),zb);
  frontVolume->AddNode(longLegVolume,0,ftrans0);
  backVolume->AddNode(shortLegVolume,0,btrans0);

  // Place leg 1
  v.Set(c->X(), c->Y()-fLegOffset-fLegRadius-off);
  rn                       = Form("FMD Ring %s long leg 1 transform", name);
  TGeoTranslation* ftrans1 = new TGeoTranslation(rn.Data(),v.X(),v.Y(),zf);
  rn                       = Form("FMD Ring %s short leg 1 transform", name);
  TGeoTranslation* btrans1 = new TGeoTranslation(rn.Data(),v.X(),v.Y(),zb);
  frontVolume->AddNode(longLegVolume,1,ftrans1);
  backVolume->AddNode(shortLegVolume,1,btrans1);

  // Place  leg 2
  v.Set(c->X(), - c->Y()+fLegOffset+fLegRadius+off);
  rn                       = Form("FMD Ring %s long leg 2 transform", name);
  TGeoTranslation* ftrans2 = new TGeoTranslation(rn.Data(),v.X(),v.Y(),zf);
  rn                       = Form("FMD Ring %s short leg 2 transform", name);
  TGeoTranslation* btrans2 = new TGeoTranslation(rn.Data(),v.X(),v.Y(),zb);
  frontVolume->AddNode(longLegVolume,2,ftrans2);
  backVolume->AddNode(shortLegVolume,2,btrans2);

  // Ring mother volume 
  fTopVolume    = new TGeoVolumeAssembly(Form(fgkRingTopName, fId));
  fBottomVolume = new TGeoVolumeAssembly(Form(fgkRingBotName, fId));

  Int_t       nmod = Int_t(360. / fTheta) / 2;
  TGeoVolume* half = fTopVolume;
  Debug(15, "Register","making %d modules in ring %s", nmod, name);
  for (Int_t i = 0; i < nmod; i++) {
    if (i >= nmod / 2) half  = fBottomVolume;
    Bool_t   isFront         = (i % 2 == 0);
    Double_t z               = fSiThickness / 2 + (i % 2) * fModuleSpacing;
    Double_t ang             = (i + .5) * 2 * fTheta;

    // Place module 
    rn = Form("FMD Ring %s mod transform %d", name, i);
    TGeoMatrix* trans        = new TGeoCombiTrans(rn.Data(),0,0,z,0);
    trans->RotateZ(ang);
    half->AddNode(sensorVolume,i,trans);
    
    // Place PCB 
    z                        += fSpacerHeight + fSiThickness / 2;
    rn                       =  Form("FMD Ring %s hybrid transform %d",name,i);
    trans                    = new TGeoCombiTrans(rn.Data(),0,0,z,0);
    trans->RotateZ(ang);
    TGeoVolume* hybrid       =  (isFront ? frontVolume : backVolume);
    half->AddNode(hybrid,i,trans);
  }
}

//____________________________________________________________________
TVector2*
Geometry::FMDRing::GetVertex(Int_t i) const
{
  return static_cast<TVector2*>(fVerticies.At(i));
}

//____________________________________________________________________
void
Geometry::FMDRing::Detector2XYZ(UShort_t sector,
				UShort_t strip, 
				Double_t& x, 
				Double_t& y, 
				Double_t& z) const
{
  if (sector >= GetNSectors()) {
    Error("Detector2XYZ", "Invalid sector number %d (>=%d) in ring %c", 
	  sector, GetNSectors(), fId);
    return;
  }
  if (strip >= GetNStrips()) {
    Error("Detector2XYZ", "Invalid strip number %d (>=%d)", 
	  strip, GetNStrips(), fId);
    return;
  }
  Double_t phi = Float_t(sector + .5) / GetNSectors() * 2 * TMath::Pi();
  Double_t r   = Float_t(strip + .5) / GetNStrips() * (fHighR - fLowR) + fLowR;
  x = r * TMath::Cos(phi);
  y = r * TMath::Sin(phi);
  if (((sector / 2) % 2) == 1) z += TMath::Sign(fModuleSpacing, z);
}

//====================================================================
ClassImp(Geometry::FMDDetector);

//____________________________________________________________________
const char* Geometry::FMDDetector::fgkHCName          = "F%dH%c";
const char* Geometry::FMDDetector::fgkIHCName         = "F%dI%c";
const char* Geometry::FMDDetector::fgkFMDName         = "F%dM%c";

//____________________________________________________________________
Geometry::FMDDetector::FMDDetector(Int_t id, FMDRing* inner, FMDRing* outer) 
  : Framework::Task(Form("FMD%d", id), "Forward multiplicity ring"), 
    fId(id), 
    fInner(inner),
    fOuter(outer), 
    fUseAssembly(kFALSE),
    fZ(0)
{
  SetHoneycombThickness();
  SetAlThickness();
  SetInnerHoneyLowR(0);
  SetInnerHoneyHighR(0);
  SetInnerZ(0);
  SetOuterZ(0);
  SetOuterHoneyLowR(0);
  SetOuterHoneyHighR(0);
}

//____________________________________________________________________
void
Geometry::FMDDetector::Initialize(Option_t* option)
{
  // Initialize.
  Framework::Task::Initialize(option);
  if (fInner) {
    SetInnerHoneyLowR(fInner->GetLowR() + 1.);
    SetInnerHoneyHighR(fInner->GetHighR() + 1.);
  }
  if (fOuter) {
    SetOuterHoneyLowR(fOuter->GetLowR() + 1.);
    SetOuterHoneyHighR(fOuter->GetHighR() + 1.);
  }
}


//____________________________________________________________________
void
Geometry::FMDDetector::Register(Option_t* option)
{
  if (gGeoManager && gGeoManager->GetVolume(GetName())) {
    Info("Register", "Volume %s already defined", GetName());
    return;
  }
  Verbose(1, "Register", "Now registering rings in %s", GetName());
  Debug(5,"Init", "-------------------------------------");
  Debug(5,"Init", "Detector %d", fId);
  Debug(5,"Init", " InnerZ\t\t\t: %8.4f",         GetInnerZ());
  Debug(5,"Init", " OuterZ\t\t\t: %8.4f",         GetOuterZ());
  Debug(5,"Init", " HoneycombThickness\t: %8.4f", GetHoneycombThickness());
  Debug(5,"Init", " AlThickness\t\t: %8.4f",      GetAlThickness());
  Debug(5,"Init", " InnerHoneyLowR\t\t: %8.4f",   GetInnerHoneyLowR());
  Debug(5,"Init", " InnerHoneyHighR\t: %8.4f",    GetInnerHoneyHighR());
  Debug(5,"Init", " OuterHoneyLowR\t\t: %8.4f",   GetOuterHoneyLowR());
  Debug(5,"Init", " OuterHoneyHighR\t: %8.4f",    GetOuterHoneyHighR());

  fTopVolume          = new TGeoVolumeAssembly(Form(fgkFMDName,fId,'T'));
  fBottomVolume       = new TGeoVolumeAssembly(Form(fgkFMDName,fId,'B'));

  TGeoMedium* voidM   = gGeoManager->GetMedium("FMD Air");
  TGeoMedium* si      = gGeoManager->GetMedium("FMD Si");
  TGeoMedium* pcb     = gGeoManager->GetMedium("FMD PCB");
  TGeoMedium* plastic = gGeoManager->GetMedium("FMD Plastic");
  TGeoMedium* al      = gGeoManager->GetMedium("FMD Al");
  TGeoMedium* kapton  = gGeoManager->GetMedium("FMD Kapton");
  FMDRing* r = 0;
  for (int i = 0; i < 2; i++) {
    Double_t lowr;
    Double_t highr;
    Double_t rz;
    switch (i) {
    case 0: 
      r      = fInner;
      lowr   = fInnerHoneyLowR;
      highr  = fInnerHoneyHighR;
      rz     = fInnerZ;
      break;
    case 1: 
      r      = fOuter;
      lowr   = fOuterHoneyLowR;
      highr  = fOuterHoneyHighR;
      rz     = fOuterZ;
      break;
    }
    if (!r) continue;
    TGeoVolume*   rtvol = r->GetTopVolume();
    TGeoVolume*   rbvol = r->GetBottomVolume();
    Char_t        c     = r->GetId();
    Int_t         id    = fId;
    const Char_t* cn    = (c == 'I' ? "Inner" : "Outer");
    // Half of Honeycomb
    TGeoTubeSeg* hcShape  = new TGeoTubeSeg(lowr,highr,fHoneycombThickness/2, 
					    0, 180);
    TGeoVolume*  hcVolume = new TGeoVolume(Form(fgkHCName,fId,c),hcShape,al);
    hcVolume->SetLineColor(5);
    hcVolume->SetFillColor(5);
    hcVolume->VisibleDaughters(kFALSE);
    // Air in honeycomb
    TGeoTubeSeg* airHcShape  = new TGeoTubeSeg(lowr+fAlThickness, 
					       highr-fAlThickness, 
					       (fHoneycombThickness 
						- fAlThickness)/2, 0, 180);
    TGeoVolume*  airHcVolume = new TGeoVolume(Form(fgkIHCName, fId, c), 
					      airHcShape, kapton);
    hcVolume->AddNode(airHcVolume, 0);
    
    // Place half-ring volumes
    Double_t z = TMath::Abs(rz - fZ);
    TGeoTranslation* t4 = new TGeoTranslation(Form("FMD%d%s transform", 
						   fId, cn), 0, 0, z);
    fTopVolume->AddNode(rtvol, Int_t(c), t4);
    fBottomVolume->AddNode(rbvol, Int_t(c), t4);

    // Place Top Honeycomb in mother volume 
    z  += r->GetRingDepth() + fHoneycombThickness / 2;
    TGeoTranslation* t1 = 
      new TGeoTranslation(Form("FMD%d%s top plate %d transform",
			       fId, cn, i), 0, 0, z);
    fTopVolume->AddNode(hcVolume, 0, t1);
    TGeoCombiTrans* t2 = 
      new TGeoCombiTrans(Form("FMD%d%s bottom plate %d transform",
			      fId, cn, i), 0, 0, z, 0); 
    t2->RotateZ(180);
    fBottomVolume->AddNode(hcVolume, 1, t2);

  }
}

//____________________________________________________________________
Geometry::FMDRing*
Geometry::FMDDetector::GetRing(Char_t id) const
{
  switch (id) {
  case 'i': case 'I': return GetInner();
  case 'o': case 'O': return GetOuter(); 
  }
  return 0;
}

//____________________________________________________________________
Double_t
Geometry::FMDDetector::GetRingZ(Char_t id) const
{
  switch (id) {
  case 'i': case 'I': return GetInnerZ();
  case 'o': case 'O': return GetOuterZ();
  }
  return 0;
}
//____________________________________________________________________
void
Geometry::FMDDetector::Detector2XYZ(Char_t ring, 
				    UShort_t sector,
				    UShort_t strip, 
				    Double_t& x, 
				    Double_t& y, 
				    Double_t& z) const
{
  FMDRing* r = GetRing(ring);
  if (!r) return;
  z = GetRingZ(ring);
  r->Detector2XYZ(sector, strip, x, y, z);
}

//====================================================================
ClassImp(Geometry::FMD1);

//____________________________________________________________________
Geometry::FMD1::FMD1(FMDRing* inner) 
  : FMDDetector(1, inner, 0)
{
  SetInnerZ(340);
}

//____________________________________________________________________
void
Geometry::FMD1::Register(Option_t* option) 
{
  Verbose(1, "Register", "Registering FMD1");
  if (gGeoManager && gGeoManager->GetVolume(GetName())) {
    Info("Register", "Volume %s already defined", GetName());
    return;
  }
  Double_t rmin     = fInner->GetLowR();
  Double_t rmax     = fInnerHoneyHighR;
  Double_t w        = fInner->GetRingDepth() + fHoneycombThickness;
  // fZ             = fInnerZ + w / 2;
  fZ                = fInnerZ;
  
  
  FMDDetector::Register(option);

  // fZ             = fInnerZ + w / 2;
  TGeoVolume* top   = gGeoManager->GetTopVolume();
  top->AddNode(fTopVolume, fId, new TGeoTranslation("FMD1 transform",0,0,fZ));
  top->AddNode(fBottomVolume, fId, 
	       new TGeoTranslation("FMD1 transform",0,0,fZ));
}

//====================================================================
ClassImp(Geometry::FMD2);

//____________________________________________________________________
Geometry::FMD2::FMD2(FMDRing* inner, FMDRing* outer) 
  : FMDDetector(2, inner, outer)
{
  SetInnerZ(83.4);
  SetOuterZ(75.2);
}


//____________________________________________________________________
void
Geometry::FMD2::Register(Option_t* option) 
{
  Verbose(1, "Register", "Registering FMD2");
  if (gGeoManager && gGeoManager->GetVolume(GetName())) {
    Info("Register", "Volume %s already defined", GetName());
    return;
  }
  fInnerHoneyHighR  = fOuterHoneyHighR;
  Double_t ow       = fInner->GetRingDepth();
  Double_t iz       = fInnerZ;
  Double_t oz       = fOuterZ;
  Double_t w        = TMath::Abs(oz - iz) + ow + fHoneycombThickness;
  // fZ                = oz + w / 2;
  fZ                = fOuterZ;
  
  Geometry::FMDDetector::Register(option);

  TGeoVolume* top = gGeoManager->GetTopVolume();
  top->AddNode(fTopVolume, fId, new TGeoTranslation("FMD2 transform",0,0,fZ));
  top->AddNode(fBottomVolume, fId, 
	       new TGeoTranslation("FMD2 transform",0,0,fZ));
}

//====================================================================
ClassImp(Geometry::FMD3);

//____________________________________________________________________
const char* Geometry::FMD3::fgkNoseName        = "F3SN";
const char* Geometry::FMD3::fgkBackName        = "F3SB";
const char* Geometry::FMD3::fgkBeamName        = "F3SL";
const char* Geometry::FMD3::fgkFlangeName      = "F3SF";

//____________________________________________________________________
Geometry::FMD3::FMD3(FMDRing* inner, FMDRing* outer) 
  : FMDDetector(3, inner, outer)
{
  SetInnerZ(-62.8);
  SetOuterZ(-75.2);
  SetNoseZ();
  SetNoseLowR();
  SetNoseHighR();
  SetNoseLength();
  SetBackLowR();
  SetBackHighR();
  SetBackLength();
  SetBeamThickness();
  SetBeamWidth();
  SetConeLength();
  SetFlangeR();
  SetNBeam();
  SetNFlange();
}

//____________________________________________________________________
void
Geometry::FMD3::Register(Option_t* option) 
{
  Verbose(1, "Register", "Registering FMD3");
  if (gGeoManager && gGeoManager->GetVolume(GetName())) {
    Info("Register", "Volume %s already defined", GetName());
    return;
  }
  fZ = fNoseZ;
  Geometry::FMDDetector::Register(option);

  SetInnerHoneyHighR(GetOuterHoneyHighR());
  Double_t zdist   = fConeLength - fBackLength - fNoseLength;
  Double_t tdist   = fBackHighR - fNoseHighR;
  Double_t innerZh = fInnerZ - fInner->GetRingDepth() - fHoneycombThickness;
  Double_t outerZh = fOuterZ - fOuter->GetRingDepth() - fHoneycombThickness;
  Double_t minZ    = TMath::Min(fNoseZ - fConeLength, outerZh);
  fAlpha           = tdist / zdist;
  fInnerHoneyHighR = ConeR(innerZh + fHoneycombThickness,"O") - 1;
  fOuterHoneyHighR = GetBackLowR();

  Double_t beaml   = TMath::Sqrt(zdist * zdist + tdist * tdist);
  Double_t theta   = -180. * TMath::ATan2(tdist, zdist) / TMath::Pi();
  Double_t innerr1 = fInner->GetLowR();
  Double_t innerr2 = fInner->GetHighR();
  Double_t outerr1 = fOuter->GetLowR();
  Double_t outerr2 = fOuter->GetHighR();
  Double_t zi, r1, r2;

  TGeoMedium* voidM          = gGeoManager->GetMedium("FMD Air");
  TGeoMedium* si             = gGeoManager->GetMedium("FMD Si");
  TGeoMedium* pcb            = gGeoManager->GetMedium("FMD PCB");
  TGeoMedium* plastic        = gGeoManager->GetMedium("FMD Plastic");
  TGeoMedium* al             = gGeoManager->GetMedium("FMD Al");
  TGeoMedium* kapton         = gGeoManager->GetMedium("FMD Kapton");
  TGeoMedium* carbon         = gGeoManager->GetMedium("FMD C");

  // Nose volume 
  TGeoTubeSeg* noseShape = new TGeoTubeSeg(fNoseLowR,fNoseHighR,fNoseLength/2,
					   0, 180);
  TGeoVolume* noseVolume = new TGeoVolume(fgkNoseName, noseShape, carbon);
  zi = fNoseLength / 2;
  Debug(15, "Register", "Adding nose at %f", zi);
  TGeoMatrix* matrix = new TGeoTranslation("FMD3 Nose translation",0,0,zi);
  fTopVolume->AddNode(noseVolume,0, matrix);
  matrix = new TGeoCombiTrans("FMD3 Nose translation",0,0,zi,0);
  matrix->RotateZ(180);
  fBottomVolume->AddNode(noseVolume,1, matrix);
  
  
  // Back
  TGeoTubeSeg* backShape = new TGeoTubeSeg(fBackLowR,fBackHighR,fBackLength/2,
					   0, 180);
  TGeoVolume* backVolume = new TGeoVolume(fgkBackName, backShape, carbon);
  zi = (fZ - fNoseZ) + fConeLength - fBackLength / 2;
  Debug(15, "Register", "Adding back at %f", zi);
  matrix = new TGeoTranslation("FMD3 Back translation", 0, 0, zi);
  fTopVolume->AddNode(backVolume, 0, matrix);
  matrix = new TGeoCombiTrans("FMD3 Back translation",0,0,zi,0);
  matrix->RotateZ(180);
  fBottomVolume->AddNode(backVolume,1, matrix);
  
  // The flanges 
  TGeoBBox* flangeShape = new TGeoBBox((fFlangeR - fBackHighR) / 2, 
				       fBeamWidth / 2, fBackLength / 2);
  TGeoVolume* flangeVolume = new TGeoVolume(fgkFlangeName,flangeShape,carbon);
  Double_t r = fBackHighR + (fFlangeR - fBackHighR) / 2;
  TGeoVolume* mother = fTopVolume;
  for (Int_t i = 0; i  < fNFlange; i++) {
    if (i >= fNFlange / 2) mother = fBottomVolume;
    Double_t phi                  = 360. / fNFlange * i + 180. / fNFlange;
    Double_t x                    = r * TMath::Cos(TMath::Pi() / 180 * phi);
    Double_t y                    = r * TMath::Sin(TMath::Pi() / 180 * phi);
    TGeoRotation* rot             = new TGeoRotation(Form("FMD3 flange %d",i));
    rot->RotateZ(phi);
    matrix = new TGeoCombiTrans(Form("FMD3 flange transform %d",i),x,y,zi,rot);
    Debug(15, "Register", "Putting flange %d at (x,y,z)=(%f,%f,%f) %f in %s", 
	  i, x, y, zi, phi, mother->GetName());
    mother->AddNode(flangeVolume, i, matrix);
  }

  // The Beams 
  TGeoBBox* beamShape = new TGeoBBox(fBeamThickness/2,fBeamWidth/2-.1,beaml/2);
  TGeoVolume* beamVolume = new TGeoVolume(fgkBeamName, beamShape, carbon);
  r                      = fNoseHighR + tdist / 2;
  zi                     = (fZ - fNoseZ) + fNoseLength + zdist / 2;
  mother                 = fTopVolume;
  for (Int_t i = 0; i  < fNBeam; i++) {
    if (i >= fNBeam / 2) mother = fBottomVolume;
    Double_t phi                = 360. / fNBeam * i;
    Double_t x                  = r * TMath::Cos(TMath::Pi() / 180 * phi);
    Double_t y                  = r * TMath::Sin(TMath::Pi() / 180 * phi);
    TGeoRotation* rot           = new TGeoRotation(Form("FMD3 beam %d",i));
    rot->RotateY(-theta);
    rot->RotateZ(phi);
    matrix = new TGeoCombiTrans(Form("FMD3 beam transform %d", i),x,y,zi,rot);
    Debug(15, "Register", "Putting beam %d at z=%f in %s", 
	  i, zi, mother->GetName());
    mother->AddNode(beamVolume, i, matrix);    
  }

  TGeoVolume* top = gGeoManager->GetTopVolume();
  matrix          = new TGeoCombiTrans("FMD3 transform",0,0,-fZ, 0);
  matrix->RotateY(180);
  top->AddNode(fTopVolume, fId, matrix);
  top->AddNode(fBottomVolume, fId, matrix);
}

//____________________________________________________________________
Double_t
Geometry::FMD3::ConeR(Double_t z, Option_t* opt) const
{
  // Calculate the cone radius at Z
  if (fAlpha < 0) {
    Warning("ConeR", "alpha not set: %lf", fAlpha);
    return -1;
  }
  if (z > fNoseZ) {
    Warning("ConeR", "z=%lf is before start of cone %lf", z, fNoseZ);
    return -1;
  }
  if (z < fOuterZ - fOuter->GetRingDepth() - fHoneycombThickness) {
    Warning("ConeR", "z=%lf is after end of cone %lf", z, 
	    fOuterZ - fOuter->GetRingDepth() - fHoneycombThickness);
    return -1;
  }
  Double_t e = fBeamThickness / TMath::Cos(TMath::ATan(fAlpha));
  if (opt[0] == 'I' || opt[1] == 'i') e *= -1;
  if (z > fNoseZ - fNoseLength) return fNoseHighR + e;
  if (z < fNoseZ - fConeLength + fBackLength) return fBackHighR + e;
  Double_t r = fNoseHighR + fAlpha * TMath::Abs(z - fNoseZ + fNoseLength) + e;
  return r;
}

//====================================================================
ClassImp(Geometry::FMD);

//____________________________________________________________________
Geometry::FMD::FMD() 
  : Framework::Task("FMD", "Forward multiplicity"),
    fUseVacuum(kTRUE), 
    fInner(0),
    fOuter(0),
    fFMD1(0),
    fFMD2(0),
    fFMD3(0)
{}

//____________________________________________________________________
void
Geometry::FMD::Initialize(Option_t* option) 
{
  Debug(5, "Initialize", "Setting up materials");
  Framework::Task::Initialize(option);

  if (gGeoManager && gGeoManager->GetMedium("FMD air")) {
    Info("Initialize", "Mediums for the FMD already defined");
    return;
  }
  Geometry::Magnet* mag = Geometry::Magnet::Instance();
  Double_t mType = mag->GetType();
  Double_t mMax  = mag->GetMax();
  
  // Parameters are 
  //	0	isvol	Is active (1) or not (0)
  //	1	ifield	Type of magnetic field 
  //	2	fieldm	Maximum magnetic field
  //	3	tmaxfd	Maxium deflection due to magnetic field
  //	4	stemax	Maximum step value
  //	5	deemax	Maximum energy loss per step
  //	6	epsil	Precision
  //	7	stmin	Minimum step value 
  //    8       -----   Ignored 
  //    9       -----   Ignored

  // Air 
  Double_t pAir[]     = { 0., mType, mMax, 1.,  .001, 1., .001, .001 };
  TGeoMixture* fmdAir = new TGeoMixture("FMD air", 4, .00120479);
  fmdAir->DefineElement(0, 12.0107,  6., 0.000124);
  fmdAir->DefineElement(1, 14.0067,  7., 0.755267);
  fmdAir->DefineElement(2, 15.9994,  8., 0.231781);
  fmdAir->DefineElement(3, 39.948,  18., 0.012827);
  fmdAir->SetTransparency('0');
  // Vacumm 
  Double_t pVacuum[]  = { 0., mType, mMax, 10,  .01, .1, .003, .003 };
  TGeoMaterial* fmdVacuum = new TGeoMaterial("FMD Vacuum",1e-16,1e-16,1e-16);
  fmdVacuum->SetTransparency('0');
  new TGeoMedium("FMD Air", 1, (fUseVacuum ? fmdVacuum : fmdAir),  
		 (fUseVacuum ? pVacuum : pAir));

  // Plastic 
  Double_t pPlastic[] = { 0., mType, mMax, 10., .01,  1., .003, .003 };
  TGeoMixture* fmdPlastic = new TGeoMixture("FMD Plastic", 2, 1.03);
  fmdPlastic->DefineElement(0,  1.01,   1, .5);
  fmdPlastic->DefineElement(1,  12.01,  6, .5);
  fmdPlastic->SetFillColor(4);
  new TGeoMedium("FMD Plastic", 1, fmdPlastic,  pPlastic);

  // PCB 
  Double_t pPCB[]     = { 0., mType, mMax, 1.,  .001, 1., .001, .001 };
  TGeoMixture* fmdPCB = new TGeoMixture("FMD PCB", 14, 1.8);
  fmdPCB->DefineElement(0,  28.0855,   14, 0.15144894);
  fmdPCB->DefineElement(1,  40.078,    20, 0.08147477);
  fmdPCB->DefineElement(2,  26.981538, 13, 0.04128158);
  fmdPCB->DefineElement(3,  24.305,    12, 0.00904554);
  fmdPCB->DefineElement(4,  10.811,     5, 0.01397570);
  fmdPCB->DefineElement(5,  47.867,    22, 0.00287685);
  fmdPCB->DefineElement(6,  22.98977,  11, 0.00445114);
  fmdPCB->DefineElement(7,  39.0983,   19, 0.00498089);
  fmdPCB->DefineElement(8,  55.845,    26, 0.00209828);
  fmdPCB->DefineElement(9,  18.9984,    9, 0.00420000);
  fmdPCB->DefineElement(10, 15.9994,    8, 0.36043788);
  fmdPCB->DefineElement(11, 12.0107,    6, 0.27529425);
  fmdPCB->DefineElement(12, 14.0067,    7, 0.01415852);
  fmdPCB->DefineElement(13,  1.00794,   1, 0.03427566);
  fmdPCB->SetFillColor(7);
  new TGeoMedium("FMD PCB", 1, fmdPCB,  pPCB);

  // Aluminium 
  Double_t pAl[]  = { 0., mType, mMax, 10., .001,  -1., .003, .003 };
  TGeoMaterial* fmdAl = new TGeoMaterial("FMD Al", 26.981539, 13, 2.7);
  fmdAl->SetFillColor(3);
  new TGeoMedium("FMD Al", 1, fmdAl, pAl);
  
  // Silicon 
  Double_t pSi[]      = { 1., mType, mMax, 1.,  .001, 1., .001, .001 };
  TGeoMaterial* fmdSi = new TGeoMaterial("FMD Si", 28.0855, 14, 2.33);
  fmdSi->SetFillColor(2);
  new TGeoMedium("FMD Si", 1, fmdSi,  pSi);

  // Chip 
  Double_t pChip[]  = { 0., mType, mMax, 10., .01,  1., .003, .003 };
  TGeoMixture* fmdChip = new TGeoMixture("FMD Chip", 6, 2.36436);
  fmdChip->DefineElement(0,  12.0107,   6, 0.039730642);
  fmdChip->DefineElement(1,  14.0067,   7, 0.001396798);
  fmdChip->DefineElement(2,  15.9994,   8, 0.01169634);
  fmdChip->DefineElement(3,   1.00794,  1, 0.004367771);
  fmdChip->DefineElement(4,  28.0855,  14, 0.844665);
  fmdChip->DefineElement(5, 107.8682,  47, 0.0981434490);
  fmdChip->SetFillColor(4);
  new TGeoMedium("FMD Chip", 1, fmdChip, pChip);

  // Carbon 
  Double_t pC[]       = { 0., mType, mMax, 10., .01,  1., .003, .003 };
  TGeoMaterial* fmdC = new TGeoMaterial("FMD C", 12.011, 6, 2.265);
  fmdC->SetFillColor(6);
  new TGeoMedium("FMD C", 1, fmdC, pC);
  
  // Kapton (inside of Honeycombs)
  Double_t pKapton[]  = { 0., mType, mMax, 1.,  .001, 1., .001, .001 };
  TGeoMaterial* fmdKapton = new TGeoMaterial("FMD Kapton", 12.011,   6.,0.01);
  fmdKapton->SetFillColor(3);
  new TGeoMedium("FMD Kapton",1,fmdKapton,pKapton);
}

//____________________________________________________________________
void
Geometry::FMD::Browse(TBrowser* b) 
{
  Debug(5, "Browse", "Browsing the FMD");
  if (fInner) b->Add(fInner);
  if (fOuter) b->Add(fOuter);
  if (fFMD1)  b->Add(fFMD1); 
  if (fFMD1)  b->Add(fFMD2); 
  if (fFMD1)  b->Add(fFMD3); 
  Framework::Task::Browse(b);
}

//____________________________________________________________________
void
Geometry::FMD::DefaultSetup()
{
  SetRing('I',   new FMDRing('I'));
  SetRing('O',   new FMDRing('O'));
  SetDetector(1, new FMD1(fInner));
  SetDetector(2, new FMD2(fInner, fOuter));
  SetDetector(3, new FMD3(fInner, fOuter));
}

//____________________________________________________________________
void
Geometry::FMD::SetDetector(Int_t i, FMDDetector* det) 
{
  if (!det) return;
  FMDDetector* which;
  switch (i) {
  case 1: which = fFMD1; break;
  case 2: which = fFMD2; break;
  case 3: which = fFMD3; break;
  }
  if (which) {
    Warning("SetDetector", "Detector %d already set, will remove that", i);
    if (fTasks) fTasks->Remove(which);
  }
  switch (i) {
  case 1: fFMD1 = det; break;
  case 2: fFMD2 = det; break;   
  case 3: fFMD3 = det; break;
  }
  Add(det);
}

//____________________________________________________________________
void
Geometry::FMD::SetRing(Char_t i, FMDRing* rng) 
{
  if (!rng) return;
  FMDRing* which;
  switch (i) {
  case 'i': case 'I': which = fInner; break;
  case 'o': case 'O': which = fOuter; break;
  }
  if (which) {
    Warning("SetRing", "Ring %d already set, will remove that", i);
    if (fTasks) fTasks->Remove(which);
  }
  switch (i) {
  case 'i': case 'I': fInner = rng; break;
  case 'o': case 'O': fOuter = rng; break;
  }
  Add(rng);
}

//____________________________________________________________________
Geometry::FMDDetector*
Geometry::FMD::GetDetector(Int_t i) const
{
  switch (i) {
  case 1: return fFMD1;
  case 2: return fFMD2;
  case 3: return fFMD3;
  }
  return 0;
}

//____________________________________________________________________
Geometry::FMDRing*
Geometry::FMD::GetRing(Char_t i) const
{
  switch (i) {
  case 'I': case 'i': return fInner;
  case 'O': case 'o': return fOuter;
  }
  return 0;
}

//____________________________________________________________________
void
Geometry::FMD::Detector2XYZ(UShort_t  detector, 
			    Char_t    ring, 
			    UShort_t  sector, 
			    UShort_t  strip, 
			    Double_t& x, 
			    Double_t& y, 
			    Double_t& z) const
{
  FMDDetector* det = GetDetector(detector);
  if (!det) return;
  det->Detector2XYZ(ring, sector, strip, x, y, z);
}



//____________________________________________________________________
//
// EOF
//
