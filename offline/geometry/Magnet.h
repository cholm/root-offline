// -*- mode: C++ -*- 
//____________________________________________________________________ 
//  
// $Id: Magnet.h,v 1.7 2005-12-14 22:04:54 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    geometry/Magnet.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Geometry::Magnet
*/
#ifndef Geometry_Magnet
#define Geometry_Magnet
#ifndef Framework_Task
# include <framework/Task.h>
#endif
class TF1;
class TVector3;
class TBrowser;

/** @defgroup magnet Magnet classes */
namespace Geometry 
{
  /** @class Magnet geometry/Magnet.h <geometry/Magnet.h>
      @brief Magnet volume setup
      @ingroup geometry magnet
      @image html Magnet.png 
   */
  class Magnet : public Framework::Task 
  {
  protected: 
    /** Inner radius */
    Double_t fRInner;
    /** Outer radius */
    Double_t fROuter;
    /** Length along beam  */
    Double_t fLength;
    /** Max field strength */
    Double_t fField;
    /** Magnetic field type */
    Int_t fType;
    /** Field profile as a function of Z */
    TF1*     fF;

    /** Singleton instance */
    static Magnet* fgInstance;
  public:
    enum EType {
      kNone=0, 
      kUserStrongInhomo, 
      kUserInhomo, 
      kUniformZ
    };
    /** @return  Singleton instance */
    static Magnet* Instance();
   /** CTOR  */
    Magnet();
    /** Initialize */
    void Initialize(Option_t* option="");
    /** Register */
    void Register(Option_t* option="");
    /** @param option unused */
    virtual void Exec(Option_t* option="") {}
    /** Get the magnetic field at @f$ \mathbf{x} = (x,y,z) @f$ in
	units of kilo Gaus (10000 Gaus = 1 T). 
	@param x Space point to get the magnetic field for 
	@param b On return, the magnetic field in units of kilogaus */
    void Field(const TVector3& x, TVector3& b);
    /** @param r Inner radius  */
    void SetRInner(Double_t r=500) { fRInner = r; } //*MENU*
    /** @param r Outer radius  */
    void SetROuter(Double_t r=530) { fROuter = r; } //*MENU*
    /** @param l Length along beam  */
    void SetLength(Double_t l=1200)  { fLength = l; } //*MENU*
    /** @param f The maximum field strength in kilo gaus */
    void SetField(Double_t f=2)    { fField  = f; } //*MENU*
    /** Set the field type */ 
    void SetType(EType type=kUniformZ); //*MENU*
    
    /** @return Inner radius  */
    Double_t GetRInner() const { return fRInner; }
    /** @return Outer radius  */
    Double_t GetROuter() const { return fROuter; }
    /** @return Length along beam  */
    Double_t GetLength() const { return fLength; }
    /** Magnetic field type */ 
    Int_t GetType() const { return fType; }
    /** Maximum magnetic field */
    Double_t GetMax() const { return fField; }
    /** @return The field profile as a function of z  */
    TF1*     GetField() const { return fF; }
    /** @return true */
    Bool_t IsFolder() const { return kTRUE; }
    /** @param b Browser */
    void Browse(TBrowser* b);
    ClassDef(Magnet,0) //
  };
}

#endif
//____________________________________________________________________
//
// EOF
//
