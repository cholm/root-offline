//____________________________________________________________________ 
//  
// $Id: Magnet.cxx,v 1.6 2007-09-28 14:50:40 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    geometry/Magnet.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementation file for Geometry::Magnet
*/
#include "geometry/Magnet.h"
#include <TGeoVolume.h>
#include <TGeoMatrix.h>
#include <TGeoPgon.h>
#include <TGeoMaterial.h>
#include <TGeoMedium.h>
#include <TGeoManager.h>
#include <TF1.h>
#include <TBrowser.h>
#include <TVector3.h>
#include <TMath.h>

//____________________________________________________________________
ClassImp(Geometry::Magnet);

//____________________________________________________________________
Geometry::Magnet* Geometry::Magnet::fgInstance = 0;

//____________________________________________________________________
Geometry::Magnet* 
Geometry::Magnet::Instance() 
{
  if (!fgInstance) fgInstance = new Magnet;
  return fgInstance;
}

//____________________________________________________________________
Geometry::Magnet::Magnet() 
  : Framework::Task("Magnet", "Solonoidial magnet")
{
  SetRInner();
  SetROuter();
  SetLength();
  SetType();
  SetField();
}

//____________________________________________________________________
void 
Geometry::Magnet::SetType(Geometry::Magnet::EType type) 
{
  fType = type;
  if (fType == kNone) fField = 0;
}

//____________________________________________________________________
void
Geometry::Magnet::Initialize(Option_t* option) 
{
  Debug(5, "Initialize", "Setting up materials");
  Framework::Task::Initialize(option);
  switch (fType) {
  case kUserStrongInhomo:
  case kUserInhomo:
    fF = new TF1("magnet_field", "[0] * (1-cosh(x*10/[1]) / cosh(10))",
		 -fLength/2, fLength/2);
    fF->SetParameters(fField, fLength/2);
    break;    
  case kUniformZ:
    fF = new TF1("magnet_field", "[0]", -fLength/2, fLength/2);
    fF->SetParameter(0, fField);
    break;
  case kNone:
  default:
    fField = 0;
    fF     = new TF1("magnet_field", "0", -fLength/2,fLength);
    break;
  }

  Double_t mType = fType;
  Double_t mMax  = fField;  
  TGeoMaterial* fe = new TGeoMaterial("Iron", 55.85, 26.0, 7.87);
  fe->SetTransparency('0');
  Double_t par[] = { 0., mType, mMax, 1., .1, 1., .1, .1, .1, 0, 0 };
  TGeoMedium* medium = new TGeoMedium("Magnet Iron", 1, fe,  par);
  TString stype;
  switch (Int_t(mType)) {
  case kNone:             stype = "none" ; break;
  case kUserStrongInhomo: stype = "Strongly inhomogenious field"; break;
  case kUserInhomo:       stype = "Inhomogenious field"; break;
  case kUniformZ:         stype = "Uniform along the Z-axis"; break;
  }
  Info("Initialize", "Magnet type: %s  maximum: %f kG", stype.Data(), mMax);
}

//____________________________________________________________________
void
Geometry::Magnet::Register(Option_t* option) 
{
  Debug(5, "Register", "Setting up volume");
  TGeoRotation* magnetRot   = new TGeoRotation("magnetRot", 360./16,0,0);
  TGeoVolume* top           = gGeoManager->GetTopVolume();

  TGeoMedium* medium       = gGeoManager->GetMedium("Magnet Iron");
  TGeoPgon*   magnetShape  = new TGeoPgon("Magnet", 0, 360, 8, 2);
  magnetShape->DefineSection(0, -fLength/2, fRInner, fROuter);
  magnetShape->DefineSection(1,  fLength/2, fRInner, fROuter);
  TGeoVolume* magnetVolume = new TGeoVolume("Mag", magnetShape, medium);
  magnetVolume->SetLineColor(2);
  magnetVolume->SetFillColor(2);
  top->AddNode(magnetVolume,  0, magnetRot);
}

//____________________________________________________________________
void
Geometry::Magnet::Field(const TVector3& x, TVector3& b) 
{
  b[0] = b[1] = 0;
  if (TMath::Sqrt(x[0] * x[0] + x[1] * x[1]) > fROuter) b[2] = 0;
  else b[2] = fF->Eval(x[2]);
}

//____________________________________________________________________
void
Geometry::Magnet::Browse(TBrowser* b)
{
  b->Add(fF);
}

  
//____________________________________________________________________
//
// EOF
//
