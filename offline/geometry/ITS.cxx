//____________________________________________________________________ 
//  
// $Id: ITS.cxx,v 1.2 2007-09-28 14:50:40 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    geometry/ITS.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementation file for Geometry::ITS
*/
#include "geometry/ITS.h"
#include "geometry/Magnet.h"
#include <TGeoVolume.h>
#include <TGeoTube.h>
#include <TGeoMaterial.h>
#include <TGeoMedium.h>
#include <TGeoManager.h>
#include <TMath.h>

//____________________________________________________________________
ClassImp(Geometry::ITS);

//____________________________________________________________________
Geometry::ITS::ITS() 
  : Framework::Task("ITS", "Time Projection Chamber")
{
  SetRInner();
  SetROuter();
  SetLength();
}

//____________________________________________________________________
void
Geometry::ITS::Initialize(Option_t* option) 
{
  Debug(5, "Initialize", "Setting up materials");
  Framework::Task::Initialize(option);

  Geometry::Magnet* mag = Geometry::Magnet::Instance();
  Double_t mType = mag->GetType();
  Double_t mMax  = mag->GetMax();
  Double_t pGas[] = { 1., mType, mMax, 1., .001, 1., .001, .001, .001, 0, 0 };

   // MATERIALS, MIXTURES AND TRACKING MEDIA
// Mixture: BODY_Air     $
   nel     = 4;
   density = 0.001205;
   pMat2 = new TGeoMixture("BODY_Air     $", nel,density);
   pMat2->DefineElement(0,12.010700,6.,0.000124);	// C
   pMat2->DefineElement(1,14.006700,7.,0.755267);	// N
   pMat2->DefineElement(2,15.999400,8.,0.231781);	// O
   pMat2->DefineElement(3,39.948002,18.,0.012827);	// AR
   pMat2->SetIndex(1);
// Medium: BODY_Air     $
   numed   = 2;  // medium number
   par[0]  = 0.; // isvol
   par[1]  = 2.; // ifield
   par[2]  = 10.; // fieldm
   par[3]  = 10.; // tmaxfd
   par[4]  = -1.; // stemax
   par[5]  = -0.100000; // deemax
   par[6]  = 0.100000; // epsil
   par[7]  = -10.; // stmin
   pMed2 = new TGeoMedium("BODY_Air     $", numed,pMat2, par);
// Material: ITS_AL$
   pMat13 = new TGeoMaterial("ITS_AL$", ,26.982000,13.,2.698900,8.879380,999.);
   pMat13->SetIndex(12);
// Medium: ITS_AL$
   numed   = 13;  // medium number
   par[0]  = 0.; // isvol
   par[1]  = 2.; // ifield
   par[2]  = 10.; // fieldm
   par[3]  = 0.100000; // tmaxfd
   par[4]  = 1.; // stemax
   par[5]  = 0.100000; // deemax
   par[6]  = 0.000100; // epsil
   par[7]  = 0.; // stmin
   pMed13 = new TGeoMedium("ITS_AL$", numed,pMat13, par);
// Mixture: ITS_GEN AIR$
   nel     = 4;
   density = 0.001205;
   pMat9 = new TGeoMixture("ITS_GEN AIR$", nel,density);
   pMat9->DefineElement(0,12.010700,6.,0.000124);	// C
   pMat9->DefineElement(1,14.006700,7.,0.755267);	// N
   pMat9->DefineElement(2,15.999400,8.,0.231781);	// O
   pMat9->DefineElement(3,39.948002,18.,0.012827);	// AR
   pMat9->SetIndex(8);
// Medium: ITS_GEN AIR$
   numed   = 9;  // medium number
   par[0]  = 0.; // isvol
   par[1]  = 2.; // ifield
   par[2]  = 10.; // fieldm
   par[3]  = 0.100000; // tmaxfd
   par[4]  = 1.; // stemax
   par[5]  = 0.100000; // deemax
   par[6]  = 0.000100; // epsil
   par[7]  = 0.; // stmin
   pMed9 = new TGeoMedium("ITS_GEN AIR$", numed,pMat9, par);
// Material: ITS_SPD shield$
   pMat57 = new TGeoMaterial("ITS_SPD shield$", ,12.011000,6.,0.193000,220.195174,999.);
   pMat57->SetIndex(56);
// Medium: ITS_SPD shield$
   numed   = 57;  // medium number
   par[0]  = 0.; // isvol
   par[1]  = 2.; // ifield
   par[2]  = 10.; // fieldm
   par[3]  = 1.; // tmaxfd
   par[4]  = 1.; // stemax
   par[5]  = 0.500000; // deemax
   par[6]  = 0.001000; // epsil
   par[7]  = 0.; // stmin
   pMed57 = new TGeoMedium("ITS_SPD shield$", numed,pMat57, par);
// Mixture: ITS_SPD AIR$
   nel     = 4;
   density = 0.001205;
   pMat27 = new TGeoMixture("ITS_SPD AIR$", nel,density);
   pMat27->DefineElement(0,12.010700,6.,0.000124);	// C
   pMat27->DefineElement(1,14.006700,7.,0.755267);	// N
   pMat27->DefineElement(2,15.999400,8.,0.231781);	// O
   pMat27->DefineElement(3,39.948002,18.,0.012827);	// AR
   pMat27->SetIndex(26);
// Medium: ITS_SPD AIR$
   numed   = 27;  // medium number
   par[0]  = 0.; // isvol
   par[1]  = 2.; // ifield
   par[2]  = 10.; // fieldm
   par[3]  = 0.100000; // tmaxfd
   par[4]  = 1.; // stemax
   par[5]  = 0.100000; // deemax
   par[6]  = 0.000100; // epsil
   par[7]  = 0.; // stmin
   pMed27 = new TGeoMedium("ITS_SPD AIR$", numed,pMat27, par);
// Mixture: ITS_SPD SERVICES$
   nel     = 10;
   density = 80.311363;
   pMat56 = new TGeoMixture("ITS_SPD SERVICES$", nel,density);
   pMat56->DefineElement(0,1.007940,1.,0.005970);	// H
   pMat56->DefineElement(1,12.011000,6.,0.304704);	// C
   pMat56->DefineElement(2,14.006740,7.,0.042510);	// N
   pMat56->DefineElement(3,15.999400,8.,0.121715);	// O
   pMat56->DefineElement(4,28.085501,14.,0.001118);	// SI
   pMat56->DefineElement(5,51.996101,24.,0.030948);	// CR
   pMat56->DefineElement(6,54.938049,25.,0.003270);	// MN
   pMat56->DefineElement(7,55.845001,26.,0.107910);	// FE
   pMat56->DefineElement(8,58.693401,28.,0.020960);	// NI
   pMat56->DefineElement(9,63.546001,29.,0.360895);	// CU
   pMat56->SetIndex(55);
// Medium: ITS_SPD SERVICES$
   numed   = 56;  // medium number
   par[0]  = 0.; // isvol
   par[1]  = 2.; // ifield
   par[2]  = 10.; // fieldm
   par[3]  = 0.100000; // tmaxfd
   par[4]  = 1.; // stemax
   par[5]  = 0.100000; // deemax
   par[6]  = 0.000100; // epsil
   par[7]  = 0.; // stmin
   pMed56 = new TGeoMedium("ITS_SPD SERVICES$", numed,pMat56, par);
// Mixture: ITS_SPD End ladder$
   nel     = 5;
   density = 3.903403;
   pMat58 = new TGeoMixture("ITS_SPD End ladder$", nel,density);
   pMat58->DefineElement(0,1.007940,1.,0.004092);	// H
   pMat58->DefineElement(1,12.010700,6.,0.107274);	// C
   pMat58->DefineElement(2,14.010000,7.,0.011438);	// N
   pMat58->DefineElement(3,15.999400,8.,0.032476);	// O
   pMat58->DefineElement(4,63.540001,29.,0.844719);	// CU
   pMat58->SetIndex(57);
// Medium: ITS_SPD End ladder$
   numed   = 58;  // medium number
   par[0]  = 0.; // isvol
   par[1]  = 2.; // ifield
   par[2]  = 10.; // fieldm
   par[3]  = 1.; // tmaxfd
   par[4]  = 1.; // stemax
   par[5]  = 0.500000; // deemax
   par[6]  = 0.001000; // epsil
   par[7]  = 0.; // stmin
   pMed58 = new TGeoMedium("ITS_SPD End ladder$", numed,pMat58, par);
// Material: ITS_SPD SI CHIP$
   pMat5 = new TGeoMaterial("ITS_SPD SI CHIP$", ,28.086000,14.,2.330000,9.349774,999.);
   pMat5->SetIndex(4);
// Medium: ITS_SPD SI CHIP$
   numed   = 5;  // medium number
   par[0]  = 0.; // isvol
   par[1]  = 2.; // ifield
   par[2]  = 10.; // fieldm
   par[3]  = 0.100000; // tmaxfd
   par[4]  = 0.007500; // stemax
   par[5]  = 0.100000; // deemax
   par[6]  = 0.000100; // epsil
   par[7]  = 0.; // stmin
   pMed5 = new TGeoMedium("ITS_SPD SI CHIP$", numed,pMat5, par);
// Material: ITS_SPD SI$
   pMat23 = new TGeoMaterial("ITS_SPD SI$", ,28.086000,14.,2.330000,9.349774,999.);
   pMat23->SetIndex(22);
// Medium: ITS_SPD SI$
   numed   = 23;  // medium number
   par[0]  = 0.; // isvol
   par[1]  = 2.; // ifield
   par[2]  = 10.; // fieldm
   par[3]  = 0.100000; // tmaxfd
   par[4]  = 0.007500; // stemax
   par[5]  = 0.100000; // deemax
   par[6]  = 0.000100; // epsil
   par[7]  = 0.; // stmin
   pMed23 = new TGeoMedium("ITS_SPD SI$", numed,pMat23, par);
// Material: ITS_SI$
   pMat4 = new TGeoMaterial("ITS_SI$", ,28.086000,14.,2.330000,9.349774,999.);
   pMat4->SetIndex(3);
// Medium: ITS_SI$
   numed   = 4;  // medium number
   par[0]  = 0.; // isvol
   par[1]  = 2.; // ifield
   par[2]  = 10.; // fieldm
   par[3]  = 0.100000; // tmaxfd
   par[4]  = 0.007500; // stemax
   par[5]  = 0.100000; // deemax
   par[6]  = 0.000100; // epsil
   par[7]  = 0.; // stmin
   pMed4 = new TGeoMedium("ITS_SI$", numed,pMat4, par);
// Mixture: ITS_SPDBUS(AL+KPT+EPOX)$
   nel     = 5;
   density = 2.128505;
   pMat42 = new TGeoMixture("ITS_SPDBUS(AL+KPT+EPOX)$", nel,density);
   pMat42->DefineElement(0,1.007940,1.,0.023523);	// H
   pMat42->DefineElement(1,12.010700,6.,0.318053);	// C
   pMat42->DefineElement(2,14.010000,7.,0.009776);	// N
   pMat42->DefineElement(3,15.999400,8.,0.078057);	// O
   pMat42->DefineElement(4,26.982000,13.,0.570591);	// AL
   pMat42->SetIndex(41);
// Medium: ITS_SPDBUS(AL+KPT+EPOX)$
   numed   = 42;  // medium number
   par[0]  = 0.; // isvol
   par[1]  = 2.; // ifield
   par[2]  = 10.; // fieldm
   par[3]  = 0.100000; // tmaxfd
   par[4]  = 1.; // stemax
   par[5]  = 0.100000; // deemax
   par[6]  = 0.000100; // epsil
   par[7]  = 0.; // stmin
   pMed42 = new TGeoMedium("ITS_SPDBUS(AL+KPT+EPOX)$", numed,pMat42, par);
// Mixture: ITS_SPD C (M55J)$
   nel     = 4;
   density = 1.630000;
   pMat26 = new TGeoMixture("ITS_SPD C (M55J)$", nel,density);
   pMat26->DefineElement(0,12.010700,6.,0.908508);	// C
   pMat26->DefineElement(1,14.006700,7.,0.010388);	// N
   pMat26->DefineElement(2,15.999400,8.,0.055958);	// O
   pMat26->DefineElement(3,1.007940,1.,0.025147);	// H
   pMat26->SetIndex(25);
// Medium: ITS_SPD C (M55J)$
   numed   = 26;  // medium number
   par[0]  = 0.; // isvol
   par[1]  = 2.; // ifield
   par[2]  = 10.; // fieldm
   par[3]  = 0.100000; // tmaxfd
   par[4]  = 1.; // stemax
   par[5]  = 0.100000; // deemax
   par[6]  = 0.000100; // epsil
   par[7]  = 0.; // stmin
   pMed26 = new TGeoMedium("ITS_SPD C (M55J)$", numed,pMat26, par);
// Material: ITS_HEAT COND GLUE$
   pMat40 = new TGeoMaterial("ITS_HEAT COND GLUE$", ,12.011000,6.,1.930000,22.019518,999.);
   pMat40->SetIndex(39);
// Medium: ITS_HEAT COND GLUE$
   numed   = 40;  // medium number
   par[0]  = 0.; // isvol
   par[1]  = 2.; // ifield
   par[2]  = 10.; // fieldm
   par[3]  = 0.100000; // tmaxfd
   par[4]  = 1.; // stemax
   par[5]  = 0.100000; // deemax
   par[6]  = 0.000100; // epsil
   par[7]  = 0.; // stmin
   pMed40 = new TGeoMedium("ITS_HEAT COND GLUE$", numed,pMat40, par);
// Mixture: ITS_INOX$
   nel     = 9;
   density = 8.030000;
   pMat33 = new TGeoMixture("ITS_INOX$", nel,density);
   pMat33->DefineElement(0,12.010700,6.,0.000300);	// C
   pMat33->DefineElement(1,54.938000,25.,0.020000);	// MN
   pMat33->DefineElement(2,28.085501,14.,0.010000);	// SI
   pMat33->DefineElement(3,30.973801,15.,0.000450);	// P
   pMat33->DefineElement(4,32.066002,16.,0.000300);	// S
   pMat33->DefineElement(5,58.692799,28.,0.120000);	// NI
   pMat33->DefineElement(6,55.996101,24.,0.170000);	// CR
   pMat33->DefineElement(7,95.940002,42.,0.025000);	// MO
   pMat33->DefineElement(8,55.845001,26.,0.654000);	// FE
   pMat33->SetIndex(32);
// Medium: ITS_INOX$
   numed   = 33;  // medium number
   par[0]  = 0.; // isvol
   par[1]  = 2.; // ifield
   par[2]  = 10.; // fieldm
   par[3]  = 0.100000; // tmaxfd
   par[4]  = 1.; // stemax
   par[5]  = 0.100000; // deemax
   par[6]  = 0.000100; // epsil
   par[7]  = 0.; // stmin
   pMed33 = new TGeoMedium("ITS_INOX$", numed,pMat33, par);
// Mixture: ITS_Water$
   nel     = 2;
   density = 1.;
   pMat14 = new TGeoMixture("ITS_Water$", nel,density);
   pMat14->DefineElement(0,1.007940,1.,0.111894);	// H
   pMat14->DefineElement(1,15.999400,8.,0.888106);	// O
   pMat14->SetIndex(13);
// Medium: ITS_WATER$
   numed   = 14;  // medium number
   par[0]  = 0.; // isvol
   par[1]  = 2.; // ifield
   par[2]  = 10.; // fieldm
   par[3]  = 0.100000; // tmaxfd
   par[4]  = 1.; // stemax
   par[5]  = 0.100000; // deemax
   par[6]  = 0.000100; // epsil
   par[7]  = 0.; // stmin
   pMed14 = new TGeoMedium("ITS_WATER$", numed,pMat14, par);
// Material: ITS_ALUMINUM$
   pMat32 = new TGeoMaterial("ITS_ALUMINUM$", ,26.982000,13.,2.698900,8.879380,999.);
   pMat32->SetIndex(31);
// Medium: ITS_ALUMINUM$
   numed   = 32;  // medium number
   par[0]  = 0.; // isvol
   par[1]  = 2.; // ifield
   par[2]  = 10.; // fieldm
   par[3]  = 0.100000; // tmaxfd
   par[4]  = 1.; // stemax
   par[5]  = 0.100000; // deemax
   par[6]  = 0.000100; // epsil
   par[7]  = 0.; // stmin
   pMed32 = new TGeoMedium("ITS_ALUMINUM$", numed,pMat32, par);
// Material: ITS_ELASTO SIL$
   pMat41 = new TGeoMaterial("ITS_ELASTO SIL$", ,28.086000,14.,2.330000,9.349774,999.);
   pMat41->SetIndex(40);
// Medium: ITS_ELASTO SIL$
   numed   = 41;  // medium number
   par[0]  = 0.; // isvol
   par[1]  = 2.; // ifield
   par[2]  = 10.; // fieldm
   par[3]  = 0.100000; // tmaxfd
   par[4]  = 1.; // stemax
   par[5]  = 0.100000; // deemax
   par[6]  = 0.000100; // epsil
   par[7]  = 0.; // stmin
   pMed41 = new TGeoMedium("ITS_ELASTO SIL$", numed,pMat41, par);
// Mixture: ITS_SDD AIR$
   nel     = 4;
   density = 0.001205;
   pMat12 = new TGeoMixture("ITS_SDD AIR$", nel,density);
   pMat12->DefineElement(0,12.010700,6.,0.000124);	// C
   pMat12->DefineElement(1,14.006700,7.,0.755267);	// N
   pMat12->DefineElement(2,15.999400,8.,0.231781);	// O
   pMat12->DefineElement(3,39.948002,18.,0.012827);	// AR
   pMat12->SetIndex(11);
// Medium: ITS_SDD AIR$
   numed   = 12;  // medium number
   par[0]  = 0.; // isvol
   par[1]  = 2.; // ifield
   par[2]  = 10.; // fieldm
   par[3]  = 0.100000; // tmaxfd
   par[4]  = 1.; // stemax
   par[5]  = 0.100000; // deemax
   par[6]  = 0.000100; // epsil
   par[7]  = 0.; // stmin
   pMed12 = new TGeoMedium("ITS_SDD AIR$", numed,pMat12, par);
// Material: ITS_SDD SI insensitive$
   pMat45 = new TGeoMaterial("ITS_SDD SI insensitive$", ,28.086000,14.,2.330000,9.349774,999.);
   pMat45->SetIndex(44);
// Medium: ITS_SDD SI insensitive$
   numed   = 45;  // medium number
   par[0]  = 0.; // isvol
   par[1]  = 2.; // ifield
   par[2]  = 10.; // fieldm
   par[3]  = 0.100000; // tmaxfd
   par[4]  = 1.; // stemax
   par[5]  = 0.100000; // deemax
   par[6]  = 0.000100; // epsil
   par[7]  = 0.; // stmin
   pMed45 = new TGeoMedium("ITS_SDD SI insensitive$", numed,pMat45, par);
// Mixture: ITS_SDD C (M55J)$
   nel     = 4;
   density = 1.630000;
   pMat11 = new TGeoMixture("ITS_SDD C (M55J)$", nel,density);
   pMat11->DefineElement(0,12.010700,6.,0.908508);	// C
   pMat11->DefineElement(1,14.006700,7.,0.010388);	// N
   pMat11->DefineElement(2,15.999400,8.,0.055958);	// O
   pMat11->DefineElement(3,1.007940,1.,0.025147);	// H
   pMat11->SetIndex(10);
// Medium: ITS_SDD C (M55J)$
   numed   = 11;  // medium number
   par[0]  = 0.; // isvol
   par[1]  = 2.; // ifield
   par[2]  = 10.; // fieldm
   par[3]  = 0.100000; // tmaxfd
   par[4]  = 1.; // stemax
   par[5]  = 0.100000; // deemax
   par[6]  = 0.000100; // epsil
   par[7]  = 0.; // stmin
   pMed11 = new TGeoMedium("ITS_SDD C (M55J)$", numed,pMat11, par);
// Mixture: ITS_G10FR4$
   nel     = 14;
   density = 1.800000;
   pMat20 = new TGeoMixture("ITS_G10FR4$", nel,density);
   pMat20->DefineElement(0,28.085501,14.,0.151449);	// SI
   pMat20->DefineElement(1,40.077999,20.,0.081475);	// CA
   pMat20->DefineElement(2,26.981539,13.,0.041282);	// AL
   pMat20->DefineElement(3,24.305000,12.,0.009046);	// MG
   pMat20->DefineElement(4,10.811000,5.,0.013976);	// B
   pMat20->DefineElement(5,47.867001,22.,0.002877);	// TI
   pMat20->DefineElement(6,22.989771,11.,0.004451);	// NA
   pMat20->DefineElement(7,39.098301,19.,0.004981);	// K
   pMat20->DefineElement(8,55.845001,26.,0.002098);	// FE
   pMat20->DefineElement(9,18.998400,9.,0.004200);	// F
   pMat20->DefineElement(10,15.999400,8.,0.360438);	// O
   pMat20->DefineElement(11,12.010700,6.,0.275294);	// C
   pMat20->DefineElement(12,14.006700,7.,0.014159);	// N
   pMat20->DefineElement(13,1.007940,1.,0.034276);	// H
   pMat20->SetIndex(19);
// Medium: ITS_G10FR4$
   numed   = 20;  // medium number
   par[0]  = 0.; // isvol
   par[1]  = 2.; // ifield
   par[2]  = 10.; // fieldm
   par[3]  = 0.100000; // tmaxfd
   par[4]  = 1.; // stemax
   par[5]  = 0.100000; // deemax
   par[6]  = 0.000100; // epsil
   par[7]  = 0.; // stmin
   pMed20 = new TGeoMedium("ITS_G10FR4$", numed,pMat20, par);
// Mixture: ITS_SDD ruby sph. Al2O3$
   nel     = 2;
   density = 3.970000;
   pMat44 = new TGeoMixture("ITS_SDD ruby sph. Al2O3$", nel,density);
   pMat44->DefineElement(0,26.981539,13.,0.470700);	// AL
   pMat44->DefineElement(1,15.999400,8.,0.529300);	// O
   pMat44->SetIndex(43);
// Medium: ITS_SDD ruby sph. Al2O3$
   numed   = 44;  // medium number
   par[0]  = 0.; // isvol
   par[1]  = 2.; // ifield
   par[2]  = 10.; // fieldm
   par[3]  = 0.100000; // tmaxfd
   par[4]  = 1.; // stemax
   par[5]  = 0.100000; // deemax
   par[6]  = 0.000100; // epsil
   par[7]  = 0.; // stmin
   pMed44 = new TGeoMedium("ITS_SDD ruby sph. Al2O3$", numed,pMat44, par);
// Mixture: ITS_SDD C AL (M55J)$
   nel     = 5;
   density = 1.986600;
   pMat35 = new TGeoMixture("ITS_SDD C AL (M55J)$", nel,density);
   pMat35->DefineElement(0,12.010700,6.,0.817658);	// C
   pMat35->DefineElement(1,14.006700,7.,0.009349);	// N
   pMat35->DefineElement(2,15.999400,8.,0.050362);	// O
   pMat35->DefineElement(3,1.007940,1.,0.022632);	// H
   pMat35->DefineElement(4,26.981539,13.,0.100000);	// AL
   pMat35->SetIndex(34);
// Medium: ITS_SDD C AL (M55J)$
   numed   = 35;  // medium number
   par[0]  = 0.; // isvol
   par[1]  = 2.; // ifield
   par[2]  = 10.; // fieldm
   par[3]  = 0.100000; // tmaxfd
   par[4]  = 1.; // stemax
   par[5]  = 0.100000; // deemax
   par[6]  = 0.000100; // epsil
   par[7]  = 0.; // stmin
   pMed35 = new TGeoMedium("ITS_SDD C AL (M55J)$", numed,pMat35, par);
// Mixture: ITS_SDD SI CHIP$
   nel     = 6;
   density = 2.364360;
   pMat10 = new TGeoMixture("ITS_SDD SI CHIP$", nel,density);
   pMat10->DefineElement(0,12.010700,6.,0.039731);	// C
   pMat10->DefineElement(1,14.006700,7.,0.001397);	// N
   pMat10->DefineElement(2,15.999400,8.,0.011696);	// O
   pMat10->DefineElement(3,1.007940,1.,0.004368);	// H
   pMat10->DefineElement(4,28.085501,14.,0.844665);	// SI
   pMat10->DefineElement(5,107.868202,47.,0.098143);	// AG
   pMat10->SetIndex(9);
// Medium: ITS_SDD SI CHIP$
   numed   = 10;  // medium number
   par[0]  = 0.; // isvol
   par[1]  = 2.; // ifield
   par[2]  = 10.; // fieldm
   par[3]  = 0.100000; // tmaxfd
   par[4]  = 0.007500; // stemax
   par[5]  = 0.100000; // deemax
   par[6]  = 0.000100; // epsil
   par[7]  = 0.; // stmin
   pMed10 = new TGeoMedium("ITS_SDD SI CHIP$", numed,pMat10, par);
// Mixture: ITS_SDD X7R capacitors$
   nel     = 7;
   density = 7.145670;
   pMat43 = new TGeoMixture("ITS_SDD X7R capacitors$", nel,density);
   pMat43->DefineElement(0,137.326996,56.,0.251639);	// BA
   pMat43->DefineElement(1,47.867001,22.,0.084755);	// TI
   pMat43->DefineElement(2,15.999400,8.,0.085976);	// O
   pMat43->DefineElement(3,58.692799,28.,0.038245);	// NI
   pMat43->DefineElement(4,63.546001,29.,0.009471);	// CU
   pMat43->DefineElement(5,118.709999,50.,0.321736);	// SN
   pMat43->DefineElement(6,207.199997,82.,0.208177);	// PB
   pMat43->SetIndex(42);
// Medium: ITS_SDD X7R capacitors$
   numed   = 43;  // medium number
   par[0]  = 0.; // isvol
   par[1]  = 2.; // ifield
   par[2]  = 10.; // fieldm
   par[3]  = 0.100000; // tmaxfd
   par[4]  = 1.; // stemax
   par[5]  = 0.100000; // deemax
   par[6]  = 0.000100; // epsil
   par[7]  = 0.; // stmin
   pMed43 = new TGeoMedium("ITS_SDD X7R capacitors$", numed,pMat43, par);
// Mixture: ITS_SDD hybrid microcab$
   nel     = 5;
   density = 2.050200;
   pMat48 = new TGeoMixture("ITS_SDD hybrid microcab$", nel,density);
   pMat48->DefineElement(0,12.010700,6.,0.242819);	// C
   pMat48->DefineElement(1,1.007940,1.,0.009262);	// H
   pMat48->DefineElement(2,14.006700,7.,0.025742);	// N
   pMat48->DefineElement(3,15.999400,8.,0.073487);	// O
   pMat48->DefineElement(4,26.981539,13.,0.648690);	// AL
   pMat48->SetIndex(47);
// Medium: ITS_SDD hybrid microcab$
   numed   = 48;  // medium number
   par[0]  = 0.; // isvol
   par[1]  = 2.; // ifield
   par[2]  = 10.; // fieldm
   par[3]  = 0.100000; // tmaxfd
   par[4]  = 1.; // stemax
   par[5]  = 0.100000; // deemax
   par[6]  = 0.000100; // epsil
   par[7]  = 0.; // stmin
   pMed48 = new TGeoMedium("ITS_SDD hybrid microcab$", numed,pMat48, par);
// Mixture: ITS_SDD anode microcab$
   nel     = 5;
   density = 2.050200;
   pMat49 = new TGeoMixture("ITS_SDD anode microcab$", nel,density);
   pMat49->DefineElement(0,12.010700,6.,0.392654);	// C
   pMat49->DefineElement(1,1.007940,1.,0.012860);	// H
   pMat49->DefineElement(2,14.006700,7.,0.041627);	// N
   pMat49->DefineElement(3,15.999400,8.,0.118833);	// O
   pMat49->DefineElement(4,26.981539,13.,0.431909);	// AL
   pMat49->SetIndex(48);
// Medium: ITS_SDD anode microcab$
   numed   = 49;  // medium number
   par[0]  = 0.; // isvol
   par[1]  = 2.; // ifield
   par[2]  = 10.; // fieldm
   par[3]  = 0.100000; // tmaxfd
   par[4]  = 1.; // stemax
   par[5]  = 0.100000; // deemax
   par[6]  = 0.000100; // epsil
   par[7]  = 0.; // stmin
   pMed49 = new TGeoMedium("ITS_SDD anode microcab$", numed,pMat49, par);
// Mixture: ITS_SDD HV microcable$
   nel     = 5;
   density = 1.608700;
   pMat46 = new TGeoMixture("ITS_SDD HV microcable$", nel,density);
   pMat46->DefineElement(0,12.010700,6.,0.520089);	// C
   pMat46->DefineElement(1,1.007940,1.,0.019839);	// H
   pMat46->DefineElement(2,14.006700,7.,0.055137);	// N
   pMat46->DefineElement(3,15.999400,8.,0.157400);	// O
   pMat46->DefineElement(4,26.981539,13.,0.247536);	// AL
   pMat46->SetIndex(45);
// Medium: ITS_SDD HV microcable$
   numed   = 46;  // medium number
   par[0]  = 0.; // isvol
   par[1]  = 2.; // ifield
   par[2]  = 10.; // fieldm
   par[3]  = 0.100000; // tmaxfd
   par[4]  = 1.; // stemax
   par[5]  = 0.100000; // deemax
   par[6]  = 0.000100; // epsil
   par[7]  = 0.; // stmin
   pMed46 = new TGeoMedium("ITS_SDD HV microcable$", numed,pMat46, par);
// Mixture: ITS_SDD LV+signal cable$
   nel     = 5;
   density = 2.103500;
   pMat47 = new TGeoMixture("ITS_SDD LV+signal cable$", nel,density);
   pMat47->DefineElement(0,12.010700,6.,0.217224);	// C
   pMat47->DefineElement(1,1.007940,1.,0.008286);	// H
   pMat47->DefineElement(2,14.006700,7.,0.023029);	// N
   pMat47->DefineElement(3,15.999400,8.,0.065741);	// O
   pMat47->DefineElement(4,26.981539,13.,0.685720);	// AL
   pMat47->SetIndex(46);
// Medium: ITS_SDD LV+signal cable$
   numed   = 47;  // medium number
   par[0]  = 0.; // isvol
   par[1]  = 2.; // ifield
   par[2]  = 10.; // fieldm
   par[3]  = 0.100000; // tmaxfd
   par[4]  = 1.; // stemax
   par[5]  = 0.100000; // deemax
   par[6]  = 0.000100; // epsil
   par[7]  = 0.; // stmin
   pMed47 = new TGeoMedium("ITS_SDD LV+signal cable$", numed,pMat47, par);
// Mixture: ITS_AIR$
   nel     = 4;
   density = 0.001205;
   pMat8 = new TGeoMixture("ITS_AIR$", nel,density);
   pMat8->DefineElement(0,12.010700,6.,0.000124);	// C
   pMat8->DefineElement(1,14.006700,7.,0.755267);	// N
   pMat8->DefineElement(2,15.999400,8.,0.231781);	// O
   pMat8->DefineElement(3,39.948002,18.,0.012827);	// AR
   pMat8->SetIndex(7);
// Medium: ITS_AIR$
   numed   = 8;  // medium number
   par[0]  = 0.; // isvol
   par[1]  = 2.; // ifield
   par[2]  = 10.; // fieldm
   par[3]  = 0.100000; // tmaxfd
   par[4]  = 1.; // stemax
   par[5]  = 0.100000; // deemax
   par[6]  = 0.000100; // epsil
   par[7]  = 0.; // stmin
   pMed8 = new TGeoMedium("ITS_AIR$", numed,pMat8, par);
// Mixture: ITS_C (M55J)$
   nel     = 4;
   density = 1.630000;
   pMat7 = new TGeoMixture("ITS_C (M55J)$", nel,density);
   pMat7->DefineElement(0,12.010700,6.,0.908508);	// C
   pMat7->DefineElement(1,14.006700,7.,0.010388);	// N
   pMat7->DefineElement(2,15.999400,8.,0.055958);	// O
   pMat7->DefineElement(3,1.007940,1.,0.025147);	// H
   pMat7->SetIndex(6);
// Medium: ITS_C (M55J)$
   numed   = 7;  // medium number
   par[0]  = 0.; // isvol
   par[1]  = 2.; // ifield
   par[2]  = 10.; // fieldm
   par[3]  = 0.100000; // tmaxfd
   par[4]  = 1.; // stemax
   par[5]  = 0.100000; // deemax
   par[6]  = 0.000100; // epsil
   par[7]  = 0.; // stmin
   pMed7 = new TGeoMedium("ITS_C (M55J)$", numed,pMat7, par);
// Mixture: ITS_CERAMICS$
   nel     = 5;
   density = 3.600000;
   pMat17 = new TGeoMixture("ITS_CERAMICS$", nel,density);
   pMat17->DefineElement(0,26.981539,13.,0.444341);	// AL
   pMat17->DefineElement(1,15.999400,8.,0.521338);	// O
   pMat17->DefineElement(2,28.085501,14.,0.013087);	// SI
   pMat17->DefineElement(3,54.938049,25.,0.017814);	// MN
   pMat17->DefineElement(4,51.996101,24.,0.003421);	// CR
   pMat17->SetIndex(16);
// Medium: ITS_CERAMICS$
   numed   = 17;  // medium number
   par[0]  = 0.; // isvol
   par[1]  = 2.; // ifield
   par[2]  = 10.; // fieldm
   par[3]  = 0.100000; // tmaxfd
   par[4]  = 1.; // stemax
   par[5]  = 0.100000; // deemax
   par[6]  = 0.000100; // epsil
   par[7]  = 0.; // stmin
   pMed17 = new TGeoMedium("ITS_CERAMICS$", numed,pMat17, par);
// Mixture: ITS_AIRFMDSDD$
   nel     = 11;
   density = 1.525328;
   pMat52 = new TGeoMixture("ITS_AIRFMDSDD$", nel,density);
   pMat52->DefineElement(0,1.007940,1.,0.019965);	// H
   pMat52->DefineElement(1,12.011000,6.,0.340961);	// C
   pMat52->DefineElement(2,14.006740,7.,0.041225);	// N
   pMat52->DefineElement(3,15.999400,8.,0.200352);	// O
   pMat52->DefineElement(4,28.085501,14.,0.000386);	// SI
   pMat52->DefineElement(5,51.996101,24.,0.001467);	// CR
   pMat52->DefineElement(6,54.938049,25.,0.000155);	// MN
   pMat52->DefineElement(7,55.845001,26.,0.005113);	// FE
   pMat52->DefineElement(8,58.693401,28.,0.000993);	// NI
   pMat52->DefineElement(9,63.546001,29.,0.381262);	// CU
   pMat52->DefineElement(10,26.981539,13.,0.008121);	// AL
   pMat52->SetIndex(51);
// Medium: ITS_AIRFMDSDD$
   numed   = 52;  // medium number
   par[0]  = 0.; // isvol
   par[1]  = 2.; // ifield
   par[2]  = 10.; // fieldm
   par[3]  = 0.100000; // tmaxfd
   par[4]  = 1.; // stemax
   par[5]  = 0.100000; // deemax
   par[6]  = 0.000100; // epsil
   par[7]  = 0.; // stmin
   pMed52 = new TGeoMedium("ITS_AIRFMDSDD$", numed,pMat52, par);
// Material: ITS_ITS SANDW C$
   pMat39 = new TGeoMaterial("ITS_ITS SANDW C$", ,12.011000,6.,0.410000,103.652853,999.);
   pMat39->SetIndex(38);
// Medium: ITS_ITS SANDW C$
   numed   = 39;  // medium number
   par[0]  = 0.; // isvol
   par[1]  = 2.; // ifield
   par[2]  = 10.; // fieldm
   par[3]  = 0.100000; // tmaxfd
   par[4]  = 1.; // stemax
   par[5]  = 0.100000; // deemax
   par[6]  = 0.000100; // epsil
   par[7]  = 0.; // stmin
   pMed39 = new TGeoMedium("ITS_ITS SANDW C$", numed,pMat39, par);
// Mixture: ITS_ITS SANDW CFMDSDD$
   nel     = 11;
   density = 1.935328;
   pMat54 = new TGeoMixture("ITS_ITS SANDW CFMDSDD$", nel,density);
   pMat54->DefineElement(0,1.007940,1.,0.016302);	// H
   pMat54->DefineElement(1,12.011000,6.,0.461870);	// C
   pMat54->DefineElement(2,14.006740,7.,0.033662);	// N
   pMat54->DefineElement(3,15.999400,8.,0.163595);	// O
   pMat54->DefineElement(4,28.085501,14.,0.000315);	// SI
   pMat54->DefineElement(5,51.996101,24.,0.001197);	// CR
   pMat54->DefineElement(6,54.938049,25.,0.000127);	// MN
   pMat54->DefineElement(7,55.845001,26.,0.004175);	// FE
   pMat54->DefineElement(8,58.693401,28.,0.000811);	// NI
   pMat54->DefineElement(9,63.546001,29.,0.311315);	// CU
   pMat54->DefineElement(10,26.981539,13.,0.006631);	// AL
   pMat54->SetIndex(53);
// Medium: ITS_ITS SANDW CFMDSDD$
   numed   = 54;  // medium number
   par[0]  = 0.; // isvol
   par[1]  = 2.; // ifield
   par[2]  = 10.; // fieldm
   par[3]  = 0.100000; // tmaxfd
   par[4]  = 1.; // stemax
   par[5]  = 0.100000; // deemax
   par[6]  = 0.000100; // epsil
   par[7]  = 0.; // stmin
   pMed54 = new TGeoMedium("ITS_ITS SANDW CFMDSDD$", numed,pMat54, par);
// Mixture: ITS_SDD/SSD rings$
   nel     = 4;
   density = 0.287500;
   pMat50 = new TGeoMixture("ITS_SDD/SSD rings$", nel,density);
   pMat50->DefineElement(0,12.010700,6.,0.854324);	// C
   pMat50->DefineElement(1,1.007940,1.,0.026409);	// H
   pMat50->DefineElement(2,14.006700,7.,0.023050);	// N
   pMat50->DefineElement(3,15.999400,8.,0.096217);	// O
   pMat50->SetIndex(49);
// Medium: ITS_SDD/SSD rings$
   numed   = 50;  // medium number
   par[0]  = 0.; // isvol
   par[1]  = 2.; // ifield
   par[2]  = 10.; // fieldm
   par[3]  = 0.100000; // tmaxfd
   par[4]  = 1.; // stemax
   par[5]  = 0.100000; // deemax
   par[6]  = 0.000100; // epsil
   par[7]  = 0.; // stmin
   pMed50 = new TGeoMedium("ITS_SDD/SSD rings$", numed,pMat50, par);
// Mixture: ITS_ITS SANDW CFMDSSD$
   nel     = 12;
   density = 1.656428;
   pMat55 = new TGeoMixture("ITS_ITS SANDW CFMDSSD$", nel,density);
   pMat55->DefineElement(0,1.007940,1.,0.014065);	// H
   pMat55->DefineElement(1,12.011000,6.,0.520598);	// C
   pMat55->DefineElement(2,14.006740,7.,0.022650);	// N
   pMat55->DefineElement(3,15.999400,8.,0.105018);	// O
   pMat55->DefineElement(4,28.085501,14.,0.021768);	// SI
   pMat55->DefineElement(5,51.996101,24.,0.009952);	// CR
   pMat55->DefineElement(6,54.938049,25.,0.001051);	// MN
   pMat55->DefineElement(7,55.845001,26.,0.034700);	// FE
   pMat55->DefineElement(8,58.693401,28.,0.006740);	// NI
   pMat55->DefineElement(9,63.546001,29.,0.249406);	// CU
   pMat55->DefineElement(10,26.981539,13.,0.010345);	// AL
   pMat55->DefineElement(11,107.868202,47.,0.000371);	// AG
   pMat55->SetIndex(54);
// Medium: ITS_ITS SANDW CFMDSSD$
   numed   = 55;  // medium number
   par[0]  = 0.; // isvol
   par[1]  = 2.; // ifield
   par[2]  = 10.; // fieldm
   par[3]  = 0.100000; // tmaxfd
   par[4]  = 1.; // stemax
   par[5]  = 0.100000; // deemax
   par[6]  = 0.000100; // epsil
   par[7]  = 0.; // stmin
   pMed55 = new TGeoMedium("ITS_ITS SANDW CFMDSSD$", numed,pMat55, par);
// Mixture: ITS_AIRFMDSSD$
   nel     = 12;
   density = 1.246428;
   pMat53 = new TGeoMixture("ITS_AIRFMDSSD$", nel,density);
   pMat53->DefineElement(0,1.007940,1.,0.019777);	// H
   pMat53->DefineElement(1,12.011000,6.,0.325901);	// C
   pMat53->DefineElement(2,14.006740,7.,0.031848);	// N
   pMat53->DefineElement(3,15.999400,8.,0.147668);	// O
   pMat53->DefineElement(4,28.085501,14.,0.030609);	// SI
   pMat53->DefineElement(5,51.996101,24.,0.013993);	// CR
   pMat53->DefineElement(6,54.938049,25.,0.001479);	// MN
   pMat53->DefineElement(7,55.845001,26.,0.048792);	// FE
   pMat53->DefineElement(8,58.693401,28.,0.009477);	// NI
   pMat53->DefineElement(9,63.546001,29.,0.350697);	// CU
   pMat53->DefineElement(10,26.981539,13.,0.014546);	// AL
   pMat53->DefineElement(11,107.868202,47.,0.005213);	// AG
   pMat53->SetIndex(52);
// Medium: ITS_AIRFMDSSD$
   numed   = 53;  // medium number
   par[0]  = 0.; // isvol
   par[1]  = 2.; // ifield
   par[2]  = 10.; // fieldm
   par[3]  = 0.100000; // tmaxfd
   par[4]  = 1.; // stemax
   par[5]  = 0.100000; // deemax
   par[6]  = 0.000100; // epsil
   par[7]  = 0.; // stmin
   pMed53 = new TGeoMedium("ITS_AIRFMDSSD$", numed,pMat53, par);
// Material: ITS_COPPER$
   pMat16 = new TGeoMaterial("ITS_COPPER$", ,63.546001,29.,8.960000,1.435164,999.);
   pMat16->SetIndex(15);
// Medium: ITS_COPPER$
   numed   = 16;  // medium number
   par[0]  = 0.; // isvol
   par[1]  = 2.; // ifield
   par[2]  = 10.; // fieldm
   par[3]  = 0.100000; // tmaxfd
   par[4]  = 1.; // stemax
   par[5]  = 0.100000; // deemax
   par[6]  = 0.000100; // epsil
   par[7]  = 0.; // stmin
   pMed16 = new TGeoMedium("ITS_COPPER$", numed,pMat16, par);
// Mixture: ITS_GEN C (M55J)$
   nel     = 4;
   density = 1.630000;
   pMat21 = new TGeoMixture("ITS_GEN C (M55J)$", nel,density);
   pMat21->DefineElement(0,12.010700,6.,0.908508);	// C
   pMat21->DefineElement(1,14.006700,7.,0.010388);	// N
   pMat21->DefineElement(2,15.999400,8.,0.055958);	// O
   pMat21->DefineElement(3,1.007940,1.,0.025147);	// H
   pMat21->SetIndex(20);
// Medium: ITS_GEN C (M55J)$
   numed   = 21;  // medium number
   par[0]  = 0.; // isvol
   par[1]  = 2.; // ifield
   par[2]  = 10.; // fieldm
   par[3]  = 0.100000; // tmaxfd
   par[4]  = 1.; // stemax
   par[5]  = 0.100000; // deemax
   par[6]  = 0.000100; // epsil
   par[7]  = 0.; // stmin
   pMed21 = new TGeoMedium("ITS_GEN C (M55J)$", numed,pMat21, par);
// Mixture: ITS_inox/alum$
   nel     = 5;
   density = 3.075000;
   pMat51 = new TGeoMixture("ITS_inox/alum$", nel,density);
   pMat51->DefineElement(0,27.,13.,0.816164);	// AL
   pMat51->DefineElement(1,55.847000,26.,0.131443);	// FE
   pMat51->DefineElement(2,51.996101,24.,0.033091);	// CR
   pMat51->DefineElement(3,58.693401,28.,0.018384);	// NI
   pMat51->DefineElement(4,28.085501,14.,0.000919);	// SI
   pMat51->SetIndex(50);
// Medium: ITS_inox/alum$
   numed   = 51;  // medium number
   par[0]  = 0.; // isvol
   par[1]  = 2.; // ifield
   par[2]  = 10.; // fieldm
   par[3]  = 0.100000; // tmaxfd
   par[4]  = 1.; // stemax
   par[5]  = 0.100000; // deemax
   par[6]  = 0.000100; // epsil
   par[7]  = 0.; // stmin
   pMed51 = new TGeoMedium("ITS_inox/alum$", numed,pMat51, par);



  TGeoMedium* gas = new TGeoMedium("ITS Gas", 1, tpcGas,  pGas);

  // Aluminium 
  Double_t pAl[]  = { 0., mType, mMax, 10., .001,  -1., .003, .003 };
  TGeoMaterial* tpcAl = new TGeoMaterial("ITS Al", 26.981539, 13, 2.7);
  tpcAl->SetFillColor(3);
  TGeoMedium* al = new TGeoMedium("ITS Al", 1, tpcAl, pAl);
}

//____________________________________________________________________
void
Geometry::ITS::Register(Option_t* option) 
{
  Debug(5, "Register", "Setting up volume");
  // Container volume 
  TGeoVolume* top       = gGeoManager->GetTopVolume();

}

//________________________________________________________c____________
//
// EOF
//
