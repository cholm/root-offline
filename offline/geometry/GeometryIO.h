// -*- mode: C++ -*- 
//____________________________________________________________________ 
//  
// $Id: GeometryIO.h,v 1.2 2005-12-14 22:04:54 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    geometry/GeometryIO.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Geometry::GeometryWriter and
    Geometry::GeometryReader 
*/
#ifndef Geometry_GeometryIO
#define Geometry_GeometryIO
#ifndef Framework_Task
# include <framework/Task.h>
#endif
class TGeoManager;
class TFile;
class TBrowser;

namespace Geometry 
{
  /** @class GeometryWriter geometry/GeometryIO.h <geometry/GeometryIO.h>
      @brief Write the geometry to file
      @ingroup geometry
   */
  class GeometryWriter : public Framework::Task 
  {
  protected: 
    /** File to write to */
    TFile* fFile;
  public:
   /** CTOR  
       @param name Name of TGeoManager should have on file
       @param file Where to write to */
    GeometryWriter(const char* name, const char* file);
    /** Initialize - Open file, and post the TGeoManager on folder 
	@param option Option */
    void Initialize(Option_t* option="");
    /** Does nothing @param option Option */
    void Register(Option_t* option="") {}
    /** @param option unused */
    virtual void Exec(Option_t* option="") {}
    /** Write to disk, and close file. 
	@param option unused */
    virtual void Finish(Option_t* option="");
    /** @return true */
    Bool_t IsFolder() const { return kTRUE; }
    /** @param b Browser */
    void Browse(TBrowser* b);
    ClassDef(GeometryWriter,0) //
  };
  //==================================================================
  /** @class GeometryReader geometry/GeometryIO.h <geometry/GeometryIO.h>
      @brief Write the geometry to file
      @ingroup geometry 
   */
  class GeometryReader : public Framework::Task 
  {
  protected: 
    /** File to write to */
    TFile* fFile;
  public:
   /** CTOR  
       @param name Name of TGeoManager the task should look for 
       @param file Where to read from */
    GeometryReader(const char* name, const char* file);
    /** Initialize - Open file, read object, and post to folder 
	@param option Option */
    void Initialize(Option_t* option="");
    /** Does nothing @param option Option */
    void Register(Option_t* option="") {}
    /** @param option unused */
    virtual void Exec(Option_t* option="") {}
    /** Close file. 
	@param option unused */
    virtual void Finish(Option_t* option="");
    /** @return true */
    Bool_t IsFolder() const { return kTRUE; }
    /** @param b Browser */
    void Browse(TBrowser* b);
    ClassDef(GeometryReader,0) //
  };
}

#endif
//____________________________________________________________________
//
// EOF
//
