//____________________________________________________________________ 
//  
// $Id: Cave.cxx,v 1.3 2007-09-28 14:50:40 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    geometry/Cave.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementation file for Geometry::Cave
*/
# include "geometry/Cave.h"
# include "geometry/Magnet.h"
# include <TGeoVolume.h>
# include <TGeoTube.h>
# include <TGeoMaterial.h>
# include <TGeoMedium.h>
# include <TGeoManager.h>
# include <TMath.h>

//____________________________________________________________________
ClassImp(Geometry::Cave);

//____________________________________________________________________
Geometry::Cave::Cave() 
  : Framework::Task("Cave", "The cave"), 
    fR(0),
    fZ(0)
{}

//____________________________________________________________________
void
Geometry::Cave::Initialize(Option_t* option) 
{
  Debug(5, "Initialize", "Setting up materials");
  Framework::Task::Initialize(option);
  if (!gGeoManager) gGeoManager = new TGeoManager(GetName(), "Geometry");
  TGeoMixture* caveAir = new TGeoMixture("Cave air", 4, .00120479);
  caveAir->DefineElement(0, 12.0107,  6., 0.000124);
  caveAir->DefineElement(1, 14.0067,  7., 0.755267);
  caveAir->DefineElement(2, 15.9994,  8., 0.231781);
  caveAir->DefineElement(3, 39.948,  18., 0.012827);
  Geometry::Magnet* mag = Geometry::Magnet::Instance();
  Double_t mType = mag->GetType();
  Double_t mMax  = mag->GetMax();
  Double_t par[] = { 0., mType, mMax, 1., .001, 1., .001, .001, .001, 0, 0 };
  TGeoMedium* medium = new TGeoMedium("Cave air", 1, caveAir,  par);
}

//____________________________________________________________________
void
Geometry::Cave::Register(Option_t* option) 
{
  Debug(5, "Initialize", "Setting up volume");
  Geometry::Magnet* geometry = Geometry::Magnet::Instance();
  if (fR <= 0) 
    fR = geometry->GetROuter() / TMath::Cos(2 * TMath::Pi() / 16) + 10;
  if (fZ <= 0) 
    fZ = geometry->GetLength() / 2 + 10;
  TGeoMedium* medium     = gGeoManager->GetMedium("Cave air");
  TGeoTube*   caveShape  = new TGeoTube("Cave", 0, fR, fZ);
  TGeoVolume* caveVolume = new TGeoVolume("Cave", caveShape, medium);
  caveVolume->SetVisibility(kFALSE);
  gGeoManager->SetTopVolume(caveVolume);
}

//____________________________________________________________________
//
// EOF
//
