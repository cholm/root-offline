// -*- mode: C++ -*- 
//____________________________________________________________________ 
//  
// $Id: ITS.h,v 1.1 2007-09-28 14:00:54 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    geometry/ITS.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Geometry::ITS
*/
#ifndef Geometry_ITS
#define Geometry_ITS
#ifndef Framework_Task
# include <framework/Task.h>
#endif
class TGeoVolume;

/** @defgroup its Inner Tracking System classes */
namespace Geometry 
{
  /** @class ITS geometry/ITS.h <geometry/ITS.h>
      @brief ITS geometry 
      @ingroup geometry its
      @image html ITS.png 
   */
  class ITS : public Framework::Task 
  {
  public:
    /** CTOR */
    ITS();
    /** Initialize */
    void Initialize(Option_t* option="");
    /** Register */
    void Register(Option_t* option="");
    /** @param option unused */
    virtual void Exec(Option_t* option="") {}
  private:
    ClassDef(ITS,0) //
  };
}

#endif
//____________________________________________________________________
//
// EOF
//
