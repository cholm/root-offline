//____________________________________________________________________ 
//  
// $Id: TOF.cxx,v 1.7 2007-09-28 14:50:40 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    geometry/TOF.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementation file for Geometry::TOF
*/
# include "geometry/TOF.h"
# include "geometry/Magnet.h"
# include <framework/Main.h>
# include <TGeoVolume.h>
# include <TGeoTube.h>
# include <TGeoMaterial.h>
# include <TGeoMedium.h>
# include <TGeoManager.h>
# include <TMath.h>

//____________________________________________________________________
ClassImp(Geometry::TOF);

//____________________________________________________________________
Geometry::TOF::TOF() 
  : Framework::Task("TOF", "Time of Flight")
{
  SetRInner();
  SetROuter();
  SetLength();
  SetNSector();
  SetNRing();
  SetNStrip();
}

//____________________________________________________________________
void
Geometry::TOF::Initialize(Option_t* option) 
{
  Debug(5, "Initialize", "Setting up materials");
  Framework::Task::Initialize(option);

  TGeoMixture* tofScint = new TGeoMixture("TOF scint", 2, 1.032);
  tofScint->DefineElement(0,  1.008, 1., 0.5);
  tofScint->DefineElement(1, 12.000, 6., 0.5);
  tofScint->SetTransparency('0');

  Geometry::Magnet* mag = Geometry::Magnet::Instance();
  Double_t mType = mag->GetType();
  Double_t mMax  = mag->GetMax();
  Double_t par[] = { 1., mType, mMax, 1., .001, 1., .001, .001, .001, 0, 0 };
  TGeoMedium* medium = new TGeoMedium("TOF scint", 1, tofScint,  par);
}

//____________________________________________________________________
void
Geometry::TOF::Register(Option_t* option) 
{
  Debug(5, "Register", "Setting up volume");
  TGeoMedium* medium       = gGeoManager->GetMedium("TOF scint");
  TGeoTube*   tofShape     = new TGeoTube("TOF", fRInner, fROuter, fLength/2);
  TGeoVolume* tofVolume    = new TGeoVolume("TOF", tofShape, medium);
  tofVolume->SetLineColor(kYellow);
  tofVolume->SetFillColor(kYellow);
  TGeoVolume* ringVolume   = tofVolume->Divide("TOFR",  3, fNRing,  0,0,0,"N");
  TGeoVolume* sectorVolume = ringVolume->Divide("TOFS", 2, fNSector,0,0,0,"N");
  TGeoVolume* top          = gGeoManager->GetTopVolume();
  sectorVolume->SetLineColor(kYellow);
  sectorVolume->SetFillColor(kYellow);
  top->AddNode(tofVolume, 0, 0);
}


//____________________________________________________________________
//
// EOF
//
