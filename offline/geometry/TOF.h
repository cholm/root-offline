// -*- mode: C++ -*- 
//____________________________________________________________________ 
//  
// $Id: TOF.h,v 1.6 2005-12-14 22:04:54 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    geometry/TOF.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Geometry::TOF
*/
#ifndef Geometry_TOF
#define Geometry_TOF
#ifndef Framework_Task
# include <framework/Task.h>
#endif

namespace Geometry 
{
  /** @class TOF geometry/TOF.h <geometry/TOF.h>
      @brief TOF simulation 
      @ingroup geometry tof
      @image html TOF.png 
   */
  class TOF : public Framework::Task 
  {
  protected:
    /** Inner radius */
    Double_t fRInner;
    /** Outer radius */
    Double_t fROuter;
    /** Length along beam  */
    Double_t fLength;
    /** Number of columns  */
    Int_t    fNSector;
    /** Number of rings  */
    Int_t    fNRing;    
    /** Number of strips */
    Int_t    fNStrip;
  public:
    /** CTOR */
    TOF();
    /** Initialize */
    void Initialize(Option_t* option="");
    /** Register */
    void Register(Option_t* option="");
    /** @param option unused */
    virtual void Exec(Option_t* option="") {}
    /** @param r Inner radius  */
    void SetRInner(Double_t r=370) { fRInner = r; }   //*MENU*
    /** @param r Outer radius  */
    void SetROuter(Double_t r=400) { fROuter = r; }   //*MENU*
    /** @param l Length along beam  */
    void SetLength(Double_t l=745)  { fLength = l; }   //*MENU*
    /** @param x # of sectors  */
    void     SetNSector(Int_t x=8)     { fNSector = x; }   //*MENU*
    /** @param x # of rings  */
    void     SetNRing(Int_t x=5)        { fNRing = x; }   //*MENU* 
    /** @param x # of strips  */
    void     SetNStrip(Int_t x=1)       { fNStrip = x; }   //*MENU* 
    /** @return Inner radius  */
    Double_t GetRInner() const { return fRInner; }
    /** @return Outer radius  */
    Double_t GetROuter() const { return fROuter; }
    /** @return Length along beam  */
    Double_t GetLength() const { return fLength; }
    /** @return # of sectors  */
    Int_t    GetNSector()  const { return fNSector; }
    /** @return # of rings  */
    Int_t    GetNRing()     const { return fNRing; }    
    /** @return # of strips  */
    Int_t    GetNStrip()     const { return fNStrip; }    
    ClassDef(TOF,0) //
  };
}

#endif
//____________________________________________________________________
//
// EOF
//
