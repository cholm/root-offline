//____________________________________________________________________ 
//  
// $Id: GeometryIO.cxx,v 1.2 2007-09-28 14:50:40 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    geometry/GeometryIO.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementation file for Geometry::GeometryWriter and 
    Geometry::GeometryReader
*/
#include "geometry/GeometryIO.h"
#include <TGeoManager.h>
#include <TBrowser.h>
#include <TFile.h>
#include <TFolder.h>

//____________________________________________________________________
ClassImp(Geometry::GeometryWriter);

//____________________________________________________________________
Geometry::GeometryWriter::GeometryWriter(const char* name, const char* file) 
  : Framework::Task(name, file)
{}

//____________________________________________________________________
void
Geometry::GeometryWriter::Initialize(Option_t* option) 
{
  Debug(5, "Initialize", "Setting up materials");
  Framework::Task::Initialize(option);
  TDirectory* savDir = gDirectory;
  fFile              = TFile::Open(GetTitle(), "RECREATE");
  savDir->cd();
  if (gGeoManager) fFolder->Add(gGeoManager);
}

//____________________________________________________________________
void
Geometry::GeometryWriter::Finish(Option_t* option) 
{
  Debug(5, "Register", "Writting to disk");
  TDirectory* savDir = gDirectory;
  fFile->cd();
  if (gGeoManager) gGeoManager->Write(GetName());
  fFile->Close();
  fFile = 0;
  savDir->cd();
}

//____________________________________________________________________
void
Geometry::GeometryWriter::Browse(TBrowser* b)
{
  b->Add(fFile);
}

//====================================================================
ClassImp(Geometry::GeometryReader);

//____________________________________________________________________
Geometry::GeometryReader::GeometryReader(const char* name, const char* file) 
  : Framework::Task(name, file)
{}

//____________________________________________________________________
void
Geometry::GeometryReader::Initialize(Option_t* option) 
{
  Debug(5, "Initialize", "Setting up materials");
  Framework::Task::Initialize(option);
  TDirectory* savDir      = gDirectory;
  fFile                   = TFile::Open(GetTitle(), "READ");
  TGeoManager* geoManager = static_cast<TGeoManager*>(fFile->Get(GetName()));
  savDir->cd();
  if (gGeoManager) {
    gGeoManager = geoManager;
    fFolder->Add(gGeoManager);
  }
}


//____________________________________________________________________
void
Geometry::GeometryReader::Finish(Option_t* option) 
{
  Debug(5, "Register", "Writting to disk");
  TDirectory* savDir = gDirectory;
  fFile->cd();
  fFile->Close();
  fFile = 0;
  savDir->cd();
}

//____________________________________________________________________
void
Geometry::GeometryReader::Browse(TBrowser* b)
{
  b->Add(fFile);
}

  
//____________________________________________________________________
//
// EOF
//
