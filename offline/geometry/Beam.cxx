//____________________________________________________________________ 
//  
// $Id: Beam.cxx,v 1.3 2007-09-28 14:50:40 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    geometry/Beam.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementation file for Geometry::Beam
*/
# include "geometry/Beam.h"
# include "geometry/Magnet.h"
# include <TGeoVolume.h>
# include <TGeoTube.h>
# include <TGeoMaterial.h>
# include <TGeoMedium.h>
# include <TGeoManager.h>

//____________________________________________________________________
ClassImp(Geometry::Beam);

//____________________________________________________________________
Geometry::Beam::Beam() 
  : Framework::Task("Beam", "beam pipe")
{}


//____________________________________________________________________
void
Geometry::Beam::Initialize(Option_t* option) 
{
  Debug(5, "Initialize", "Setting up materials");
  Framework::Task::Initialize(option);
  TGeoMaterial* be = new TGeoMaterial("Beryllium", 0.01, 4.0, 1.848);
  Geometry::Magnet* mag = Geometry::Magnet::Instance();
  Double_t mType = mag->GetType();
  Double_t mMax  = mag->GetMax();
  Double_t par[] = { 0., mType, mMax, 1., .1, 1., .1, .1, .1, 0, 0 };
  TGeoMedium* medium = new TGeoMedium("Beryllium", 1, be,  par);
}

//____________________________________________________________________
void
Geometry::Beam::Register(Option_t* option) 
{
  Debug(5, "Register", "Setting up volume");

  TGeoVolume* cave  = gGeoManager->GetVolume("Cave");
  if (!cave) {
    Error("Register", "Failed to get cave volume");
    return;
  }
  TGeoShape*  caves = cave->GetShape();
  Double_t z1, z2;
  Double_t zl       = caves->GetAxisRange(3, z1, z2);
  TGeoMedium* medium     = gGeoManager->GetMedium("Beryllium");
  TGeoTube*   beamShape  = new TGeoTube("Beam", 3.9, 4.0, z1);
  TGeoVolume* beamVolume = new TGeoVolume(GetName(), beamShape, medium);
  beamVolume->SetLineColor(kBlue);
  beamVolume->SetFillColor(kBlue);
  TGeoVolume* top        = gGeoManager->GetTopVolume();
  top->AddNode(beamVolume, 0, 0);
}


//____________________________________________________________________
//
// EOF
//
