//____________________________________________________________________ 
//  
// $Id: TPC.cxx,v 1.4 2007-09-28 14:50:40 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    geometry/TPC.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementation file for Geometry::TPC
*/
# include "geometry/TPC.h"
# include "geometry/Magnet.h"
# include <TGeoVolume.h>
# include <TGeoTube.h>
# include <TGeoMaterial.h>
# include <TGeoMedium.h>
# include <TGeoManager.h>
# include <TMath.h>

//____________________________________________________________________
ClassImp(Geometry::TPC);

//____________________________________________________________________
Geometry::TPC::TPC() 
  : Framework::Task("TPC", "Time Projection Chamber")
{
  SetRInner();
  SetROuter();
  SetLength();
}

//____________________________________________________________________
void
Geometry::TPC::Initialize(Option_t* option) 
{
  Debug(5, "Initialize", "Setting up materials");
  Framework::Task::Initialize(option);

  TGeoMixture* tpcGas = new TGeoMixture("TPC Gas", 2, 0.00168);
  tpcGas->DefineElement(0, 44.00, 22., 0.10);
  tpcGas->DefineElement(1, 39.95, 18., 0.90);
  tpcGas->SetTransparency('0');

  Geometry::Magnet* mag = Geometry::Magnet::Instance();
  Double_t mType = mag->GetType();
  Double_t mMax  = mag->GetMax();
  Double_t pGas[] = { 1., mType, mMax, 1., .001, 1., .001, .001, .001, 0, 0 };
  TGeoMedium* gas = new TGeoMedium("TPC Gas", 1, tpcGas,  pGas);

  // Aluminium 
  Double_t pAl[]  = { 0., mType, mMax, 10., .001,  -1., .003, .003 };
  TGeoMaterial* tpcAl = new TGeoMaterial("TPC Al", 26.981539, 13, 2.7);
  tpcAl->SetFillColor(3);
  TGeoMedium* al = new TGeoMedium("TPC Al", 1, tpcAl, pAl);
}

//____________________________________________________________________
void
Geometry::TPC::Register(Option_t* option) 
{
  Debug(5, "Register", "Setting up volume");
  // Container volume 
  TGeoVolume* top       = gGeoManager->GetTopVolume();
  TGeoVolume* tpcTop    = new TGeoVolumeAssembly("TPCt");
  top->AddNode(tpcTop, 0, 0);
  
  TGeoMedium* tpcGas    = gGeoManager->GetMedium("TPC Gas");
  // Gas volume 
  TGeoTube*   gasShape  =  new TGeoTube("TPC", fRInner, fROuter, fLength/2);
  TGeoVolume* fGasVol   = new TGeoVolume("TPC", gasShape, tpcGas);
  fGasVol->SetLineColor(kBlue);
  fGasVol->SetFillColor(kBlue);
  tpcTop->AddNode(fGasVol, 0, 0);

  TGeoMedium* tpcAl     = gGeoManager->GetMedium("TPC Al");
  // Inner volume 
  TGeoTube* innerShape  = new TGeoTube("TPCi", fRInner-10,fRInner,fLength/2);
  fInner                = new TGeoVolume("TPCi", innerShape, tpcAl);
  tpcTop->AddNode(fInner, 0, 0);

  // Outer volume 
  TGeoTube* outerShape  = new TGeoTube("TPCo",fROuter,fROuter+10,fLength/2);
  fOuter                = new TGeoVolume("TPCo", outerShape, tpcAl);
  tpcTop->AddNode(fOuter, 0, 0);
}

//________________________________________________________c____________
//
// EOF
//
