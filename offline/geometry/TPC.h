// -*- mode: C++ -*- 
//____________________________________________________________________ 
//  
// $Id: TPC.h,v 1.5 2005-12-14 22:04:54 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    geometry/TPC.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Geometry::TPC
*/
#ifndef Geometry_TPC
#define Geometry_TPC
#ifndef Framework_Task
# include <framework/Task.h>
#endif
class TGeoVolume;

/** @defgroup tpc Time Projection Chamber classes */
namespace Geometry 
{
  /** @class TPC geometry/TPC.h <geometry/TPC.h>
      @brief TPC geometry 
      @ingroup geometry tpc
      @image html TPC.png 
   */
  class TPC : public Framework::Task 
  {
  private:
    /** Inner radius */
    Double_t fRInner;
    /** Outer radius */
    Double_t fROuter;
    /** Length along beam  */
    Double_t fLength;

    TGeoVolume* fGasVol;
    TGeoVolume* fInner;
    TGeoVolume* fOuter;
  public:
    /** CTOR */
    TPC();
    /** Initialize */
    void Initialize(Option_t* option="");
    /** Register */
    void Register(Option_t* option="");
    /** @param option unused */
    virtual void Exec(Option_t* option="") {}
    /** @param r Inner radius  */
    void SetRInner(Double_t r=85) { fRInner = r; } //*MENU*
    /** @param r Outer radius  */
    void SetROuter(Double_t r=250) { fROuter = r; } //*MENU*
    /** @param l Length along beam  */
    void SetLength(Double_t l=500)  { fLength = l; } //*MENU*
    /** @return Inner radius  */
    Double_t GetRInner() const { return fRInner; } //*MENU*
    /** @return Outer radius  */
    Double_t GetROuter() const { return fROuter; } //*MENU*
    /** @return Length along beam  */
    Double_t GetLength() const { return fLength; } //*MENU*
    ClassDef(TPC,0) //
  };
}

#endif
//____________________________________________________________________
//
// EOF
//
