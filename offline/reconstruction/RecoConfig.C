//____________________________________________________________________ 
//  
// $Id: RecoConfig.C,v 1.2 2005-12-14 22:05:28 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    RecoConfig.C
 *  @author  Christian Holm Christensen <cholm@nbi.dk>
 *  @date    Thu Dec  2 01:12:02 2004
 *  @brief   Example configuration script 
 */
/** Function to configure a reconstruction run
    @ingroup reconstruction
 */
#ifdef __CINT__
void RecoConfig() 
#endif
{
  gROOT->LoadClass("Simulation::Main", "libSimulation.so");
  gROOT->LoadClass("Reconstruction::TOF", "libReconstruction.so");

  Framework::Main* main          = Framework::Main::Instance();

  Framework::TreeReader*    read = new Framework::TreeReader("Digits", 
							      "digits.root");
  Geometry::GeometryReader* gread= new Geometry::GeometryReader("geom", 
								 "geom.root");
  Framework::ArrayReader*   rv0  = new Framework::ArrayReader("V0Digits", 
							       "Data::V0Digit",
							       "V0", "Digits");
  Framework::ArrayReader*   rfmd = new Framework::ArrayReader("FMDDigits", 
							      "Data::FMDDigit",
							      "FMD", "Digits");
  Reconstruction::V0*       v0   = new Reconstruction::V0;
  Reconstruction::FMD*      fmd  = new Reconstruction::FMD;
  Framework::TreeWriter*    write= new Framework::TreeWriter("Recon", 
							     "reco.root");

  main->Add(read);
  main->Add(gread);
  main->Add(rv0);
  main->Add(rfmd);
  main->Add(v0);
  main->Add(fmd);
  main->Add(write);
  
  main->SetVerbose(5);
  main->SetDebug(2);

  if (!gROOT->IsBatch()) {
    TBrowser* b = new TBrowser("b");
    
    Printf("\nSetup complete.\n\nUse context menu to execute reconstruction, "
	   "or type\n\n\tFramework::Main::Instance()->Loop();\n\n"
	   "to loop over the events");
  }
}

//
// EOF
//
  
