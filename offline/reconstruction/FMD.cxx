//____________________________________________________________________ 
//  
// $Id: FMD.cxx,v 1.7 2007-09-28 14:50:40 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    reconstruction/FMD.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec 16 01:36:07 2004
    @brief   Implementation of Reconstruction::FMD
*/
#include <TClonesArray.h>
#include <TRandom.h>
#include <TBranch.h>
#include <TTree.h>
#include <TFolder.h>
#include <TGeoVolume.h>
#include <TGeoBBox.h>
#include <TGeoManager.h>
#include "reconstruction/FMD.h"
#include "data/FMDDigit.h"
#include "data/Vertex.h"
#include "data/FMDReco.h"
#include <TMath.h>
  
//____________________________________________________________________
ClassImp(Reconstruction::FMD);

//____________________________________________________________________
Reconstruction::FMD::FMD()
  : Framework::Task("FMD", "FMD reconstructor")
{
  // Default constructor
  fRandom = new TRandom;
  fCache  = new TClonesArray("Data::FMDReco");
  SetMIPEnergy();
  SetMIPRange();
  SetADCRange();
  SetPedestal();
}


//____________________________________________________________________
void 
Reconstruction::FMD::Register(Option_t* option) 
{
  // Register a clones array as a branch and in a folder 
  Debug(5, "Register", "register output branch");
  TTree* tree = 0;
  if (!(tree = GetBaseTree("Recon"))) return;
  fBranch = tree->Branch(GetName(), &fCache);
}

//____________________________________________________________________
void 
Reconstruction::FMD::Detector2XYZ(UShort_t d, Char_t r, UShort_t s, UShort_t t,
				  Double_t& x, Double_t& y, Double_t& z) 
{
  TString path;
  TString n;
  TGeoNode* top = gGeoManager->GetTopNode();
  if (!top) { 
    Warning("Detector2XYZ", "no top node");
    return;
  }
  path += top->GetName();
  
  n = Form("FMD%d_%d",d,d);
  TGeoNode* fmd=static_cast<TGeoNode*>(top->GetNodes()->FindObject(n.Data()));
  if (!fmd) { 
    Warning("Detector2XYZ", "no %s node in %s", n.Data(), path.Data());
    return;
  }
  path += "/";
  path += fmd->GetName();
  n = Form("FMD%c_%d", r,r);
  TGeoNode* rng =static_cast<TGeoNode*>(fmd->GetNodes()->FindObject(n.Data()));
  if (!rng) { 
    Warning("Detector2XYZ", "no %snode in %s", n.Data(), path.Data());
    return;
  }
  path += "/";
  path += rng->GetName();
  n    =  Form("F%cMO_%d", r,(s / 2));
  TGeoNode* mod =static_cast<TGeoNode*>(rng->GetNodes()->FindObject(n.Data()));
  if (!mod) { 
    Warning("Detector2XYZ", "no %s node in %s", n.Data(), path.Data());
    return;
  }
  path += "/";
  path += mod->GetName();
  n    =  Form("F%cAC_0", r);
  TGeoNode* act =static_cast<TGeoNode*>(mod->GetNodes()->FindObject(n.Data()));
  if (!act) { 
    Warning("Detector2XYZ", "no %s node in %s", n.Data(), path.Data());
    return;
  }
  path += "/";
  path += act->GetName();
  n    =  Form("F%cSE_%d", r, (s % 2) + 1);
  TGeoNode* sec =static_cast<TGeoNode*>(act->GetNodes()->FindObject(n.Data()));
  if (!sec) { 
    Warning("Detector2XYZ", "no %s node in %s", n.Data(), path.Data());
    return;
  }
  path += "/";
  path += sec->GetName();
  n    =  Form("F%cST_%d", r, t + 1);
  TGeoNode* str =static_cast<TGeoNode*>(sec->GetNodes()->FindObject(n.Data()));
  if (!str) { 
    Warning("Detector2XYZ", "no %s node in %s", n.Data(), path.Data());
    return;
  }
  path += "/";
  path += str->GetName();

  Debug(10,"Detector2YXZ", "FMD%d%c[%d,%d] path is %s", 
	d, r, s, t, path.Data());
  TGeoVolume* vol = str->GetVolume();
  if (!vol) {
    Warning("Detector2XYZ", "No volume for %s", path.Data());
    return;
  }
  TGeoBBox* bbox = static_cast<TGeoBBox*>(vol->GetShape());
  if (!bbox) {
    Warning("Detector2XYZ", "No bounding box for %s", vol->GetName());
    return;
  }    
  const Double_t* o   = bbox->GetOrigin();
  Double_t        l[] = { o[0], o[1], o[2] };
  Double_t        m[] = { 0, 0, 0 };
  str->LocalToMaster(l, m);
  sec->LocalToMaster(m, l);
  act->LocalToMaster(l, m);
  mod->LocalToMaster(m, l);
  rng->LocalToMaster(l, m);
  fmd->LocalToMaster(m, l);
  x = l[0];
  y = l[1];
  z = l[2];

  // Check that we find the right coordinates
#if 0
  TGeoNode* check = gGeoManager->FindNode(x, y, z);
  if (check) {
    if (check != str) 
      Info("Detector2XYZ", "mismatch %s != %s!", check->GetName(), 
	   str->GetName());
  }
  else 
    Info("Detector2XYZ", "manager gave up on (%f,%f,%f)", x, y, z);
#endif
}

//____________________________________________________________________
void 
Reconstruction::FMD::Exec(Option_t* option) 
{
  TFolder* bfolder = GetBaseFolder();
  if (!bfolder) return;
  TFolder* folder = static_cast<TFolder*>(bfolder->FindObject("FMDDigits"));
  if (!folder) {
    Debug(1, "Exec", "couldn't find the digits folder");
    return;
  }
  TClonesArray* digits =
    static_cast<TClonesArray*>(folder->FindObject("Data::FMDDigits"));
  if (!digits) {
    Stop("Exec", "Couldn't find digits array");
    return;
  }
  Data::Vertex* vertex = 
    static_cast<Data::Vertex*>(bfolder->FindObject("vertex"));
  // If not flagged as OK, we don't want it.
  if (vertex && vertex->fT < 1000)
    Debug(1, "Exec", "Got vertex: %s %s", 
	  vertex->GetName(), vertex->GetTitle());
  else {
    Debug(1, "Exec", "couldn't find the vertex object");
    vertex = 0;
  }
  
  
  // Geometry::FMD* geometry = Geometry::FMD::Instance();

  Int_t n = digits->GetEntries();
  Verbose(1, "Exec", "got %d digits", n);

  fCache->Clear();
  TClonesArray& cache = *static_cast<TClonesArray*>(fCache);

  Int_t j = 0;
  for (Int_t i = 0; i < n; i++) {
    Data::FMDDigit* digit = static_cast<Data::FMDDigit*>(digits->At(i));
    UShort_t counts       = digit->fCounts;
    Float_t  pedSub       = fPedestal + fPedestalFactor * fPedestalWidth;
    Float_t  countsPedSub = counts - pedSub;
    if (countsPedSub <= 0) { 
      Debug(10,"Exec", "Digit at  %s=%d below threshold %f = %f + %f * %f", 
	    digit->Name(), counts, pedSub, fPedestal, fPedestalFactor,
	    fPedestalWidth);
      continue;
    }
    Float_t  multiplicity = countsPedSub / fADCRange * fMIPRange;
    Float_t  energyLoss   = multiplicity * fMIPEnergy;
    
    if (vertex) {
      Double_t x, y, z;
      Detector2XYZ(digit->Detector(), digit->Ring(), digit->Sector(), 
		   digit->Strip(), x, y, z);
      Double_t r = TMath::Sqrt(x * x + y * y);
      Double_t theta = TMath::ATan2(r, TMath::Abs(z - vertex->fZ));
      energyLoss   *= TMath::Cos(theta);
      multiplicity *= TMath::Cos(theta);
    }
    Debug(10, "Exec", "Adding new rec at %s=%f", digit->Name(), multiplicity);
    new (cache[j++]) Data::FMDReco(*digit, energyLoss, multiplicity);
  }
}


//____________________________________________________________________ 
//  
// EOF
//
