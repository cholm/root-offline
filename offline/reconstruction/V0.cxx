//____________________________________________________________________ 
//  
// $Id: V0.cxx,v 1.6 2007-09-28 14:50:40 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    reconstruction/V0.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec 16 01:36:07 2004
    @brief   Implementation of Reconstruction::V0
*/
#include <TClonesArray.h>
#include <TRandom.h>
#include <TBranch.h>
#include <TTree.h>
#include <TFolder.h>
#include <TGeoVolume.h>
#include <TGeoNode.h>
#include <TGeoBBox.h>
#include <TGeoManager.h>
#include "reconstruction/V0.h"
#include "data/Vertex.h"
#include "data/V0Digit.h"
#include "data/V0Reco.h"
#include <TMath.h>

//____________________________________________________________________
ClassImp(Reconstruction::V0);

//____________________________________________________________________
Reconstruction::V0::V0()
  : Framework::Task("V0", "V0 reconstructor"), 
    fVertex(0)
{
  // Default constructor
  fCache  = new TClonesArray("Data::V0Reco");
  SetMIPEnergy();
  SetMIPRange();
  SetADCRange();
  SetTDCConv();
  SetTDCDelay();
  SetPedestal();
}

//____________________________________________________________________
void 
Reconstruction::V0::Initialize(Option_t* option)
{
  Framework::Task::Initialize();
  fVertex = new Data::Vertex;
  // fFolder->Add(fVertex);
  TFolder* folder = GetBaseFolder();
  if (!folder) return;
  folder->Add(fVertex);
}

//____________________________________________________________________
void 
Reconstruction::V0::Register(Option_t* option) 
{
  // Register a clones array as a branch and in a folder 
  Debug(5, "Register", "register output branch");
  TTree* tree = 0;
  if (!(tree = GetBaseTree("Recon"))) return;
  fBranch = tree->Branch(GetName(), &fCache);
  fBranch = tree->Branch("Vertex", "Data::Vertex", &fVertex);
}

//____________________________________________________________________
void 
Reconstruction::V0::Detector2XYZ(Char_t d, UShort_t r, UShort_t s,
				 Double_t& x, Double_t& y, Double_t& z) 
{
  TString path;
  TString n;
  TGeoNode* top = gGeoManager->GetTopNode();
  if (!top) { 
    Warning("Detector2XYZ", "no top node");
    return;
  }
  path += top->GetName();
  
  n = Form("V0%c_%d",d,d);
  TGeoNode* v0 = static_cast<TGeoNode*>(top->GetNodes()->FindObject(n.Data()));
  if (!v0) { 
    Warning("Detector2XYZ", "no %s node in %s", n.Data(), path.Data());
    return;
  }
  path += "/";
  path += v0->GetName();
  n = Form("V0%c%d_%d", d,r,r);
  TGeoNode* rng = static_cast<TGeoNode*>(v0->GetNodes()->FindObject(n.Data()));
  if (!rng) { 
    Warning("Detector2XYZ", "no %snode in %s", n.Data(), path.Data());
    return;
  }
  path += "/";
  path += rng->GetName();
  n    =  Form("V%c%dM_%d", d,r,s);
  TGeoNode* mod =static_cast<TGeoNode*>(rng->GetNodes()->FindObject(n.Data()));
  if (!mod) { 
    Warning("Detector2XYZ", "no %s node in %s", n.Data(), path.Data());
    return;
  }
  path += "/";
  path += mod->GetName();

  TGeoVolume* vol = mod->GetVolume();
  if (!vol) {
    Warning("Detector2XYZ", "No volume for %s", path.Data());
    return;
  }
  TGeoBBox* bbox = static_cast<TGeoBBox*>(vol->GetShape());
  if (!bbox) {
    Warning("Detector2XYZ", "No bounding box for %s", vol->GetName());
    return;
  }    
  const Double_t* o   = bbox->GetOrigin();
  Double_t        l[] = { o[0], o[1], o[2] };
  Double_t        m[] = { 0, 0, 0 };
  mod->LocalToMaster(l, m);
  rng->LocalToMaster(m,l);
  v0->LocalToMaster(l,m);
  x = m[0];
  y = m[1];
  z = m[2];

  TGeoNode* check = gGeoManager->FindNode(x, y, z);
  if (check) {
    if (check != mod) 
      Info("Detector2XYZ", "mismatch %s != %s!", check->GetName(), 
	   mod->GetName());
  }
  else 
    Info("Detector2XYZ", "manager gave up on (%f,%f,%f)", x, y, z);
}
			   
//____________________________________________________________________
void 
Reconstruction::V0::Exec(Option_t* option) 
{
  TFolder* folder = GetBaseFolder();
  if (!folder)
    return;
  folder = static_cast<TFolder*>(folder->FindObject("V0Digits"));
  if (!folder) {
    Debug(1, "Exec", "couldn't find the digits folder");
    return;
  }
  TClonesArray* digits =
    static_cast<TClonesArray*>(folder->FindObject("Data::V0Digits"));
  if (!digits) {
    Stop("Exec", "Couldn't find digits array");
    return;
  }

  Int_t n = digits->GetEntries();
  Verbose(1, "Exec", "got %d digits", n);

  fCache->Clear();
  fVertex->fZ = 0;
  fVertex->fT = 1000000;
  TClonesArray& cache = *static_cast<TClonesArray*>(fCache);

  Data::V0Reco* a = 0;
  Data::V0Reco* c = 0;
  
  // Find first digit 
  Int_t j = 0;
  for (Int_t i = 0; i < n; i++) {
    Data::V0Digit* digit = static_cast<Data::V0Digit*>(digits->At(i));
    UShort_t counts       = digit->fCounts;
    Float_t  pedSub       = fPedestal + fPedestalFactor * fPedestalWidth;
    Float_t  countsPedSub = counts - pedSub;
    if (countsPedSub <= 0) continue;
    
    Float_t  multiplicity = countsPedSub / fADCRange * fMIPRange;
    Float_t  energyLoss   = multiplicity * fMIPEnergy;
    Float_t  time         = (digit->fTime / fTDCConv) + fTDCDelay;

    Data::V0Reco* reco = new (cache[j++]) Data::V0Reco(digit->fDetector, 
						       digit->fRing, 
						       digit->fSector, 
						       energyLoss, 
						       multiplicity, 
						       time);
    // Find first hit on either side 
    Data::V0Reco* what = 0;
    switch (reco->fDetector) {
    case 'a': case 'A': 
      a = (a && a->fTime < reco->fTime) ? a : reco;
      what = a;
      break;
    case 'c': case 'C': 
      c = (c && c->fTime < reco->fTime) ? c : reco;
      what = c;
      break;
    }
    Debug(30, "Exec", "New time %g on side %c", what->fTime, what->fDetector);
  }
  if (!a || !c) {
    Warning("Exec", "can not make vertex - "
	    "one of A (0x%x) or C (0x%x) is missing", a, c);
    return;
  }
  Double_t x=0, y=0, z=0, r;
  Detector2XYZ(a->fDetector, a->fRing, a->fSector, x, y, z);
  Double_t da  = TMath::Sqrt(x * x + y * y + z * z);
  Detector2XYZ(c->fDetector, c->fRing, c->fSector, x, y, z);
  Double_t dc  = TMath::Sqrt(x * x + y * y + z * z);
  Double_t dda = a->fTime * TMath::C() * 100;
  Double_t ddc = c->fTime * TMath::C() * 100;
  Double_t ipz = (TMath::C() * 100 * (a->fTime - c->fTime) - (da - dc)) / 2;
  Verbose(3, "Exec", "Side A distance %f; C distance %f -> = %f",
	  da, dc, ipz); 
  Debug(10,"Exec", "vz=(c * 100 * (%g - %g) - (%f - %f))/2=%f", 
	a->fTime, c->fTime, da, dc, ipz);
  if (!fVertex) {
    Warning("Exec", "Vertex obejct disappeared");
    return;
  }
  fVertex->fZ = ipz;  
  fVertex->fT = 0;
}


//____________________________________________________________________ 
//  
// EOF
//
