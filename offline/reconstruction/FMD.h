// -*- mode: c++ -*- 
//____________________________________________________________________ 
//  
// $Id: FMD.h,v 1.6 2006-11-08 01:41:44 cholm Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    reconstruction/FMD.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Tue Dec 14 15:32:30 2004
    @brief   Declaration of the FMD digitizer 
*/
#ifndef Reconstruction_FMD
#define Reconstruction_FMD
#ifndef Framework_Task
# include <framework/Task.h>
#endif
class TRandom;

/** @defgroup reconstruction Reconstruction classes */
/** @namespace Reconstruction Namespace for all reconstructors
 */
namespace Reconstruction 
{
  /** @class FMD reconstruction/FMD.h <reconstruction/FMD.h>
      @brief Reconstruct FMD Hits and from FMD digits 
      @ingroup reconstruction fmd
      @image html FMDReco.png
   */
  class FMD : public Framework::Task
  {
  private:
    /** Random number generator */
    TRandom* fRandom;
    /** Energy deposited by a MIP */
    Double_t fMIPEnergy;
    /** ADC MIP dynamic range */
    Int_t    fMIPRange;
    /** ADC dynamic range */
    Int_t    fADCRange;
    /** Pedestal mean */
    Double_t fPedestal;
    /** Pedestal spread */
    Double_t fPedestalWidth;
    /** Pedestal factor */
    Double_t fPedestalFactor;
    /** Convert detector coordnates to cartisian global coordinates 
	@param d Detector #
	@param r Ring Id
	@param s Sector # 
	@param t Strip # 
	@param x X -coordinate 
	@param y Y -coordinate 
	@param z Z -coordinate */
    void Detector2XYZ(UShort_t d, Char_t r, UShort_t s, UShort_t t,
		      Double_t& x, Double_t& y, Double_t& z);
  public:
    /** Create an FMD digitizer */
    FMD();
    virtual ~FMD() {}
    
    /** @param x Energy deposisted by a MIP */
    void     SetMIPEnergy(Double_t x=.03*1.664)  { fMIPEnergy = x; }  //*MENU*
    /** @param x ADC dynamic MIP range */
    void     SetMIPRange(Int_t x=20)      { fMIPRange = x; }    //*MENU*
    /** @param x ADC range */
    void     SetADCRange(Int_t x=1024) { fADCRange = x; }    //*MENU*
    /** Set pedestal parameters 
	@param m Pedestal mean 
	@param w Pedestal width  
	@param f Pedestal factor */
    void     SetPedestal(Double_t m=10, Double_t w=.2, Double_t f=2) { 
      fPedestal = m; fPedestalWidth = w; fPedestalFactor = f;
    }
    /** @return  Energy deposisted by a MIP */
    Double_t GetMIPEnergy() const { return fMIPEnergy; } 
    /** @return  ADC dynamic MIP range */
    Int_t    GetMIPRange()  const { return fMIPRange; }
    /** @return  ADC range */
    Int_t    GetADCRange()  const { return fADCRange; }

    /** Register output branch, etc. 
	@param option Not used  */
    virtual void Register(Option_t* option=""); 
    /** Create digits from hits 
	@param option Not used. */
    virtual void Exec(Option_t* option=""); 
    ClassDef(FMD,0) // Reconstruct hits in the FMD
  };
}

#endif
//____________________________________________________________________ 
//  
// EOF
//
